{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Zachlod, D., R\u00fcttgers, B., Bludau, S., Mohlberg, H., Langner, R., Zilles, K., Amunts, K. (2020) Four new cytoarchitectonic areas surrounding the primary and early auditory cortex in human brains. Cortex, 128:1-29",
			"doi": "10.1016/j.cortex.2020.02.021"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area TE 2.1 (STG) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area TE 2.1 (STG). The probability map of Area TE 2.1 (STG) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area TE 2.1 (STG):\nZachlod et al. (2019) [Data set, v5.1] [DOI: 10.25493/R28N-2TD](https://doi.org/10.25493%2FR28N-2TD)\nZachlod et al. (2020) [Data set, v6.0] [DOI: 10.25493/AT57-MNB](https://doi.org/10.25493%2FAT57-MNB)\n\nThe most probable delineation of Area TE 2.1 (STG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 2.1 (STG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/3332ad65-a5e6-436f-95f0-c634fbcb075c"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area TE 2.1 (STG) (v6.2)",
	"files": [
		{
			"byteSize": 87866.0,
			"hashcode": "fb7cdb4934718ff6f57c2143aec3202f",
			"name": "Area-TE-2.1_r_N10_nlin2Stdcolin27_6.2_publicDOI_3fa55258ec2b2909be6fe1665d5220f3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-2.1_pub/6.2/Area-TE-2.1_r_N10_nlin2Stdcolin27_6.2_publicDOI_3fa55258ec2b2909be6fe1665d5220f3.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "3fa55258ec2b2909be6fe1665d5220f3"
		},
		{
			"byteSize": null,
			"hashcode": "83af3fcd7d7840bd4f26f0f2f9492459",
			"name": null,
			"absolutePath": null,
			"contentType": null,
			"hash": null
		},
		{
			"byteSize": 96392.0,
			"hashcode": "257c3988459c0029ddf48da109c82f10",
			"name": "Area-TE-2.1_l_N10_nlin2Stdcolin27_6.2_publicDOI_632f4fb4ef86acb2a4d0031c66095d3e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-2.1_pub/6.2/Area-TE-2.1_l_N10_nlin2Stdcolin27_6.2_publicDOI_632f4fb4ef86acb2a4d0031c66095d3e.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "632f4fb4ef86acb2a4d0031c66095d3e"
		},
		{
			"byteSize": 77677.0,
			"hashcode": "3cb86129ca650cc086af6104561d803a",
			"name": "Area-TE-2.1_l_N10_nlin2ICBM152asym2009c_6.2_publicDOI_632f4fb4ef86acb2a4d0031c66095d3e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-2.1_pub/6.2/Area-TE-2.1_l_N10_nlin2ICBM152asym2009c_6.2_publicDOI_632f4fb4ef86acb2a4d0031c66095d3e.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4d9ab8e9cbff1dc92c5129a212771303"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "004127f50f08dd0e03c6f2a77824f461",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-2.1_pub/6.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 21.0,
			"hashcode": "bba1039a45ddf632a873059cda382488",
			"name": "subjects_Area-TE-2.1.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-2.1_pub/6.2/subjects_Area-TE-2.1.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/959e0acd-4e69-40ac-a4c2-c9dd9f7bd2a1",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		},
		{
			"schema.org/shortName": null,
			"identifier": "fbec7907-0560-41cb-bec6-bf376991f02b303708",
			"name": "Zachlod, Daniel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/fbec7907-0560-41cb-bec6-bf376991f02b",
			"shortName": "Zachlod, D."
		}
	],
	"id": "959e0acd-4e69-40ac-a4c2-c9dd9f7bd2a1",
	"kgReference": [
		"10.25493/1PAH-KDR"
	],
	"publications": [
		{
			"name": "Four new cytoarchitectonic areas surrounding the primary and early auditory cortex in human brains ",
			"cite": "Zachlod, D., R\u00fcttgers, B., Bludau, S., Mohlberg, H., Langner, R., Zilles, K., Amunts, K. (2020) Four new cytoarchitectonic areas surrounding the primary and early auditory cortex in human brains. Cortex, 128:1-29",
			"doi": "10.1016/j.cortex.2020.02.021"
		}
	]
}
