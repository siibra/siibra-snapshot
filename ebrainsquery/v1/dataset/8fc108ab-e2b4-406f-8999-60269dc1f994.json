{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Roetzer-Pejrimovsky, T., Moser, A. C., Atli, B., Vogel, C. C., Mercea, P. A., Prihoda, R., Gelpi, E., Haberler, C., H\u00f6ftberger, R., Hainfellner, J. A., Baumann, B., Langs, G., Woehrer, A. (2022) The Digital Brain Tumour Atlas, an open histopathology resource. Scientific Data. 2022;9(1):55.",
			"doi": "10.1038/s41597-022-01157-0"
		}
	],
	"activity": [
		{
			"protocols": [
				"Brain-wide",
				"cancer",
				"tumor",
				"H&E",
				"Atlas",
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"Digital Histopathology",
		"H&E staining"
	],
	"custodians": [
		{
			"schema.org/shortName": null,
			"identifier": "cb320a5b-ade9-41f5-903b-17ff2b311267",
			"name": "Roetzer-Pejrimovsky, Thomas",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/cb320a5b-ade9-41f5-903b-17ff2b311267",
			"shortName": "Roetzer-Pejrimovsky, T."
		}
	],
	"project": [],
	"description": "Currently, approximately 150 different brain tumour types are defined by the WHO. Recent endeavours to exploit machine learning and deep learning methods for supporting more precise diagnostics based on the histological tumour appearance have been hampered by the relative paucity of accessible digital histopathological datasets. While freely available datasets are relatively common in many medical specialties such as radiology and genomic medicine, there is still an unmet need regarding histopathological data.\n\nThus, we digitized a significant portion of a large dedicated brain tumour bank based at the Division of Neuropathology and Neurochemistry of the Medical University of Vienna, covering brain tumour cases from 1995-2019. A total of 3,115 slides of 126 brain tumour types (including 47 control tissue slides) have been scanned. Additionally, complementary clinical annotations have been collected for each case.\n\nIn the present manuscript, we thoroughly discuss this unique dataset and make it publicly available for potential use cases in machine learning and digital image analysis, teaching and as a reference for external validation.",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "The use of this dataset requires that the user cites the associated DOI and adheres to the conditions of use that are contained in the Data Use Agreement. You may not use the dataset for commercial purposes.",
			"url": null
		}
	],
	"embargoStatus": [
		{
			"identifier": "1d726b76-b176-47ed-96f0-b4f2e17d5f19",
			"name": "Under review",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/1d726b76-b176-47ed-96f0-b4f2e17d5f19"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [
		"Homo sapiens"
	],
	"name": "The Digital Brain Tumour Atlas, an open histopathology resource",
	"files": [],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/8fc108ab-e2b4-406f-8999-60269dc1f994",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "ca117306-3f31-4164-a150-eda3e8fad8be",
			"name": "Woehrer, Adelheid",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ca117306-3f31-4164-a150-eda3e8fad8be",
			"shortName": "Woehrer, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "41e8c013-e48a-4c1b-840b-1091f289fb97",
			"name": "Langs, Georg",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/41e8c013-e48a-4c1b-840b-1091f289fb97",
			"shortName": "Langs, G."
		},
		{
			"schema.org/shortName": null,
			"identifier": "187f9a0b-aa4b-4496-acb2-3ede759bf3c9",
			"name": "Baumann, Bernhard",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/187f9a0b-aa4b-4496-acb2-3ede759bf3c9",
			"shortName": "Baumann, B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "6952d2cb-c170-461a-8cf5-39d1c8af6391",
			"name": "Hainfellner, Johannes A.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/6952d2cb-c170-461a-8cf5-39d1c8af6391",
			"shortName": "Hainfellner, J. A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "78c537b8-d875-47ad-8c5f-ee75894c7e27",
			"name": "H\u00f6ftberger, Romana",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/78c537b8-d875-47ad-8c5f-ee75894c7e27",
			"shortName": "H\u00f6ftberger, R."
		},
		{
			"schema.org/shortName": null,
			"identifier": "37ed43c5-2fa6-4060-af88-daa1332272f6",
			"name": "Haberler, Christine",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/37ed43c5-2fa6-4060-af88-daa1332272f6",
			"shortName": "Haberler, C."
		},
		{
			"schema.org/shortName": null,
			"identifier": "ad645ca3-951c-4c0a-a0a9-515afde7034d",
			"name": "Gelpi, Ellen",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ad645ca3-951c-4c0a-a0a9-515afde7034d",
			"shortName": "Gelpi, E."
		},
		{
			"schema.org/shortName": null,
			"identifier": "cd520779-2ceb-4256-85cd-55cb2679f6ff",
			"name": "Prihoda, Romana",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/cd520779-2ceb-4256-85cd-55cb2679f6ff",
			"shortName": "Prihoda, R."
		},
		{
			"schema.org/shortName": null,
			"identifier": "fd38069d-6ca4-41da-92ab-7a68adc59eb6",
			"name": "Mercea, Petra A.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/fd38069d-6ca4-41da-92ab-7a68adc59eb6",
			"shortName": "Mercea, P. A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "5b61d807-385e-4dad-acb4-ef1884a2ba67",
			"name": "Vogel, Clemens Christian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5b61d807-385e-4dad-acb4-ef1884a2ba67",
			"shortName": "Vogel, C. C."
		},
		{
			"schema.org/shortName": null,
			"identifier": "0a298675-5d2a-404f-b450-4114559afc83",
			"name": "Atli, Baran",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/0a298675-5d2a-404f-b450-4114559afc83",
			"shortName": "Atli, B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "208777b1-ab25-489c-a32f-778a26359724",
			"name": "Moser, Anna-Christina",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/208777b1-ab25-489c-a32f-778a26359724",
			"shortName": "Moser, A. C."
		},
		{
			"schema.org/shortName": null,
			"identifier": "cb320a5b-ade9-41f5-903b-17ff2b311267",
			"name": "Roetzer-Pejrimovsky, Thomas",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/cb320a5b-ade9-41f5-903b-17ff2b311267",
			"shortName": "Roetzer-Pejrimovsky, T."
		}
	],
	"id": "8fc108ab-e2b4-406f-8999-60269dc1f994",
	"kgReference": [
		"10.25493/WQ48-ZGX"
	],
	"publications": [
		{
			"name": "The Digital Brain Tumour Atlas, an open histopathology resource",
			"cite": "Roetzer-Pejrimovsky, T., Moser, A. C., Atli, B., Vogel, C. C., Mercea, P. A., Prihoda, R., Gelpi, E., Haberler, C., H\u00f6ftberger, R., Hainfellner, J. A., Baumann, B., Langs, G., Woehrer, A. (2022) The Digital Brain Tumour Atlas, an open histopathology resource. Scientific Data. 2022;9(1):55.",
			"doi": "10.1038/s41597-022-01157-0"
		}
	]
}
