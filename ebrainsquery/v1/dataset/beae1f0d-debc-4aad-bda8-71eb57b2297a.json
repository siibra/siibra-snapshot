{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Palomero-Gallagher, N., Kedo, O., Mohlberg, H., Zilles, K., Amunts, K. (2020) Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus. Brain Struct Funct., 225(3):881-907.",
			"doi": "10.1007/s00429-019-02022-4"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of CA3 (Hippocampus) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to CA3 (Hippocampus). The probability map of CA3 (Hippocampus) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of CA3 (Hippocampus): \nAmunts et al. (2020) [Data set, v11b.0] [DOI: 10.25493/MQ0Y-22E](https://doi.org/10.25493/MQ0Y-22E)\nPalomero-Gallagher et al. (2020) [Data set, v13.0] [DOI: 10.25493/C546-GS0](https://doi.org/10.25493%2FC546-GS0)\n\nThe most probable delineation of CA3 (Hippocampus) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "CA3 (Hippocampus)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/73d8e689-9485-4796-b2b2-47181fc45323"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of CA3 (Hippocampus) (v13.0)",
	"files": [
		{
			"byteSize": 73882.0,
			"hashcode": "5a559c12fb7baba6154484792834f03e",
			"name": "CA3_r_N10_nlin2ICBM152asym2009c_13.0_publicP_61e9544347ec2574aa724f651a09074d.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA3_pub/13.0/CA3_r_N10_nlin2ICBM152asym2009c_13.0_publicP_61e9544347ec2574aa724f651a09074d.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "61e9544347ec2574aa724f651a09074d"
		},
		{
			"byteSize": 92959.0,
			"hashcode": "96cf916d946d0682ee2932b150c06e84",
			"name": "CA3_l_N10_nlin2Stdcolin27_13.0_publicP_7d45eda4613190b3b37867c66b810543.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA3_pub/13.0/CA3_l_N10_nlin2Stdcolin27_13.0_publicP_7d45eda4613190b3b37867c66b810543.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "7d45eda4613190b3b37867c66b810543"
		},
		{
			"byteSize": 21.0,
			"hashcode": "51f094361b44340499f1ed4e81aac52e",
			"name": "subjects_CA3.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA3_pub/13.0/subjects_CA3.csv",
			"contentType": "text/csv",
			"hash": "b53cceeb6a14da4074456a7f89f87ab9"
		},
		{
			"byteSize": 69672.0,
			"hashcode": "407b9fa6954317de6ab5ae167d740b65",
			"name": "CA3_l_N10_nlin2ICBM152asym2009c_13.0_publicP_484eeaf9d0a834a6ddab47b9ec84f00f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA3_pub/13.0/CA3_l_N10_nlin2ICBM152asym2009c_13.0_publicP_484eeaf9d0a834a6ddab47b9ec84f00f.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "484eeaf9d0a834a6ddab47b9ec84f00f"
		},
		{
			"byteSize": 95975.0,
			"hashcode": "70d00f4fffee4053bec2904af938821e",
			"name": "CA3_r_N10_nlin2Stdcolin27_13.0_publicP_f5b2841787a24cd3c4b9c8aa446e3b51.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA3_pub/13.0/CA3_r_N10_nlin2Stdcolin27_13.0_publicP_f5b2841787a24cd3c4b9c8aa446e3b51.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "f5b2841787a24cd3c4b9c8aa446e3b51"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "eac5efd31aaed24699d1ffd9863a6fb1",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA3_pub/13.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/beae1f0d-debc-4aad-bda8-71eb57b2297a",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3e871e11c214a058a43cb1fc38788eea",
			"name": "Kedo, O.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9dabc7a3-fa77-481f-ab9e-8ef6ac6e7d42",
			"shortName": "Kedo, O."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "beae1f0d-debc-4aad-bda8-71eb57b2297a",
	"kgReference": [
		"10.25493/S4VD-N70"
	],
	"publications": [
		{
			"name": "Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus.",
			"cite": "Palomero-Gallagher, N., Kedo, O., Mohlberg, H., Zilles, K., Amunts, K. (2020) Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus. Brain Struct Funct., 225(3):881-907.",
			"doi": "10.1007/s00429-019-02022-4"
		}
	]
}
