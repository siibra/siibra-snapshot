{
	"formats": [],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology",
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"Semantic atlas registration",
		"autoradiography - quantitative analysis",
		"autoradiography - imaging"
	],
	"custodians": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"project": [
		"Quantitative receptor data"
	],
	"description": "This dataset contains the densities (in fmol/mg protein) of receptors for classical neurotransmitters of CA2 (hippocampus) obtained by means of quantitative _in vitro_ autoradiography. The receptor densities are visualized as _fingerprints_ (**fp**), which provide the mean density and standard deviation for each of the analyzed receptor types, averaged across samples. \n\nOverview of available measurements [ **receptor** \\| **_neurotransmitter_** \\| _labeling agent_ ]:\n**AMPA** \\| **_glutamate_** \\| _[<sup>3</sup>H]AMPA_\n**kainate** \\| **_glutamate_** \\| _[<sup>3</sup>H]kainate_\n**NMDA** \\| **_glutamate_** \\| _[<sup>3</sup>H]MK-801_\n**GABA<sub>A</sub>** \\| **_GABA_** \\| _[<sup>3</sup>H]muscimol_\n\nInformation on the used tissue samples and corresponding subjects, as well as analyzed receptors accompanies the provided dataset.\n\n**For methodological details, see:**\nZilles, K. et al. (2002). Quantitative analysis of cyto- and receptorarchitecture of the human brain, pp. 573-602. In: Brain Mapping: The Methods, 2nd edition (A.W. Toga and J.C. Mazziotta, eds.). San Diego, Academic Press.\n\nPalomero-Gallagher N, Zilles K. (2018) Cyto- and receptorarchitectonic mapping of the human brain. In: Handbook of Clinical Neurology 150: 355-387",
	"parcellationAtlas": [
		{
			"name": "Waxholm Space rat brain atlas v3",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/ebb923ba-b4d5-4b82-8088-fa9215c2e1fe",
			"id": "ebb923ba-b4d5-4b82-8088-fa9215c2e1fe"
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Rat Cornu ammonis 2 (v3)",
			"alias": "Cornu ammonis 2",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/96bed5ca-ac4f-46a3-99ef-873e7f133f4f"
		}
	],
	"species": [
		"Rattus norvegicus"
	],
	"name": "Density measurements of different receptors for CA2 (Hippocampus) [rat, v1.0]",
	"files": [
		{
			"byteSize": 382.0,
			"hashcode": "535d5233aa70cfa9de896e7275ceb944",
			"name": "labelling-agents.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CA2_pub/v1.0/labelling-agents.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "93d0eeab689d22a9d05ae4eb360a4a6e"
		},
		{
			"byteSize": 168.0,
			"hashcode": "701ba8a5c233a374f7b62b8919ff582f",
			"name": "tissue-samples.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CA2_pub/v1.0/tissue-samples.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "dc8736b61ff9074874935518983fe553"
		},
		{
			"byteSize": 270.0,
			"hashcode": "1d67331ea3a8aaf1eba29d974cfe44df",
			"name": "subjects.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CA2_pub/v1.0/subjects.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "ff1dede7313d846a04e81e6f0ac42a4d"
		},
		{
			"byteSize": 177.0,
			"hashcode": "33c60a8061d2043ad006b0887db531a9",
			"name": "CA2_fp_20190410.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CA2_pub/v1.0/CA2_fp_20190410.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "5b3a6ae7379e51ea7f598d2867dae33f"
		},
		{
			"byteSize": 676.0,
			"hashcode": "05f168b0731d00a4ac54734fecf8edb6",
			"name": "receptors.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CA2_pub/v1.0/receptors.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "ed9df1a31c9733eddf3f3aece751d98c"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/6af93f57-b16d-436f-b05c-6d8bb4385250",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "6af93f57-b16d-436f-b05c-6d8bb4385250",
	"kgReference": [
		"10.25493/M85E-VXR"
	],
	"publications": []
}
