{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Scheperjans, F., Eickhoff, S. B., Hoemke, L., Mohlberg, H., Hermann, K., Amunts, K., & Zilles, K. (2008). Probabilistic Maps, Morphometry, and Variability of Cytoarchitectonic Areas in the Human Superior Parietal Cortex. Cerebral Cortex, 18(9), 2141\u20132157. ",
			"doi": "10.1093/cercor/bhm241"
		},
		{
			"cite": "Scheperjans, F., Hermann, K., Eickhoff, S. B., Amunts, K., Schleicher, A., & Zilles, K. (2007). Observer-Independent Cytoarchitectonic Mapping of the Human Superior Parietal Cortex. Cerebral Cortex, 18(4), 846\u2013867. ",
			"doi": "10.1093/cercor/bhm116"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area 7A (SPL) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area 7A (SPL). The probability map of Area 7A (SPL) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area 7A (SPL):\nScheperjans et al. (2018) [Data set, v8.2] [DOI: 10.25493/MQ4V-YRR](https://doi.org/10.25493%2FMQ4V-YRR)\n\nThe most probable delineation of Area 7A (SPL) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 7A (SPL)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/e26e999f-77ad-4934-9569-8290ed05ebda"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area 7A (SPL) (v8.4)",
	"files": [
		{
			"byteSize": 158374.0,
			"hashcode": "c3e875a3a0618ad375cce35480d72412",
			"name": "Area-7A_l_N10_nlin2MNI152ASYM2009C_8.4_publicP_0120e6ac53deea83a5203a7368a35b31.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7A_pub/8.4/Area-7A_l_N10_nlin2MNI152ASYM2009C_8.4_publicP_0120e6ac53deea83a5203a7368a35b31.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "0120e6ac53deea83a5203a7368a35b31"
		},
		{
			"byteSize": 150322.0,
			"hashcode": "9232cef60760e4cbfca2492c1a77ef76",
			"name": "Area-7A_r_N10_nlin2Stdcolin27_8.4_publicP_1c5f7d8bdc2a3fcce61ed23e3ed2e292.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7A_pub/8.4/Area-7A_r_N10_nlin2Stdcolin27_8.4_publicP_1c5f7d8bdc2a3fcce61ed23e3ed2e292.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "1c5f7d8bdc2a3fcce61ed23e3ed2e292"
		},
		{
			"byteSize": 132866.0,
			"hashcode": "99015d7b612ef2623c91f8689fa27b4e",
			"name": "Area-7A_r_N10_nlin2MNI152ASYM2009C_8.4_publicP_b5a68034a7ab7caabf5fe1909cbb87dd.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7A_pub/8.4/Area-7A_r_N10_nlin2MNI152ASYM2009C_8.4_publicP_b5a68034a7ab7caabf5fe1909cbb87dd.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "b5a68034a7ab7caabf5fe1909cbb87dd"
		},
		{
			"byteSize": 169966.0,
			"hashcode": "399ef7e22448c3044e1f5464856057dd",
			"name": "Area-7A_l_N10_nlin2Stdcolin27_8.4_publicP_8d57d1cf5be46a085868adc403ad297f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7A_pub/8.4/Area-7A_l_N10_nlin2Stdcolin27_8.4_publicP_8d57d1cf5be46a085868adc403ad297f.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "8d57d1cf5be46a085868adc403ad297f"
		},
		{
			"byteSize": 23.0,
			"hashcode": "54ec7e7aa90e8676d2af3efaf65a38fb",
			"name": "subjects_Area-7A.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7A_pub/8.4/subjects_Area-7A.csv",
			"contentType": "text/csv",
			"hash": "499f1e375313a21192c78e14fe30bfc9"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/f2635688-3894-471f-88db-c065871582de",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "274457d14fdc8db443d4765a3ac061ba",
			"name": "Hoemke, L.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/323aa606-da3a-42a1-b16f-e07887dc43ec",
			"shortName": "Hoemke, L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "f6062e66693aad8bce7a1412ab016a73",
			"name": "Hermann, K.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/58065272-f536-4e39-90e5-77f7bf034e0c",
			"shortName": "Hermann, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7be384119cf6b1bfbff17cdda3476f2e",
			"name": "Scheperjans, Filip",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/eac78bec-6b4c-4652-bea0-0e927cd558b2",
			"shortName": "Scheperjans, F."
		}
	],
	"id": "f2635688-3894-471f-88db-c065871582de",
	"kgReference": [
		"10.25493/7HX2-AJH"
	],
	"publications": [
		{
			"name": "Probabilistic Maps, Morphometry, and Variability of Cytoarchitectonic Areas in the Human Superior Parietal Cortex",
			"cite": "Scheperjans, F., Eickhoff, S. B., Hoemke, L., Mohlberg, H., Hermann, K., Amunts, K., & Zilles, K. (2008). Probabilistic Maps, Morphometry, and Variability of Cytoarchitectonic Areas in the Human Superior Parietal Cortex. Cerebral Cortex, 18(9), 2141\u20132157. ",
			"doi": "10.1093/cercor/bhm241"
		},
		{
			"name": "Observer-Independent Cytoarchitectonic Mapping of the Human Superior Parietal Cortex",
			"cite": "Scheperjans, F., Hermann, K., Eickhoff, S. B., Amunts, K., Schleicher, A., & Zilles, K. (2007). Observer-Independent Cytoarchitectonic Mapping of the Human Superior Parietal Cortex. Cerebral Cortex, 18(4), 846\u2013867. ",
			"doi": "10.1093/cercor/bhm116"
		}
	]
}
