{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Henssen, A., Zilles, K., Palomero-Gallagher, N., Schleicher, A., Mohlberg, H., Gerboga, F., \u2026 Amunts, K. (2016). Cytoarchitecture and probability maps of the human medial orbitofrontal cortex. Cortex, 75, 87\u2013112. ",
			"doi": "10.1016/j.cortex.2015.11.006"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Fo2 (OFC) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area Fo2 (OFC). The probability map of Area Fo2 (OFC) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area Fo2 (OFC):\nHenssen et al. (2018) [Data set, v3.2] [DOI: 110.25493/N14D-JQT](https://doi.org/10.25493%2FN14D-JQT)\nHenssen et al. (2019) [Data set, v3.4] [DOI: 10.25493/3JB9-2V2](https://doi.org/10.25493%2F3JB9-2V2)\n\nThe most probable delineation of Area Fo2 (OFC) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Fo2 (OFC)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/30a04d2b-58e1-43d7-8b8f-1f0b598382d0"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Fo2 (OFC) (v5.0)",
	"files": [
		{
			"byteSize": 22.0,
			"hashcode": "03268a53b0d79abc96b79cb86072352b",
			"name": "subjects_Area-Fo2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo2_pub/5.0/subjects_Area-Fo2.csv",
			"contentType": "text/csv",
			"hash": "c0ca3500dacdbb7c92472bcc903e78bb"
		},
		{
			"byteSize": 95970.0,
			"hashcode": "c5e054105c29fb788d84ff7a4d2be18d",
			"name": "Area-Fo2_r_N10_nlin2Stdcolin27_5.0_publicP_2a174d5759d139042d1ebc1f76a85e97.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo2_pub/5.0/Area-Fo2_r_N10_nlin2Stdcolin27_5.0_publicP_2a174d5759d139042d1ebc1f76a85e97.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "2a174d5759d139042d1ebc1f76a85e97"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "2b44d9580d059bd95ae8255335428a0b",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo2_pub/5.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 97848.0,
			"hashcode": "761f25de3be992abbcedc2bb41a5d805",
			"name": "Area-Fo2_l_N10_nlin2Stdcolin27_5.0_publicP_f49ae5943f29ef5262dd2a6ddd55ebb6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo2_pub/5.0/Area-Fo2_l_N10_nlin2Stdcolin27_5.0_publicP_f49ae5943f29ef5262dd2a6ddd55ebb6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "f49ae5943f29ef5262dd2a6ddd55ebb6"
		},
		{
			"byteSize": 68684.0,
			"hashcode": "633c527590549536569b32bb88a86fc9",
			"name": "Area-Fo2_l_N10_nlin2ICBM152asym2009c_5.0_publicP_6a48a02c490c05cce96893ebfebed8fc.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo2_pub/5.0/Area-Fo2_l_N10_nlin2ICBM152asym2009c_5.0_publicP_6a48a02c490c05cce96893ebfebed8fc.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "6a48a02c490c05cce96893ebfebed8fc"
		},
		{
			"byteSize": 66542.0,
			"hashcode": "8786674bc214c31628c6536be2bfbf81",
			"name": "Area-Fo2_r_N10_nlin2ICBM152asym2009c_5.0_publicP_37d021e9fef741a8c87b34429f0b9deb.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo2_pub/5.0/Area-Fo2_r_N10_nlin2ICBM152asym2009c_5.0_publicP_37d021e9fef741a8c87b34429f0b9deb.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "37d021e9fef741a8c87b34429f0b9deb"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/2e66adfa-310f-43d5-8866-2a1919b39c37",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "4afc7cc46bd5d84015409461ffe716e2",
			"name": "Gerboga, Fatma",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/b4a1f498-d9af-4eab-9df3-315944012d0e",
			"shortName": "Gerboga, F."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "e9a1ae9d473cf1b79677cacc5938a84d",
			"name": "Henssen, Anton",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8b620030-94eb-4e88-9f63-8bf79c9a11f7",
			"shortName": "Henssen, A."
		}
	],
	"id": "2e66adfa-310f-43d5-8866-2a1919b39c37",
	"kgReference": [
		"10.25493/PF48-73B"
	],
	"publications": [
		{
			"name": "Cytoarchitecture and probability maps of the human medial orbitofrontal cortex",
			"cite": "Henssen, A., Zilles, K., Palomero-Gallagher, N., Schleicher, A., Mohlberg, H., Gerboga, F., \u2026 Amunts, K. (2016). Cytoarchitecture and probability maps of the human medial orbitofrontal cortex. Cortex, 75, 87\u2013112. ",
			"doi": "10.1016/j.cortex.2015.11.006"
		}
	]
}
