{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Zaborszky, L., Hoemke, L., Mohlberg, H., Schleicher, A., Amunts, K., & Zilles, K. (2008). Stereotaxic probabilistic maps of the magnocellular cell groups in human basal forebrain. NeuroImage, 42(3), 1127\u20131141. ",
			"doi": "10.1016/j.neuroimage.2008.05.055"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Ch 123 (Basal Forebrain) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Ch 123 (Basal Forebrain). The probability map of Ch 123 (Basal Forebrain) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Ch 123 (Basal Forebrain):\nZaborszky et al. (2018) [Data set, v4.0] [DOI: 10.25493/DE24-4FC](https://doi.org/10.25493%2FDE24-4FC)\nZaborszky et al. (2019) [Data set, v4.2] [DOI: 10.25493/7SEP-P2V](https://doi.org/10.25493%2F7SEP-P2V)\n\nThe most probable delineation of Ch 123 (Basal Forebrain) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Ch 123 (Basal Forebrain)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/bb111a95-e04c-4987-8254-4af4ed8b0022"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Ch 123 (Basal Forebrain) (v4.3)",
	"files": [
		{
			"byteSize": 39467.0,
			"hashcode": "6c059d1ea664f5f0652cc7b77e292dba",
			"name": "Ch-123_l_N10_nlin2ICBM152asym2009c_4.3_publicP_13eae1ca3ab3d4a58b0b70bd882ee8a1.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ch-123_pub/4.3/Ch-123_l_N10_nlin2ICBM152asym2009c_4.3_publicP_13eae1ca3ab3d4a58b0b70bd882ee8a1.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "d7d91064a82314c41196f738a01e1110"
		},
		{
			"byteSize": 68881.0,
			"hashcode": "01fae73034206cec9321d62847cbbac7",
			"name": "Ch-123_l_N10_nlin2Stdcolin27_4.3_publicP_13eae1ca3ab3d4a58b0b70bd882ee8a1.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ch-123_pub/4.3/Ch-123_l_N10_nlin2Stdcolin27_4.3_publicP_13eae1ca3ab3d4a58b0b70bd882ee8a1.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "13eae1ca3ab3d4a58b0b70bd882ee8a1"
		},
		{
			"byteSize": 41022.0,
			"hashcode": "fcfda737500804feee307e42dbef913f",
			"name": "Ch-123_r_N10_nlin2ICBM152asym2009c_4.3_publicP_90bb17b15fb026d6f067b8cf203cc049.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ch-123_pub/4.3/Ch-123_r_N10_nlin2ICBM152asym2009c_4.3_publicP_90bb17b15fb026d6f067b8cf203cc049.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "a02792309919d6b2fd6a02464342573d"
		},
		{
			"byteSize": 21.0,
			"hashcode": "6f198b6921f57a3f8c7f27fc8df19d89",
			"name": "subjects_Ch-123.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ch-123_pub/4.3/subjects_Ch-123.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "3478c327974f9e9e4ba93cc27650d7f4",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ch-123_pub/4.3/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 69941.0,
			"hashcode": "b2891e87e3c3e8926e7b8848230fa4b3",
			"name": "Ch-123_r_N10_nlin2Stdcolin27_4.3_publicP_90bb17b15fb026d6f067b8cf203cc049.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ch-123_pub/4.3/Ch-123_r_N10_nlin2Stdcolin27_4.3_publicP_90bb17b15fb026d6f067b8cf203cc049.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "90bb17b15fb026d6f067b8cf203cc049"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/33c461a4-0b40-458f-a25e-9f2fe37ff6ff",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "274457d14fdc8db443d4765a3ac061ba",
			"name": "Hoemke, L.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/323aa606-da3a-42a1-b16f-e07887dc43ec",
			"shortName": "Hoemke, L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "5b43785b16504b1ee97d3f2bca0f465e",
			"name": "Zaborszky, Laszlo",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/046690a9-5dce-4bd4-913e-aba36c242f19",
			"shortName": "Zaborszky, L."
		}
	],
	"id": "33c461a4-0b40-458f-a25e-9f2fe37ff6ff",
	"kgReference": [
		"10.25493/19M1-B4B"
	],
	"publications": [
		{
			"name": "Stereotaxic probabilistic maps of the magnocellular cell groups in human basal forebrain",
			"cite": "Zaborszky, L., Hoemke, L., Mohlberg, H., Schleicher, A., Amunts, K., & Zilles, K. (2008). Stereotaxic probabilistic maps of the magnocellular cell groups in human basal forebrain. NeuroImage, 42(3), 1127\u20131141. ",
			"doi": "10.1016/j.neuroimage.2008.05.055"
		}
	]
}
