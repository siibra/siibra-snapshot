{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic  Area STS2 (STS) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to  Area STS2 (STS). The probability map of  Area STS2 (STS) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area STS2 (STS):\nZachlod et al. (2019) [Data set, v3.0] [DOI: 10.25493/Y5QZ-KV](https://doi.org/10.25493%2FY5QZ-KV)\nZachlod et al. (2019) [Data set, v3.1] [DOI: 10.25493/KHY9-J3Y](https://doi.org/10.25493%2FKHY9-J3Y)\nZachlod et al. (2020) [Data set, v5.0] [10.25493/1E12-161](https://doi.org/10.25493%2F1E12-161)\n\nThe most probable delineation of Area STS2 (STS) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area STS2 (STS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/278fc30f-2e24-4046-856b-95dfaf561635"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area STS2 (STS) (v5.3)",
	"files": [
		{
			"byteSize": 134434.0,
			"hashcode": "b49014fba4a71e1c5ada6348bf1d237c",
			"name": "Area-STS2_l_N10_nlin2ICBM152asym2009c_5.3_publicDOI_a2efa6a1493999a362c65e49e422cbdd.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS2_pub/5.3/Area-STS2_l_N10_nlin2ICBM152asym2009c_5.3_publicDOI_a2efa6a1493999a362c65e49e422cbdd.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "f41fe4c41ac1a2443fce7e4041ff02f6"
		},
		{
			"byteSize": 143301.0,
			"hashcode": "42752ce1ad1e0f488c8a39f4c5e04612",
			"name": "Area-STS2_l_N10_nlin2Stdcolin27_5.3_publicDOI_a2efa6a1493999a362c65e49e422cbdd.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS2_pub/5.3/Area-STS2_l_N10_nlin2Stdcolin27_5.3_publicDOI_a2efa6a1493999a362c65e49e422cbdd.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "a2efa6a1493999a362c65e49e422cbdd"
		},
		{
			"byteSize": 21.0,
			"hashcode": "5c52ed67307829daf687d76d62964a3e",
			"name": "subjects_Area-STS2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS2_pub/5.3/subjects_Area-STS2.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		},
		{
			"byteSize": 134332.0,
			"hashcode": "810e62ea23dcd31f50974c8c94b91d5f",
			"name": "Area-STS2_r_N10_nlin2ICBM152asym2009c_5.3_publicDOI_666102198bbbaf2e67ea9c2d01ca6483.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS2_pub/5.3/Area-STS2_r_N10_nlin2ICBM152asym2009c_5.3_publicDOI_666102198bbbaf2e67ea9c2d01ca6483.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "34610baaad755bfa4e400378d436582a"
		},
		{
			"byteSize": 143030.0,
			"hashcode": "a2c6af8d839fae67067ee009138da67b",
			"name": "Area-STS2_r_N10_nlin2Stdcolin27_5.3_publicDOI_666102198bbbaf2e67ea9c2d01ca6483.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS2_pub/5.3/Area-STS2_r_N10_nlin2Stdcolin27_5.3_publicDOI_666102198bbbaf2e67ea9c2d01ca6483.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "666102198bbbaf2e67ea9c2d01ca6483"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "63a83d8ad3ea4b9ce77ca3203e0d9c22",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS2_pub/5.3/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/58759477-8aeb-4745-aad9-f913768406f0",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "fbec7907-0560-41cb-bec6-bf376991f02b303708",
			"name": "Zachlod, Daniel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/fbec7907-0560-41cb-bec6-bf376991f02b",
			"shortName": "Zachlod, D."
		}
	],
	"id": "58759477-8aeb-4745-aad9-f913768406f0",
	"kgReference": [
		"10.25493/MEEC-HC5"
	],
	"publications": []
}
