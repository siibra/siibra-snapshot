{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Kurth, F., Eickhoff, S. B., Schleicher, A., Hoemke, L., Zilles, K., & Amunts, K. (2009). Cytoarchitecture and Probabilistic Maps of the Human Posterior Insular Cortex. Cerebral Cortex, 20(6), 1448\u20131461. ",
			"doi": "10.1093/cercor/bhp208"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Ig2 (Insula) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area Ig2 (Insula). The probability map of Area Ig2 (Insula) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area Ig2 (Insula):\nKurth et al. (2018) [Data set, v11.0] [DOI: 10.25493/N7C7-BHW](https://doi.org/10.25493%2FN7C7-BHW)\nKurth et al. (2019) [Data set, v13.1] [DOI: 10.25493/662G-E0W](https://doi.org/10.25493%2F662G-E0W)\nKurth et al. (2020) [Data set, v14.0] [DOI: 10.25493/GSH9-K78](https://doi.org/10.25493%2FGSH9-K78)\n\nThe most probable delineation of Area Ig2 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Ig2 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/49092952-1eef-4b89-b8bf-1bf1f25f149a"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Ig2 (Insula) (v14.2)",
	"files": [
		{
			"byteSize": 24.0,
			"hashcode": "8f1a0aeeb75e9e716680e8b0cb6309dd",
			"name": "subjects_Area-Ig2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/14.2/subjects_Area-Ig2.csv",
			"contentType": "text/csv",
			"hash": "5f65aff2b50cea7b07f7c426a59762b3"
		},
		{
			"byteSize": 51823.0,
			"hashcode": "3d1f1b58a43317cda8a9c9b5afc73d47",
			"name": "Area-Ig2_l_N10_nlin2ICBM152asym2009c_14.2_publicP_416abe5e03d471c5b5a94c0787e5d6ff.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/14.2/Area-Ig2_l_N10_nlin2ICBM152asym2009c_14.2_publicP_416abe5e03d471c5b5a94c0787e5d6ff.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "dc9387bffab78581b229ed8fe0155e91"
		},
		{
			"byteSize": 77379.0,
			"hashcode": "ee918ec9b22bfb1633f9ee8207ae308b",
			"name": "Area-Ig2_l_N10_nlin2Stdcolin27_14.2_publicP_416abe5e03d471c5b5a94c0787e5d6ff.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/14.2/Area-Ig2_l_N10_nlin2Stdcolin27_14.2_publicP_416abe5e03d471c5b5a94c0787e5d6ff.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "416abe5e03d471c5b5a94c0787e5d6ff"
		},
		{
			"byteSize": 79151.0,
			"hashcode": "f50f8b3eb636aafd250f7dfa367638be",
			"name": "Area-Ig2_r_N10_nlin2Stdcolin27_14.2_publicP_4bf77468853b534da8f2d40c36c5384e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/14.2/Area-Ig2_r_N10_nlin2Stdcolin27_14.2_publicP_4bf77468853b534da8f2d40c36c5384e.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4bf77468853b534da8f2d40c36c5384e"
		},
		{
			"byteSize": 52726.0,
			"hashcode": "300175342ed8a135e088291005de5949",
			"name": "Area-Ig2_r_N10_nlin2ICBM152asym2009c_14.2_publicP_4bf77468853b534da8f2d40c36c5384e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/14.2/Area-Ig2_r_N10_nlin2ICBM152asym2009c_14.2_publicP_4bf77468853b534da8f2d40c36c5384e.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "140a4585b009a55f36b239c2b12043ca"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "4b032648ec887b570bea810acc443d2c",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/14.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/29c712b6-b6e4-4899-b294-2470f22475c0",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "274457d14fdc8db443d4765a3ac061ba",
			"name": "Hoemke, L.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/323aa606-da3a-42a1-b16f-e07887dc43ec",
			"shortName": "Hoemke, L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "28cd2bbf034cb2518c8d67db992c5d64",
			"name": "Kurth, F.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9705b35c-3857-438b-b04f-f6a8ab79926c",
			"shortName": "Kurth, F."
		}
	],
	"id": "29c712b6-b6e4-4899-b294-2470f22475c0",
	"kgReference": [
		"10.25493/GQ0N-28B"
	],
	"publications": [
		{
			"name": "Cytoarchitecture and Probabilistic Maps of the Human Posterior Insular Cortex",
			"cite": "Kurth, F., Eickhoff, S. B., Schleicher, A., Hoemke, L., Zilles, K., & Amunts, K. (2009). Cytoarchitecture and Probabilistic Maps of the Human Posterior Insular Cortex. Cerebral Cortex, 20(6), 1448\u20131461. ",
			"doi": "10.1093/cercor/bhp208"
		}
	]
}
