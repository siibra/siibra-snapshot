{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Schubert N, Axer M, Schober M, Huynh A-M, Huysegoms M, Palomero-Gallagher N, Bjaalie JG, Leergaard TB, Kirlangic ME, Amunts K and Zilles K (2016) 3D Reconstructed Cyto-, Muscarinic M2 Receptor, and Fiber Architecture of the Rat Brain Registered to the Waxholm Space Atlas. Front. Neuroanat. 10:51. doi: 10.3389/fnana.2016.00051",
			"doi": "10.3389/fnana.2016.00051"
		}
	],
	"activity": [
		{
			"protocols": [],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "Waxholm Space rat brain atlas v.2.0",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/d5717c4a-0fa1-46e6-918c-b8003069ade8"
		}
	],
	"methods": [
		"Spatial atlas registration",
		"Microscopy"
	],
	"custodians": [
		{
			"schema.org/shortName": null,
			"identifier": "5d9b99cf50302c9f4b9367df398dd38f",
			"name": "Axer, Markus",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/462efcaf-c34f-45f2-b11b-c8c9249df3a7",
			"shortName": "Axer, M."
		}
	],
	"project": [
		"Wistar rat brain fibre orientation model"
	],
	"description": "The 3D fibre orientation model of a male Wistar rat brain was derived from 3D-PLI as described in Axer et al. 2011a [1].  \nThe brain was immersion fixed in 4% paraformaldehyde. After cryoprotection (10% glycerin for 3 days, followed by 20% glycerin for 14 days at +4\u00b0C), the brain was deep frozen at -50\u00b0C and stored till further processing. The brain was serially sectioned in the coronal plane (section thickness 60 \u03bcm) using a large-scale cryostat microtome (Polycut CM 3500, Leica, Germany) and coverslipped with glycerin. Immediately after coverslipping, the sections were measured using the large-area polarimeter (LAP, pixel size: 64 \u03bcm x 64 \u03bcm, cf. [1]). \nDuring sectioning, each blockface was digitized using a CCD camera mounted above the brain in order to obtain an undistorted reference image of each section. Spatial resolution in the z-direction was 60 \u03bcm. No staining was applied. This procedure resulted in an uninterrupted series of 446 sections through the entire brain, which ultimately enabled the 3D reconstruction. \n\nThe application of the Jones calculus [2] describes the light transmittance through the LAP and enables the calculation of the individual spatial fiber orientation in each voxel (defined by pixel size and section thickness). The fiber orientation is defined by the pair of angles (\u03b1, \u03c6) = (inclination, direction) indicating the fiber axis orientation out of and within the section plane, respectively. Inclination and direction angles are encoded in RGB or HSV color space to provide one fiber orientation map (FOM) per section. The entire data set of aligned FOMs (i.e. the fibre orientation model) is assembled in a single NIfTI file (http://nifti.nimh.nih.gov). FOMs are the fundamental data structure provided by 3D-PLI and have an in-plane resolution of 64 \u03bcm\u00d764 \u03bcm, and, since each section was 60 \u03bcm thick, a spatial resolution in the z-direction of 60 \u03bcm. They contain a single 3D fiber orientation vector per voxel that is interpreted as the spatial orientation of the fibers in this voxel.\n\nNon-linear deformations introduced by brain sectioning and mounting were corrected using blockface images as undistorted references for the spatial alignment of 3D-PLI FOMs. Hence, in a first step the blockface images had to be 3D reconstructed. The reconstruction method consisted of a two-phase registration: a marker-based alignment of the blockface images and a refinement of the pre-reconstructed volume using 3D information [3].\nThe 3D reconstruction of the FOMs was done in two steps: (i) a 3D affine registration ensured the correct spatial alignment of the brains and (ii) a subsequent 3D non-linear registration compensated non-linear distortions of the brain sections. Using segmented images the centers of gravity of the corresponding brain masks were calculated and aligned. Based on this initial transformation, an intensity based rigid registration was performed using mutual information as metric. The second step, the refinement, was done by means of a slice-by-slice B-Spline registration with sum of squared differences as metric and a grid size of 5 \u00d7 6 [4].\nAfterwards the fibre orientation model was transferred into the common rodent reference space, the Waxholm Space atlas [5]. The transformation of the brains into the same space was also done in the two step strategy described above.\n\n \n\n**References** \n  \n[1] Axer, M., Amunts, K., Gr\u00e4\u00dfel, D., Palm, C., Dammers, J., Axer, H., et al. (2011a). A novel approach to the human connectome: ultra-high resolution mapping of fiber tracts in the brain. NeuroImage 54, 1091\u20131101. doi: 10.1016/j.neuroimage.2010.08.075\n\n[2] Jones, RC. (1941) A new calculus for the treatment of optical systems. J. Opt. Soc. Am. 31, 488\u2013503. doi:10.1364/JOSA.31.000488\n\n[3] Schober, M., Schl\u00f6mer, P., Cremer, M., Mohlberg, H., Huynh, A.-M., Schubert, N., et al. (2015). \u201cReference volume generation for subsequent 3D reconstruction of histological sections,\u201d in Proceedings of Bildverarbeitung f\u00fcr die Medizin, (L\u00fcbeck), 143\u2013148.\n\n[4] Schubert, N., Kirlangic, M. E., Schober, M., Huynh, A.-M., Amunts, K., Zilles, K., et al. (2016). 3D Reconstructed Cyto-, Muscarinic M2 Receptor, and Fiber Architecture of the Rat Brain Registered to the Waxholm Space Atlas. Frontiers Neuroanatomy 10, 1-13. doi: 10.3389/fnana.2016.00051\n\n[5] Papp, E. A., Leergaard, T. B., Calabrese, E., Johnson, G. A., and Bjaalie, J. G. (2014). Waxholm space atlas of the Spraque Dawley rat brain. NeuroImage 97, 374\u2013386. doi: 10.1016/j.neuroimage.2014.04.001",
	"parcellationAtlas": [
		{
			"name": "Waxholm Space rat brain atlas v2",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/2449a7f0-6dd0-4b5a-8f1e-aec0db03679d",
			"id": "2449a7f0-6dd0-4b5a-8f1e-aec0db03679d"
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-sa/4.0"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [
		"Rattus norvegicus"
	],
	"name": "Wistar rat brain fibre orientation model",
	"files": [
		{
			"byteSize": 362206000.0,
			"hashcode": "2bdc72c2c9e537c602919db7105f14f9",
			"name": "R2022_reco_foms_RGB.nii",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/hbp-d00006_Wistar-rat-brain-fiber-orient_pub/R2022_reco_foms_RGB.nii",
			"contentType": "application/octet-stream",
			"hash": "a09370c33973738251f909e96914d4ae"
		},
		{
			"byteSize": 190030780.0,
			"hashcode": "d9977b521695ca65714e322ccded766f",
			"name": "R2022_reco_blockface.nii",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/hbp-d00006_Wistar-rat-brain-fiber-orient_pub/R2022_reco_blockface.nii",
			"contentType": "application/octet-stream",
			"hash": "d39e66aa9db16a4221c081b44aada4bf"
		},
		{
			"byteSize": 139987180.0,
			"hashcode": "d9f780e8f7287f01f0721ee4284601c1",
			"name": "WHS_MRI_toR2022.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/hbp-d00006_Wistar-rat-brain-fiber-orient_pub/WHS_MRI_toR2022.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "512c2800bbc21b2defb5e1b3d2475989"
		},
		{
			"byteSize": 360930628.0,
			"hashcode": "e97d099cf8546ed25b0214c0e1ec1a96",
			"name": "R2022_reco_foms_HSV.nii",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/hbp-d00006_Wistar-rat-brain-fiber-orient_pub/R2022_reco_foms_HSV.nii",
			"contentType": "application/octet-stream",
			"hash": "5f25a3f27d896ead9f54c7360177d592"
		},
		{
			"byteSize": 220539.0,
			"hashcode": "b72c18e79c3ba8d9f85193abd9aa1e4e",
			"name": "DataDescriptor-WistarRatBrainFiberOrientationModel.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/hbp-d00006_Wistar-rat-brain-fiber-orient_pub/DataDescriptor-WistarRatBrainFiberOrientationModel.pdf",
			"contentType": "application/pdf",
			"hash": "b0f49ebe4250f7c63de2e53f3d67c15b"
		},
		{
			"byteSize": 3639429.0,
			"hashcode": "52dd1f5f9e35a026ecf266b785d59a16",
			"name": "WHS_atlas_toR2022.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/hbp-d00006_Wistar-rat-brain-fiber-orient_pub/WHS_atlas_toR2022.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "61b26db79caa42d7b21e693e16225b9c"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/a3b84a34-6afc-4ef7-9cd0-125700aea30e",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "2a7008ce-cb2e-42b0-8579-a585496d40c0",
			"name": "Kirlangic, Mehmet E.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/2a7008ce-cb2e-42b0-8579-a585496d40c0",
			"shortName": "Kirlangic, M. E."
		},
		{
			"schema.org/shortName": null,
			"identifier": "af81c3857f9ddaeff432d30349c4565a",
			"name": "Leergaard, Trygve B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a1cb1a1f-2634-4e81-b97f-303fd55efc13",
			"shortName": "Leergaard, T.B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "cdda3cdc7b3f7ab98830205602f87db3",
			"name": "Bjaalie, Jan G.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5271206e-7dcd-4081-a49a-bc5f147028ce",
			"shortName": "Bjaalie, J.G."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": null,
			"identifier": "976e2143-3c35-4502-a47b-6a25d03befdc301597",
			"name": "Huysegoms, Marcel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/976e2143-3c35-4502-a47b-6a25d03befdc",
			"shortName": "Huysegoms, M."
		},
		{
			"schema.org/shortName": null,
			"identifier": "91984061-7928-4738-84b5-84390bf12d0f301597",
			"name": "Huynh, Anh-Minh",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/91984061-7928-4738-84b5-84390bf12d0f",
			"shortName": "Huynh, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "9dd5b86e618fd8757107c5baa2d1f9d2",
			"name": "Schober, Martin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/fffefb8d-ab40-48e3-a527-ada4c1768863",
			"shortName": "Schober, M."
		},
		{
			"schema.org/shortName": null,
			"identifier": "5d9b99cf50302c9f4b9367df398dd38f",
			"name": "Axer, Markus",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/462efcaf-c34f-45f2-b11b-c8c9249df3a7",
			"shortName": "Axer, M."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3b3ad0ec-11be-44b4-bf32-13838cfa7776",
			"name": "Schubert, Nicole",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3b3ad0ec-11be-44b4-bf32-13838cfa7776",
			"shortName": "Schubert, N."
		}
	],
	"id": "a3b84a34-6afc-4ef7-9cd0-125700aea30e",
	"kgReference": [
		"10.25493/F9RX-65U"
	],
	"publications": [
		{
			"name": "3D Reconstructed Cyto-, Muscarinic M2 Receptor, and Fiber Architecture of the Rat Brain Registered to the Waxholm Space Atlas.",
			"cite": "Schubert N, Axer M, Schober M, Huynh A-M, Huysegoms M, Palomero-Gallagher N, Bjaalie JG, Leergaard TB, Kirlangic ME, Amunts K and Zilles K (2016) 3D Reconstructed Cyto-, Muscarinic M2 Receptor, and Fiber Architecture of the Rat Brain Registered to the Waxholm Space Atlas. Front. Neuroanat. 10:51. doi: 10.3389/fnana.2016.00051",
			"doi": "10.3389/fnana.2016.00051"
		}
	]
}
