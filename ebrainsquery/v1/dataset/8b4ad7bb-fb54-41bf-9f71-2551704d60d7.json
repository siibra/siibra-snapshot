{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Palomero-Gallagher, N., Hoffstaedter, F., Mohlberg, H., Eickhoff, S.B., Amunts, K., Zilles, K. (2018). Human Pregenual Anterior Cingulate Cortex: Structural, Functional, and Connectional Heterogeneity. Cereb Cortex",
			"doi": "10.1093/cercor/bhy124"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic areas pv24c, pd24cv and pd24cd (combined to a single probability map named Area p24c (pACC)) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area p24c (pACC). The probability map of Area p24c (pACC) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area p24c (pACC):\nPalomero-Gallagher et al. (2019) [Data set, v16.0] [DOI: 10.25493/YSP3-WR3](https://doi.org/10.25493%2FYSP3-WR3)\nPalomero-Gallagher et al. (2019) [Data set, v16.1] [DOI: 10.25493/QA7B-JM9](https://doi.org/10.25493%2FQA7B-JM9)\nPalomero-Gallagher et al. (2020) [Data set, v18.0] [DOI: 10.25493/Z7HZ-CHD](https://doi.org/10.25493%2FZ7HZ-CHD)\n\nThe most probable delineation of Area p24c (pACC) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area p24c (pACC)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/e6507a3d-f2f8-4c17-84ff-0e7297e836a0"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area p24c (pACC) (v20.1)",
	"files": [
		{
			"byteSize": 99211.0,
			"hashcode": "765ce0e7afe10c4fc3eda70fcd865ab5",
			"name": "Area-p24c_r_N10_nlin2Stdcolin27_20.1_publicP_053903002f4c9addfae39f9d0a1492c3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-p24c_pub/20.1/Area-p24c_r_N10_nlin2Stdcolin27_20.1_publicP_053903002f4c9addfae39f9d0a1492c3.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "053903002f4c9addfae39f9d0a1492c3"
		},
		{
			"byteSize": 75886.0,
			"hashcode": "959141fb6d7f6b09a69eb0178e232900",
			"name": "Area-p24c_l_N10_nlin2ICBM152asym2009c_20.1_publicP_49eee9fc3255dbae702d8b10f26740b6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-p24c_pub/20.1/Area-p24c_l_N10_nlin2ICBM152asym2009c_20.1_publicP_49eee9fc3255dbae702d8b10f26740b6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "f2230dfb029de1983f98ea77e004d6a6"
		},
		{
			"byteSize": 96225.0,
			"hashcode": "1f24635c4757d67c72b5035034ba895c",
			"name": "Area-p24c_l_N10_nlin2Stdcolin27_20.1_publicP_49eee9fc3255dbae702d8b10f26740b6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-p24c_pub/20.1/Area-p24c_l_N10_nlin2Stdcolin27_20.1_publicP_49eee9fc3255dbae702d8b10f26740b6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "49eee9fc3255dbae702d8b10f26740b6"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "274e68488741dac3738e6c5196f53352",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-p24c_pub/20.1/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 25.0,
			"hashcode": "d5e8d791d8cfff99490c9b2766a0638f",
			"name": "subjects_Area-p24c.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-p24c_pub/20.1/subjects_Area-p24c.csv",
			"contentType": "text/csv",
			"hash": "ffd504b3c0f37d8e6d7e02d0a9a6b36c"
		},
		{
			"byteSize": 84054.0,
			"hashcode": "27aa1011c7318f84709793accbbb4572",
			"name": "Area-p24c_r_N10_nlin2ICBM152asym2009c_20.1_publicP_053903002f4c9addfae39f9d0a1492c3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-p24c_pub/20.1/Area-p24c_r_N10_nlin2ICBM152asym2009c_20.1_publicP_053903002f4c9addfae39f9d0a1492c3.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "f9c60125d27b39da34581d81bcf06b85"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/8b4ad7bb-fb54-41bf-9f71-2551704d60d7",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Hoffstaedter, F.",
			"identifier": "fea7d2c30b595f5a45a7093d7beea7b7",
			"name": "Hoffstaedter, Felix",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/86af969b-c570-4f51-80ba-571de55e817b",
			"shortName": "Hoffstaedter, F."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "8b4ad7bb-fb54-41bf-9f71-2551704d60d7",
	"kgReference": [
		"10.25493/DT62-6XA"
	],
	"publications": [
		{
			"name": "Human Pregenual Anterior Cingulate Cortex: Structural, Functional, and Connectional Heterogeneity",
			"cite": "Palomero-Gallagher, N., Hoffstaedter, F., Mohlberg, H., Eickhoff, S.B., Amunts, K., Zilles, K. (2018). Human Pregenual Anterior Cingulate Cortex: Structural, Functional, and Connectional Heterogeneity. Cereb Cortex",
			"doi": "10.1093/cercor/bhy124"
		}
	]
}
