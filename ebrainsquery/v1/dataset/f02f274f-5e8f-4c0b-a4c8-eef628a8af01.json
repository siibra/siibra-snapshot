{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Amunts, K., Kedo, O., Kindler, M., Pieperhoff, P., Mohlberg, H., Shah, N. J., \u2026 Zilles, K. (2005). Cytoarchitectonic mapping of the human amygdala, hippocampal region and entorhinal cortex: intersubject variability and probability maps. Anatomy and Embryology, 210(5-6), 343\u2013352. ",
			"doi": "10.1007/s00429-005-0025-5"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic CA (Hippocampus) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to CA (Hippocampus). The probability map of CA (Hippocampus) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nThe most probable delineation of CA (Hippocampus) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "CA (Hippocampus)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/b6fa12a6-85f4-4e2d-bb44-ef2835f7e67d"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of CA (Hippocampus) (v11.1)",
	"files": [
		{
			"byteSize": 131079.0,
			"hashcode": "3df3239c6005ed123fa5cb27549d7624",
			"name": "CA_l_N10_nlin2Stdcolin27_11.1_publicP_171cb45e2e19686d663f24ede52ef4d5.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA_pub/11.1/CA_l_N10_nlin2Stdcolin27_11.1_publicP_171cb45e2e19686d663f24ede52ef4d5.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "171cb45e2e19686d663f24ede52ef4d5"
		},
		{
			"byteSize": 116180.0,
			"hashcode": "f1592bb8d0247df320f8151feef5ff8f",
			"name": "CA_r_N10_nlin2MNI152ASYM2009C_11.1_publicP_15fe605957c7968f858481c082673d8f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA_pub/11.1/CA_r_N10_nlin2MNI152ASYM2009C_11.1_publicP_15fe605957c7968f858481c082673d8f.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "15fe605957c7968f858481c082673d8f"
		},
		{
			"byteSize": 114773.0,
			"hashcode": "935f0fe260dc29a5b3094a431268ff03",
			"name": "CA_l_N10_nlin2MNI152ASYM2009C_11.1_publicP_fd49776deee300e6dd94db513592d716.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA_pub/11.1/CA_l_N10_nlin2MNI152ASYM2009C_11.1_publicP_fd49776deee300e6dd94db513592d716.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "fd49776deee300e6dd94db513592d716"
		},
		{
			"byteSize": 21.0,
			"hashcode": "17ee1e0cf59c1cd37030e1ab9e624153",
			"name": "subjects_CA.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA_pub/11.1/subjects_CA.csv",
			"contentType": "text/csv",
			"hash": "b53cceeb6a14da4074456a7f89f87ab9"
		},
		{
			"byteSize": 134971.0,
			"hashcode": "db5747a165831133fd13462adb3b92f2",
			"name": "CA_r_N10_nlin2Stdcolin27_11.1_publicP_0852971ef836a0f321ed9f16ef1a26db.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA_pub/11.1/CA_r_N10_nlin2Stdcolin27_11.1_publicP_0852971ef836a0f321ed9f16ef1a26db.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "0852971ef836a0f321ed9f16ef1a26db"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/f02f274f-5e8f-4c0b-a4c8-eef628a8af01",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c05e03ddce0de72b2b2b6c3c3b9ca01e",
			"name": "Schneider, F.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d3d619d9-a1fa-4795-abc3-ec78c730cd8a",
			"shortName": "Schneider, F."
		},
		{
			"schema.org/shortName": null,
			"identifier": "f0d8c279b7687c2649debfc4435f110a",
			"name": "Habel, U.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bc3c1bc5-8186-4f81-af2d-dcb1fb55c357",
			"shortName": "Habel, U."
		},
		{
			"schema.org/shortName": null,
			"identifier": "572527acec93ccba7befe3b8fa72d4ca",
			"name": "Shah, Nadim J.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3048fc49-e9c7-43a7-bf7e-ab0aafc6facc",
			"shortName": "Shah, N. J."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "8f85d02ddf1de227f39d91a83c67f28c",
			"name": "Pieperhoff, Peter",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5d838c31-6dcf-44a6-b16b-4d19d770c36b",
			"shortName": "Pieperhoff, P."
		},
		{
			"schema.org/shortName": null,
			"identifier": "e05394ccaa2664b012ee2389e425455e",
			"name": "Kindler, M.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/6f1924d2-616a-45cc-a5c6-54fc3a4e450b",
			"shortName": "Kindler, M."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3e871e11c214a058a43cb1fc38788eea",
			"name": "Kedo, O.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9dabc7a3-fa77-481f-ab9e-8ef6ac6e7d42",
			"shortName": "Kedo, O."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"id": "f02f274f-5e8f-4c0b-a4c8-eef628a8af01",
	"kgReference": [
		"10.25493/B85T-D88"
	],
	"publications": [
		{
			"name": "Cytoarchitectonic mapping of the human amygdala, hippocampal region and entorhinal cortex: intersubject variability and probability maps",
			"cite": "Amunts, K., Kedo, O., Kindler, M., Pieperhoff, P., Mohlberg, H., Shah, N. J., \u2026 Zilles, K. (2005). Cytoarchitectonic mapping of the human amygdala, hippocampal region and entorhinal cortex: intersubject variability and probability maps. Anatomy and Embryology, 210(5-6), 343\u2013352. ",
			"doi": "10.1007/s00429-005-0025-5"
		}
	]
}
