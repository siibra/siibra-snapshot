{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Palomero-Gallagher, N., Eickhoff, S. B., Hoffstaedter, F., Schleicher, A., Mohlberg, H., Vogt, B. A., \u2026 Zilles, K. (2015). Functional organization of human subgenual cortical areas: Relationship between architectonical segregation and connectional heterogeneity. NeuroImage, 115, 177\u2013190. ",
			"doi": "10.1016/j.neuroimage.2015.04.053"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area 25 (sACC) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area 25 (sACC). The probability map of Area 25 (sACC) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area 25 (sACC):\nPalomero-Gallagher et al. (2019) [Data set, v16.1] [DOI: 10.25493/51AM-WN4](https://doi.org/10.25493%2F51AM-WN4)\n\nThe most probable delineation of Area 25 (sACC) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 25 (sACC)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/9010ef76-accd-4308-9951-f37b6a10f42b"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area 25 (sACC) (v16.0)",
	"files": [
		{
			"byteSize": 44960.0,
			"hashcode": "4acf5d0fd16b967040da893a5eeedc17",
			"name": "Area-25_r_N10_nlin2ICBM152ASYM2009C_16.0_publicP_4ebbe488e665f9e7cbe7f50155054d26.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-25_pub/16.0/Area-25_r_N10_nlin2ICBM152ASYM2009C_16.0_publicP_4ebbe488e665f9e7cbe7f50155054d26.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "4ebbe488e665f9e7cbe7f50155054d26"
		},
		{
			"byteSize": 75916.0,
			"hashcode": "25d7c3f7b7552b79024a250293b9bc62",
			"name": "Area-25_l_N10_nlin2Stdcolin27_16.0_publicP_e1de38b836b599246dac4e59abd53362.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-25_pub/16.0/Area-25_l_N10_nlin2Stdcolin27_16.0_publicP_e1de38b836b599246dac4e59abd53362.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "e1de38b836b599246dac4e59abd53362"
		},
		{
			"byteSize": 75979.0,
			"hashcode": "247aff386a8f518e95b8ce46b849eb92",
			"name": "Area-25_r_N10_nlin2Stdcolin27_16.0_publicP_0e1ddebb5df71aa8d1a22f5cf6914d19.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-25_pub/16.0/Area-25_r_N10_nlin2Stdcolin27_16.0_publicP_0e1ddebb5df71aa8d1a22f5cf6914d19.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "0e1ddebb5df71aa8d1a22f5cf6914d19"
		},
		{
			"byteSize": 25.0,
			"hashcode": "59691f4a279a4ecb920a90ae62800bb1",
			"name": "subjects_Area-25.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-25_pub/16.0/subjects_Area-25.csv",
			"contentType": "text/csv",
			"hash": "ffd504b3c0f37d8e6d7e02d0a9a6b36c"
		},
		{
			"byteSize": 45677.0,
			"hashcode": "fe7e7448adfeab3fdf3de49a1857c42a",
			"name": "Area-25_l_N10_nlin2ICBM152ASYM2009C_16.0_publicP_b6877ad8b2b88e132cf93c95b901f648.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-25_pub/16.0/Area-25_l_N10_nlin2ICBM152ASYM2009C_16.0_publicP_b6877ad8b2b88e132cf93c95b901f648.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "b6877ad8b2b88e132cf93c95b901f648"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/56ffb2e9-a1a8-44c9-a5ad-b6ce62f88971",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "34c04d30add1e771ef4cedf54c59d89d",
			"name": "Vogt, Brent A.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5f88e480-660a-4b6e-a6da-57979529ab6f",
			"shortName": "Vogt, B."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Hoffstaedter, F.",
			"identifier": "fea7d2c30b595f5a45a7093d7beea7b7",
			"name": "Hoffstaedter, Felix",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/86af969b-c570-4f51-80ba-571de55e817b",
			"shortName": "Hoffstaedter, F."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "4cff63f6a270b19f7f541e100e48913c",
	"kgReference": [
		"10.25493/NMPJ-EU"
	],
	"publications": [
		{
			"name": "Functional organization of human subgenual cortical areas: Relationship between architectonical segregation and connectional heterogeneity",
			"cite": "Palomero-Gallagher, N., Eickhoff, S. B., Hoffstaedter, F., Schleicher, A., Mohlberg, H., Vogt, B. A., \u2026 Zilles, K. (2015). Functional organization of human subgenual cortical areas: Relationship between architectonical segregation and connectional heterogeneity. NeuroImage, 115, 177\u2013190. ",
			"doi": "10.1016/j.neuroimage.2015.04.053"
		}
	]
}
