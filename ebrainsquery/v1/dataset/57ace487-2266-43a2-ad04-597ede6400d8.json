{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "BigBrain",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/a1655b99-82f1-420f-a3c2-fe80fd4c8588"
		}
	],
	"methods": [
		"magnetic resonance imaging (MRI)",
		"silver staining",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Ultrahigh resolution 3D maps of cytoarchitectonic areas in the Big Brain model"
	],
	"description": "This dataset contains cytoarchitectonic maps of Area TE 1.2 (HESCHL) in the Big Brain dataset [Amunts et al. 2013]. The mappings were created using the semi-automatic method presented in Schleicher et al. 1999, based on coronal histological sections on 1 micron resolution. Mappings are available on approximately every 15-60th section of this region. They were then aligned to the corresponding sections of the 3D reconstructed Big Brain space, using the transformations used in Amunts et al. 2013, kindly provided by Claude Lepage (McGill University).\n\nFrom these delineations, a preliminary 3D map of Area TE 1.2 (HESCHL) has been created by simple interpolation of the coronal contours in the 3D anatomical space of the Big Brain. This map gives a first impression of the location of this area in the Big Brain, and can be viewed in the atlas viewer using the URL below.\n\nA full mapping of this area in every histological section using a Deep Learning approach is in progress.\n\n\n**Additional information:**\nThe reference delineations used for this map are part of the work for the corresponding probabilistic map of Area TE 1.2 (HESCHL) of the JuBrain Cytoarchitectonic Atlas, published in:\nAmunts et al. (2018) [Data set] [DOI: 10.25493/T1WA-MBT](https://doi.org/10.25493%2FT1WA-MBT)\nAmunts et al. (2019) [Data set, v5.1] [DOI: 10.25493/R382-617](https://doi.org/10.25493%2FR382-617)\n\nIn addition, the dataset of the probabilistic cytoarchitectonic map of Area TE 1.2 (HESCHL) is part of the following research publications:\nRademacher, J., Morosan, P., Schormann, T., Schleicher, A., Werner, C., Freund, H.-J., & Zilles, K. (2001). Probabilistic Mapping and Volume Measurement of Human Primary Auditory Cortex. NeuroImage, 13(4), 669\u2013683.  [DOI: 10.1006/nimg.2000.0714](https://doi.org/10.1006%2Fnimg.2000.0714)\n\nMorosan, P., Rademacher, J., Schleicher, A., Amunts, K., Schormann, T., & Zilles, K. (2001). Human Primary Auditory Cortex: Cytoarchitectonic Subdivisions and Mapping into a Spatial Reference System. NeuroImage, 13(4), 684\u2013701. [DOI: 10.1006/nimg.2000.0715](https://doi.org/10.1006%2Fnimg.2000.0715)\n",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 1.2 (HESCHL)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/677cd48c-70fa-4bbd-9f0a-ffdc7744bc0f"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Interpolated 3D map of Area TE 1.2 (HESCHL) in the BigBrain",
	"files": [
		{
			"byteSize": 993037.0,
			"hashcode": "851e4c3bb24b220379d4c08dc0c6c7f6",
			"name": "preview_volume_auditory_Te12.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-TE-1.2/volumes/preview_volume_auditory_Te12.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "990631929074ccc8a83af85f8b5dac41"
		},
		{
			"byteSize": 103024312.0,
			"hashcode": "a23afcda3596e0bcc2c4f223af72162a",
			"name": "volume_auditory_Te12.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-TE-1.2/volumes/volume_auditory_Te12.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "52cb6856cfaea82ddfc87f6e6f8acd1e"
		},
		{
			"byteSize": 90124.0,
			"hashcode": "2d5350d85d98ccdcc0e6985f26b092e4",
			"name": "auditory_Te12.zip",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-TE-1.2/annotations/auditory_Te12.zip",
			"contentType": "application/zip",
			"hash": "6532fd54905eb9262e4ac73929648a5e"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/57ace487-2266-43a2-ad04-597ede6400d8",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "49e0e76dfff178efd76fd0325be98cd9",
			"name": "Freund, H.-J.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f1fc023f-5f78-4ce3-89a1-04de6b767a2e",
			"shortName": "Freund, H."
		},
		{
			"schema.org/shortName": "Werner, C.",
			"identifier": "c1ffe49cbf938cecbeee703a6602cb2c",
			"name": "Werner, C.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/687b9def-7ed0-48fe-bb24-f779bc9a94bb",
			"shortName": "Werner, C."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3a2dcba69606495850f0112bf23c4b03",
			"name": "Rademacher, J\u00f6rg",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/aa01de8e-a6de-4c9c-af62-8746a3e29ee1",
			"shortName": "Rademacher, J."
		},
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		}
	],
	"id": "57ace487-2266-43a2-ad04-597ede6400d8",
	"kgReference": [
		"10.25493/SGGR-3GU"
	],
	"publications": []
}
