{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Amunts, K., Malikovic, A., Mohlberg, H., Schormann, T., & Zilles, K. (2000). Brodmann\u2019s Areas 17 and 18 Brought into Stereotaxic Space\u2014Where and How Variable? NeuroImage, 11(1), 66\u201384. ",
			"doi": "10.1006/nimg.1999.0516"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area hOc2 (V2, 18) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area hOc2 (V2, 18). The probability map of Area hOc2 (V2, 18) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area hOc2 (V2, 18):\nAmunts et al. (2019) [Data set, v2.4] [DOI: 10.25493/QG9C-THD](https://doi.org/10.25493%2FQG9C-THD)\n\nThe most probable delineation of Area hOc2 (V2, 18) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hOc2 (V2, 18)",
			"alias": "Area hOc2 (V2, 18)",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/04674a3c-bb3a-495e-a466-206355e630bd"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area hOc2 (V2, 18) (v2.2)",
	"files": [
		{
			"byteSize": 237865.0,
			"hashcode": "d01785bbc4943388a97dbb61d6ba3089",
			"name": "Area-hOc2_l_N10_nlin2Stdcolin27_2.2_publicP_602c7427a7b4d168999f7d29b48113d7.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.2/Area-hOc2_l_N10_nlin2Stdcolin27_2.2_publicP_602c7427a7b4d168999f7d29b48113d7.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "602c7427a7b4d168999f7d29b48113d7"
		},
		{
			"byteSize": 211996.0,
			"hashcode": "9cefa439c01261607930107afcd8697e",
			"name": "Area-hOc2_r_N10_nlin2ICBM152ASYM2009C_2.2_publicP_0bc2a6d27d0c0ae7555c1d85b571c9e1.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.2/Area-hOc2_r_N10_nlin2ICBM152ASYM2009C_2.2_publicP_0bc2a6d27d0c0ae7555c1d85b571c9e1.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "0bc2a6d27d0c0ae7555c1d85b571c9e1"
		},
		{
			"byteSize": 210998.0,
			"hashcode": "0cadd78e5fe8f0738cfafd4536c7935e",
			"name": "Area-hOc2_l_N10_nlin2ICBM152ASYM2009C_2.2_publicP_ccdd6f792a38668bf125c46858dd4433.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.2/Area-hOc2_l_N10_nlin2ICBM152ASYM2009C_2.2_publicP_ccdd6f792a38668bf125c46858dd4433.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "ccdd6f792a38668bf125c46858dd4433"
		},
		{
			"byteSize": 219013.0,
			"hashcode": "153a5f772222e2042c5bb908e95dcb1d",
			"name": "Area-hOc2_r_N10_nlin2Stdcolin27_2.2_publicP_b86eac420f4538ce907f54fbc7e399f7.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.2/Area-hOc2_r_N10_nlin2Stdcolin27_2.2_publicP_b86eac420f4538ce907f54fbc7e399f7.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "b86eac420f4538ce907f54fbc7e399f7"
		},
		{
			"byteSize": 20.0,
			"hashcode": "f9161afb9c5dadfbf669fc313e15fb7e",
			"name": "subjects_Area-hOc2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.2/subjects_Area-hOc2.csv",
			"contentType": "text/csv",
			"hash": "d4fd357f7807fd448ba3bc2240f5df20"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/319a40d9-6c02-4c93-b6b4-a796c11a55d3",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "0b6888c55d248a941a022961b3de4374",
			"name": "Malikovic, Aleksandar",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8267c403-6f87-4c77-b113-3bd50d848e31",
			"shortName": "Malikovic, A."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"id": "4ceee0c2684c257fa7401cd923f44b6a",
	"kgReference": [
		"10.25493/FKMC-ZX7"
	],
	"publications": [
		{
			"name": "Brodmann's Areas 17 and 18 Brought into Stereotaxic Space\u2014Where and How Variable?",
			"cite": "Amunts, K., Malikovic, A., Mohlberg, H., Schormann, T., & Zilles, K. (2000). Brodmann\u2019s Areas 17 and 18 Brought into Stereotaxic Space\u2014Where and How Variable? NeuroImage, 11(1), 66\u201384. ",
			"doi": "10.1006/nimg.1999.0516"
		}
	]
}
