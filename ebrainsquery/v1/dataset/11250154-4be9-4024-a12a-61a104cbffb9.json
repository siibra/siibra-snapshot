{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Palomero-Gallagher, N., Kedo, O., Mohlberg, H., Zilles, K., Amunts, K. (2020) Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus. Brain Struct Funct., 225(3):881-907.",
			"doi": "10.1007/s00429-019-02022-4"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic HC-Prosubiculum (Hippocampus) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to HC-Prosubiculum (Hippocampus). The probability map of HC-Prosubiculum (Hippocampus) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nThe most probable delineation of Entorhinal Cortex derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "HC-Prosubiculum (Hippocampus)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/7b97a074-3fdb-49ff-a24a-0c2b2334fd45"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of HC-Prosubiculum (Hippocampus) (v13.0) ",
	"files": [
		{
			"byteSize": 103503.0,
			"hashcode": "9e6f526cadf5b1ba95feed3bba9ff9a0",
			"name": "HC-Prosubiculum_l_N10_nlin2Stdcolin27_13.0_publicP_7f0b7cf20e7f9089b7d26198444bddf3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-HC-Prosubiculum_pub/13.0/HC-Prosubiculum_l_N10_nlin2Stdcolin27_13.0_publicP_7f0b7cf20e7f9089b7d26198444bddf3.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "7f0b7cf20e7f9089b7d26198444bddf3"
		},
		{
			"byteSize": 21.0,
			"hashcode": "4d4bfce71bea2c6ac08299688af086e4",
			"name": "subjects_HC-Prosubiculum.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-HC-Prosubiculum_pub/13.0/subjects_HC-Prosubiculum.csv",
			"contentType": "text/csv",
			"hash": "b53cceeb6a14da4074456a7f89f87ab9"
		},
		{
			"byteSize": 86978.0,
			"hashcode": "b621eaa835c3df4b5fa6fb4381502ff5",
			"name": "HC-Prosubiculum_l_N10_nlin2ICBM152asym2009c_13.0_publicP_5a3c04fa786353b9df6f21cef214fbfb.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-HC-Prosubiculum_pub/13.0/HC-Prosubiculum_l_N10_nlin2ICBM152asym2009c_13.0_publicP_5a3c04fa786353b9df6f21cef214fbfb.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "5a3c04fa786353b9df6f21cef214fbfb"
		},
		{
			"byteSize": 101208.0,
			"hashcode": "47f93ac370c400780a760ffd1f8d365f",
			"name": "HC-Prosubiculum_r_N10_nlin2Stdcolin27_13.0_publicP_9459ca49363a3ee3deb3a5ae15ddf47a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-HC-Prosubiculum_pub/13.0/HC-Prosubiculum_r_N10_nlin2Stdcolin27_13.0_publicP_9459ca49363a3ee3deb3a5ae15ddf47a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "9459ca49363a3ee3deb3a5ae15ddf47a"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "44ce6d3ab956a140d79ea80a1d6c2b69",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-HC-Prosubiculum_pub/13.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 81873.0,
			"hashcode": "f02fa03da3a69c98c91bba6c925bf057",
			"name": "HC-Prosubiculum_r_N10_nlin2ICBM152asym2009c_13.0_publicP_22868609283bfd73f81f595574bfc49e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-HC-Prosubiculum_pub/13.0/HC-Prosubiculum_r_N10_nlin2ICBM152asym2009c_13.0_publicP_22868609283bfd73f81f595574bfc49e.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "22868609283bfd73f81f595574bfc49e"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/11250154-4be9-4024-a12a-61a104cbffb9",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3e871e11c214a058a43cb1fc38788eea",
			"name": "Kedo, O.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9dabc7a3-fa77-481f-ab9e-8ef6ac6e7d42",
			"shortName": "Kedo, O."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "11250154-4be9-4024-a12a-61a104cbffb9",
	"kgReference": [
		"10.25493/XPCE-CG1"
	],
	"publications": [
		{
			"name": "Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus.",
			"cite": "Palomero-Gallagher, N., Kedo, O., Mohlberg, H., Zilles, K., Amunts, K. (2020) Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus. Brain Struct Funct., 225(3):881-907.",
			"doi": "10.1007/s00429-019-02022-4"
		}
	]
}
