{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Grodzinsky, Y., Deschamps, I., Pieperhoff, P., Iannilli, F., Agmon, G., Loewenstein, Y., Amunts, K. (2020) Logical negation mapped onto the brain. Brain Structure and Function, 225(1):19-31",
			"doi": "10.1007/s00429-019-01975-w"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Id7 (Insula) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains, obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently, the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces, where each voxel is assigned with the probability to belong to Area Id7 (Insula). The probability map of Area Id7 (Insula) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets. \n\nOther available data versions of Area Id7 (Insula):\nAmunts et al. (2019) [Data set, v6.0] [DOI: 10.25493/B2E3-JQR](https://doi.org/10.25493%2FB2E3-JQR)\n\nThe most probable delineation of Area Id7 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Id7 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/3d5729f5-55c6-412a-8fc1-41a95c71b13a"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Id7 (Insula) (v6.1)",
	"files": [
		{
			"byteSize": 86471.0,
			"hashcode": "3648c107b7ca8b034edcdf55806aadf1",
			"name": "Area-Id7_r_N10_nlin2Stdcolin27_6.1_publicDOI_683c76c33eaea809fcf576f4e8dcbbb3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/6.1/Area-Id7_r_N10_nlin2Stdcolin27_6.1_publicDOI_683c76c33eaea809fcf576f4e8dcbbb3.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "683c76c33eaea809fcf576f4e8dcbbb3"
		},
		{
			"byteSize": 56024.0,
			"hashcode": "149c0340a59f2a13a59a04adc2a39a80",
			"name": "Area-Id7_l_N10_nlin2MNI152ASYM2009C_6.1_publicDOI_ff4f0b96f9355bd5e924191a126e7faa.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/6.1/Area-Id7_l_N10_nlin2MNI152ASYM2009C_6.1_publicDOI_ff4f0b96f9355bd5e924191a126e7faa.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "ff4f0b96f9355bd5e924191a126e7faa"
		},
		{
			"byteSize": 82553.0,
			"hashcode": "e595f741ffc8163264ba6f7a63cbd068",
			"name": "Area-Id7_l_N10_nlin2Stdcolin27_6.1_publicDOI_f2f17c06c0a643ce49d98b126a619601.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/6.1/Area-Id7_l_N10_nlin2Stdcolin27_6.1_publicDOI_f2f17c06c0a643ce49d98b126a619601.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "f2f17c06c0a643ce49d98b126a619601"
		},
		{
			"byteSize": 24.0,
			"hashcode": "8c7ed6c1c73ec0ac5034f57e6b23c77e",
			"name": "subjects_Area-Id7.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/6.1/subjects_Area-Id7.csv",
			"contentType": "text/csv",
			"hash": "5f65aff2b50cea7b07f7c426a59762b3"
		},
		{
			"byteSize": 61201.0,
			"hashcode": "251601148d78860c02f73502c5d0ad78",
			"name": "Area-Id7_r_N10_nlin2MNI152ASYM2009C_6.1_publicDOI_02b5d10dbbb00b886a864301521bdd9c.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/6.1/Area-Id7_r_N10_nlin2MNI152ASYM2009C_6.1_publicDOI_02b5d10dbbb00b886a864301521bdd9c.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "02b5d10dbbb00b886a864301521bdd9c"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/2da42b86-d839-41e8-bb27-1bb7697e3868",
	"contributors": [
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "ca58d45c-c0b4-46cd-912a-82f581f77ce6303708",
			"name": "Iannilli, Francesca",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ca58d45c-c0b4-46cd-912a-82f581f77ce6",
			"shortName": "Iannilli, F."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"id": "2da42b86-d839-41e8-bb27-1bb7697e3868",
	"kgReference": [
		"10.25493/88QG-JMS"
	],
	"publications": [
		{
			"name": "Logical negation mapped onto the brain",
			"cite": "Grodzinsky, Y., Deschamps, I., Pieperhoff, P., Iannilli, F., Agmon, G., Loewenstein, Y., Amunts, K. (2020) Logical negation mapped onto the brain. Brain Structure and Function, 225(1):19-31",
			"doi": "10.1007/s00429-019-01975-w"
		}
	]
}
