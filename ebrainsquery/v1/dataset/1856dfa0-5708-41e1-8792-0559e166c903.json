{
	"formats": [],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology",
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"autoradiography - quantitative analysis",
		"autoradiography - imaging"
	],
	"custodians": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"project": [
		"Quantitative receptor data"
	],
	"description": "This dataset contains the densities (in fmol/mg protein) of receptors for classical neurotransmitters in the ventral part of Area V3 obtained by means of quantitative _in vitro_ autoradiography. The receptor densities are visualized as _fingerprints_ (**fp**), which provide the mean density and standard deviation for each of the analyzed receptor types, averaged across samples. \n\nOverview of available measurements [ **receptor** \\| **_neurotransmitter_** \\| _labeling agent_ ]:\n**AMPA** \\| **_glutamate_** \\| _[<sup>3</sup>H]AMPA_\n**kainate** \\| **_glutamate_** \\| _[<sup>3</sup>H]kainate_\n**NMDA** \\| **_glutamate_** \\| _[<sup>3</sup>H]MK-801_\n**GABA<sub>A</sub>** \\| **_GABA_** \\| _[<sup>3</sup>H]muscimol_\n**GABA<sub>B</sub>** \\| **_GABA_** \\| _[<sup>3</sup>H]CGP54626_\n**GABA<sub>A</sub>/BZ** \\| **_GABA_** \\| _[<sup>3</sup>H]flumazenil_\n**muscarinic M<sub>1</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]pirenzepine_\n**muscarinic M<sub>2</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]oxotremorine-M_\n**muscarinic M<sub>3</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]4-DAMP_\n**&#945<sub>1</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[<sup>3</sup>H]prazosin_\n**&#945<sub>2</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[<sup>3</sup>H]UK-14,304_\n**5-HT<sub>1A</sub>** \\| **_serotonin_** \\| _[<sup>3</sup>H]8-OH-DPAT_\n**5-HT<sub>2</sub>** \\| **_serotonin_** \\| _[<sup>3</sup>H]ketanserin_\n**D<sub>1</sub>** \\| **_dopamine_** \\| _[<sup>3</sup>H]SCH23390_\n\nInformation on the used tissue samples and corresponding subjects, as well as analyzed receptors accompanies the provided dataset.\n\n**For methodological details, see:**\nZilles, K. et al. (2002). Quantitative analysis of cyto- and receptorarchitecture of the human brain, pp. 573-602. In: Brain Mapping: The Methods, 2nd edition (A.W. Toga and J.C. Mazziotta, eds.). San Diego, Academic Press.\n\nPalomero-Gallagher N, Zilles K. (2018) Cyto- and receptorarchitectonic mapping of the human brain. In: Handbook of Clinical Neurology 150: 355-387",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [
		"Macaca fascicularis"
	],
	"name": "Density measurements of different receptors for ventral part of Area V3 [macaque, v1.0]",
	"files": [
		{
			"byteSize": 919.0,
			"hashcode": "6216c4deabb5d844417a1335b7fc34ce",
			"name": "labelling-agents.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-macaque-V3v_pub/v1.0/labelling-agents.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "1c3b981e32c62c87871f4660bcd4b30f"
		},
		{
			"byteSize": 177.0,
			"hashcode": "d343f840873a23248088cf403497e03b",
			"name": "subjects.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-macaque-V3v_pub/v1.0/subjects.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "7ca0e2350c4c0b0937ccf6a090965850"
		},
		{
			"byteSize": 355.0,
			"hashcode": "2321a8e111c7729ba66b4a97cf2a09c7",
			"name": "V3v_fp_20200214.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-macaque-V3v_pub/v1.0/V3v_fp_20200214.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "743cf65375d3b5d1abf5265ba4611611"
		},
		{
			"byteSize": 1553.0,
			"hashcode": "bd378f342efbea86efe3e2fe0271a5fb",
			"name": "receptors.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-macaque-V3v_pub/v1.0/receptors.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "49acd1801920c5f416ca1cfe15aabd97"
		},
		{
			"byteSize": 170.0,
			"hashcode": "f943b1624462be1ea2207689cab337cd",
			"name": "tissue-samples.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-macaque-V3v_pub/v1.0/tissue-samples.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "4ef7c90d4cc39b159bd5ce228d26b3a9"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/1856dfa0-5708-41e1-8792-0559e166c903",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "1856dfa0-5708-41e1-8792-0559e166c903",
	"kgReference": [
		"10.25493/3Q4Z-540"
	],
	"publications": []
}
