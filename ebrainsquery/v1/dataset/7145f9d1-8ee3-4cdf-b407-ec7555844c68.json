{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Ia (Insula) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces where each voxel is assigned with the probability to belong to Area Ia (Insula). The probability map of Area Ia (Insula) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area Ia (Insula):\nQuabs et al. (2019) [Data set, v3.1] [DOI: 10.25493/WW8G-T2G](https://doi.org/10.25493%2FWW8G-T2G)\n\nThe most probable delineation of Area Ia (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493/8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Ia (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/110d0d7b-cb88-48ea-9caf-863f548dbe38"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Ia (Insula) (v4.0)",
	"files": [
		{
			"byteSize": 24.0,
			"hashcode": "070c1cf31dd8e957bc43a6196c5bca7b",
			"name": "subjects_Area-Ia.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ia_pub/4.0/subjects_Area-Ia.csv",
			"contentType": "text/csv",
			"hash": "5f65aff2b50cea7b07f7c426a59762b3"
		},
		{
			"byteSize": 43540.0,
			"hashcode": "89ed27226b0ec908fba6129e08f4f412",
			"name": "Area-Ia_l_N14_nlin2ICBM152asym2009c_4.0_publicDOI_95248e36316b422d32204753a3f6ca47.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ia_pub/4.0/Area-Ia_l_N14_nlin2ICBM152asym2009c_4.0_publicDOI_95248e36316b422d32204753a3f6ca47.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "95248e36316b422d32204753a3f6ca47"
		},
		{
			"byteSize": 74244.0,
			"hashcode": "d609501c12fd6f2a1ff9cad065d1712f",
			"name": "Area-Ia_r_N14_nlin2Stdcolin27_4.0_publicDOI_575028d6c1df88c7fd79efe4f2514230.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ia_pub/4.0/Area-Ia_r_N14_nlin2Stdcolin27_4.0_publicDOI_575028d6c1df88c7fd79efe4f2514230.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "575028d6c1df88c7fd79efe4f2514230"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "f83cefd5c83308241c781bab1ec99d3c",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ia_pub/4.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 44868.0,
			"hashcode": "ddb173b00ed0da007d886ee014b54d7f",
			"name": "Area-Ia_r_N14_nlin2ICBM152asym2009c_4.0_publicDOI_536b961dbff28255ff121d8e86c52547.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ia_pub/4.0/Area-Ia_r_N14_nlin2ICBM152asym2009c_4.0_publicDOI_536b961dbff28255ff121d8e86c52547.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "536b961dbff28255ff121d8e86c52547"
		},
		{
			"byteSize": 73042.0,
			"hashcode": "ea2206b1fa23dca8cff60ffdf2ad9263",
			"name": "Area-Ia_l_N14_nlin2Stdcolin27_4.0_publicDOI_8d5da2c8a47e442eccec99cf703cb173.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ia_pub/4.0/Area-Ia_l_N14_nlin2Stdcolin27_4.0_publicDOI_8d5da2c8a47e442eccec99cf703cb173.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "8d5da2c8a47e442eccec99cf703cb173"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/7145f9d1-8ee3-4cdf-b407-ec7555844c68",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "005d97d8-cd14-4cc2-a6fc-bca56655e7e7",
			"name": "Kossakowski, Klaudia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/005d97d8-cd14-4cc2-a6fc-bca56655e7e7",
			"shortName": "Kossakowski, K."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"name": "Quabs, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"shortName": "Quabs, J."
		}
	],
	"id": "7145f9d1-8ee3-4cdf-b407-ec7555844c68",
	"kgReference": [
		"10.25493/K54A-YC9"
	],
	"publications": []
}
