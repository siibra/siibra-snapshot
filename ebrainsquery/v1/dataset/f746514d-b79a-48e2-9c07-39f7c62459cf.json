{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Schiffer, C., Spitzer, H., Kiwitz, K., Unger, N., Wagstyl, K., Evans, A. C., Harmeling, S., Amunts, K., Dickscheid, T (2021). Convolutional Neural Networks for cytoarchitectonic brain mapping at large scale. NeuroImage 118327",
			"doi": "10.1016/j.neuroimage.2021.118327"
		},
		{
			"cite": "Schleicher, A. , Zilles, K. (2005). The verticality index: A quantitative approach to the analysis of the columnar arrangement of neurons in the primate neocortex. Neocortical modularity and the cell minicolumn 181\u2013185",
			"doi": ""
		},
		{
			"cite": "Schleicher, A., Amunts, K., Geyer, S., Morosan, P., & Zilles, K. (1999). Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics. NeuroImage, 9(1), 165\u2013177. ",
			"doi": "10.1006/nimg.1998.0385"
		},
		{
			"cite": "Spitzer, H., Kiwitz, K., Amunts, K., Harmeling, S., Dickscheid, T. (2018). Improving Cytoarchitectonic Segmentation of Human Brain Areas with Self-supervised Siamese Networks. In: Frangi A., Schnabel J., Davatzikos C., Alberola-L\u00f3pez C., Fichtinger G. (eds) Medical Image Computing and Computer Assisted Intervention \u2013 MICCAI 2018. MICCAI 2018. Lecture Notes in Computer Science, vol 11072. Springer, Cham.",
			"doi": "10.1007/978-3-030-00931-1_76"
		},
		{
			"cite": "Spitzer, H., Amunts, K., Harmeling, S., and Dickscheid, T. (2017). Parcellation of visual cortex on high-resolution histological brain sections using convolutional neural networks, in 2017 IEEE 14th International Symposium on Biomedical Imaging (ISBI 2017), pp. 920\u2013923.",
			"doi": "10.1109/ISBI.2017.7950666"
		},
		{
			"cite": "Amunts, K., Lepage, C., Borgeat, L., Mohlberg, H., Dickscheid, T., Rousseau, M. E., Bludau, S., Bazin, P. L., Lewis, L. B., Oros-Peusquens, A. M., Shah, N. J., Lippert, T., Zilles, K., Evans, A. C. (2013).  BigBrain: An Ultrahigh-Resolution 3D Human Brain Model. Science, 340(6139),1472-5.",
			"doi": "10.1126/science.1235381"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"analysis technique"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "BigBrain",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/a1655b99-82f1-420f-a3c2-fe80fd4c8588"
		}
	],
	"methods": [
		"silver staining",
		"cytoarchitectonic mapping",
		"Deep-Learning based cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Ultrahigh resolution 3D maps of cytoarchitectonic areas in the Big Brain model"
	],
	"description": "This dataset contains automatically created cytoarchitectonic maps of Area hOc3v (LingG) in the BigBrain. Mappings were created using Deep Convolutional Neural networks trained on delineations on every 60th section using multivariate statistical image analysis, applied to GLI-images of coronal histological sections of 1 micron resolution. Resulting mappings are available on every section. Maps were transformed to the 3D reconstructed BigBrain space. Individual sections were used to assemble a 3D volume of the area, low quality results were replaced by interpolations between nearest neighboring sections. The volume was then smoothed using an 11\u00b3 median filter and largest connected components were identified to remove false positive results. The dataset consists of a HDF5 file containing the volume in RAS dimension ordering (20 micron isotropic resolution, dataset \u201cvolume\u201d) and an affine transformation matrix (dataset \u201caffine\u201d). An additional dataset \u201cinterpolation_info\u201d contains an integer vector for each section which indicates if a section was interpolated due to low quality results (value 2) or not (value 1). ",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hOc3v (LingG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/0d6392fd-b905-4bc3-bac9-fc44d8990a30"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Ultrahigh resolution 3D cytoarchitectonic map of Area hOc3v (LingG) created by a Deep-Learning assisted workflow",
	"files": [
		{
			"byteSize": 11986682.0,
			"hashcode": "3112b03bce5ed55997c8756e8bdfe6a9",
			"name": "2020_09_29_hOc3v_masks.zip",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d002272_BigBrainCytoMapping-hoc3v_pub/Deep-Learning/2020_09_29_hOc3v_masks.zip",
			"contentType": "application/zip",
			"hash": "d27ab819408ea68943d710622994dffb"
		},
		{
			"byteSize": 158440.0,
			"hashcode": "41c7bbafb311b310e05fb5c49a38a86f",
			"name": "EBRAINS-DataDescriptor_hOc3v_DeepLearning.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d002272_BigBrainCytoMapping-hoc3v_pub/Deep-Learning/EBRAINS-DataDescriptor_hOc3v_DeepLearning.pdf",
			"contentType": "application/pdf",
			"hash": "39df1bd37ece1a41432620f8013723a1"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "8cdfc1ca217660831dfa3165c3fa0703",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d002272_BigBrainCytoMapping-hoc3v_pub/Deep-Learning/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 84896297.0,
			"hashcode": "6d27bdf5761acb24065e66c46137dc1e",
			"name": "2020_09_29_hOc3v.hdf5",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d002272_BigBrainCytoMapping-hoc3v_pub/Deep-Learning/2020_09_29_hOc3v.hdf5",
			"contentType": "application/octet-stream",
			"hash": "87bd191adfa3159dd6ba3d10a2c89d6e"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/f746514d-b79a-48e2-9c07-39f7c62459cf",
	"contributors": [
		{
			"schema.org/shortName": "Dickscheid, T.",
			"identifier": "6815ac666477e233eb244713ed5cf8dd",
			"name": "Dickscheid, Timo",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ac2d5fde-4202-47da-97c5-87f7a35a8efd",
			"shortName": "Dickscheid, T."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c4a9ca26-8b84-4fac-b40b-158cf891be3b",
			"name": "Kiwitz, Kai",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c4a9ca26-8b84-4fac-b40b-158cf891be3b",
			"shortName": "Kiwitz, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "d5f892af-a003-4e54-9c88-0c10ec3da455",
			"name": "Schiffer, Christian ",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d5f892af-a003-4e54-9c88-0c10ec3da455",
			"shortName": "Schiffer, C."
		}
	],
	"id": "f746514d-b79a-48e2-9c07-39f7c62459cf",
	"kgReference": [
		"10.25493/PH0C-EKX"
	],
	"publications": [
		{
			"name": "Convolutional Neural Networks for cytoarchitectonic brain mapping at large scale",
			"cite": "Schiffer, C., Spitzer, H., Kiwitz, K., Unger, N., Wagstyl, K., Evans, A. C., Harmeling, S., Amunts, K., Dickscheid, T (2021). Convolutional Neural Networks for cytoarchitectonic brain mapping at large scale. NeuroImage 118327",
			"doi": "10.1016/j.neuroimage.2021.118327"
		},
		{
			"name": "The verticality index: A quantitative approach to the analysis of the columnar arrangement of neurons in the primate neocortex ",
			"cite": "Schleicher, A. , Zilles, K. (2005). The verticality index: A quantitative approach to the analysis of the columnar arrangement of neurons in the primate neocortex. Neocortical modularity and the cell minicolumn 181\u2013185",
			"doi": ""
		},
		{
			"name": "Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics",
			"cite": "Schleicher, A., Amunts, K., Geyer, S., Morosan, P., & Zilles, K. (1999). Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics. NeuroImage, 9(1), 165\u2013177. ",
			"doi": "10.1006/nimg.1998.0385"
		},
		{
			"name": "Improving Cytoarchitectonic Segmentation of Human Brain Areas with Self-supervised Siamese Networks",
			"cite": "Spitzer, H., Kiwitz, K., Amunts, K., Harmeling, S., Dickscheid, T. (2018). Improving Cytoarchitectonic Segmentation of Human Brain Areas with Self-supervised Siamese Networks. In: Frangi A., Schnabel J., Davatzikos C., Alberola-L\u00f3pez C., Fichtinger G. (eds) Medical Image Computing and Computer Assisted Intervention \u2013 MICCAI 2018. MICCAI 2018. Lecture Notes in Computer Science, vol 11072. Springer, Cham.",
			"doi": "10.1007/978-3-030-00931-1_76"
		},
		{
			"name": "Parcellation of visual cortex on high-resolution histological brain sections using convolutional neural networks",
			"cite": "Spitzer, H., Amunts, K., Harmeling, S., and Dickscheid, T. (2017). Parcellation of visual cortex on high-resolution histological brain sections using convolutional neural networks, in 2017 IEEE 14th International Symposium on Biomedical Imaging (ISBI 2017), pp. 920\u2013923.",
			"doi": "10.1109/ISBI.2017.7950666"
		},
		{
			"name": "BigBrain: An Ultrahigh-Resolution 3D Human Brain Model",
			"cite": "Amunts, K., Lepage, C., Borgeat, L., Mohlberg, H., Dickscheid, T., Rousseau, M. E., Bludau, S., Bazin, P. L., Lewis, L. B., Oros-Peusquens, A. M., Shah, N. J., Lippert, T., Zilles, K., Evans, A. C. (2013).  BigBrain: An Ultrahigh-Resolution 3D Human Brain Model. Science, 340(6139),1472-5.",
			"doi": "10.1126/science.1235381"
		}
	]
}
