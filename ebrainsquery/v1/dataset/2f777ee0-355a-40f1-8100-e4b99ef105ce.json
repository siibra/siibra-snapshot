{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Richter, M., Amunts, K., Mohlberg, H., Bludau, S., Eickhoff, S. B., Zilles, K., Caspers, S. (2018). Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions. Cerebral Cortex, 29(3), 1305-1327",
			"doi": "10.1093/cercor/bhy245"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area hOc6 (POS) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area hOc6 (POS). The probability map of Area hOc6 (POS) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area hOc6 (POS):\nRichter et al. (2019) [Data set, v7.0] [DOI: 10.25493/ZKR8-P13](https://doi.org/10.25493%2FZKR8-P13)\nRichter et al. (2019) [Data set, v7.1] [DOI: 10.25493/4101-1ZG](https://doi.org/10.25493%2F4101-1ZG)\n\nThe most probable delineation of Area hOc6 (POS) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nNexus ID: minds/core/dataset/v1.0.0/e13a1f52-453c-4a50-a7ea-3c0cd0475e70",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hOc6 (POS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/d72e0210-a910-4b15-bcaf-80c3433cd3e0"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area hOc6 (POS) (v7.3)",
	"files": [
		{
			"byteSize": 53370.0,
			"hashcode": "b13bba68e54b6b9a39d09260f37e20f0",
			"name": "Area-hOc6_l_N10_nlin2ICBM152asym2009c_7.3_publicP_65bc1b5335bf87cd405346b6eb08f170.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc6_pub/7.3/Area-hOc6_l_N10_nlin2ICBM152asym2009c_7.3_publicP_65bc1b5335bf87cd405346b6eb08f170.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "649acf658481cfc39162791b85f07613"
		},
		{
			"byteSize": 23.0,
			"hashcode": "a381cb1853eea4edf0f511f3fb3f1347",
			"name": "subjects_Area-hOc6.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc6_pub/7.3/subjects_Area-hOc6.csv",
			"contentType": "text/csv",
			"hash": "662f05cadecb8a40e61bebf252676853"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "0c696d5ac2b1b024d3eabbb53145a810",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc6_pub/7.3/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 80498.0,
			"hashcode": "9d64e16ee8e371746070bba2b81c1822",
			"name": "Area-hOc6_r_N10_nlin2Stdcolin27_7.3_publicP_4ca179bec0ec0a19785557d27ae969c6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc6_pub/7.3/Area-hOc6_r_N10_nlin2Stdcolin27_7.3_publicP_4ca179bec0ec0a19785557d27ae969c6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4ca179bec0ec0a19785557d27ae969c6"
		},
		{
			"byteSize": 80212.0,
			"hashcode": "ec5586d435f6408e92900c7dfe19b1f4",
			"name": "Area-hOc6_l_N10_nlin2Stdcolin27_7.3_publicP_65bc1b5335bf87cd405346b6eb08f170.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc6_pub/7.3/Area-hOc6_l_N10_nlin2Stdcolin27_7.3_publicP_65bc1b5335bf87cd405346b6eb08f170.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "65bc1b5335bf87cd405346b6eb08f170"
		},
		{
			"byteSize": 55616.0,
			"hashcode": "94a57a46d577ec41741c3a65f3aa8f28",
			"name": "Area-hOc6_r_N10_nlin2ICBM152asym2009c_7.3_publicP_4ca179bec0ec0a19785557d27ae969c6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc6_pub/7.3/Area-hOc6_r_N10_nlin2ICBM152asym2009c_7.3_publicP_4ca179bec0ec0a19785557d27ae969c6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "c5c299fd082cfef53fecc498d721c71a"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/2f777ee0-355a-40f1-8100-e4b99ef105ce",
	"contributors": [
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"name": "Richter, Monika",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"shortName": "Richter, M."
		}
	],
	"id": "2f777ee0-355a-40f1-8100-e4b99ef105ce",
	"kgReference": [
		"10.25493/VAYZ-07B"
	],
	"publications": [
		{
			"name": " Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions",
			"cite": "Richter, M., Amunts, K., Mohlberg, H., Bludau, S., Eickhoff, S. B., Zilles, K., Caspers, S. (2018). Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions. Cerebral Cortex, 29(3), 1305-1327",
			"doi": "10.1093/cercor/bhy245"
		}
	]
}
