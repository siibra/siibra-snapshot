{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Tellmann, S., Bludau, S., Eickhoff, S., Mohlberg, H., Minnerop, M., & Amunts, K. (2015). Cytoarchitectonic mapping of the human brain cerebellar nuclei in stereotaxic space and delineation of their co-activation patterns. Frontiers in Neuroanatomy, 09. ",
			"doi": "10.3389/fnana.2015.00054"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Fastigial Nucleus (Cerebellum) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Fastigial Nucleus (Cerebellum). The probability map of Fastigial Nucleus (Cerebellum) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Fastigial Nucleus (Cerebellum):\nTellmann et al. (2018) [Data set, v6.0] [DOI: 10.25493/JNN1-V3Q](https://doi.org/10.25493%2FJNN1-V3Q)\nTellmann et al. (2019) [Data set, v2.3] [DOI: 10.25493/3YJ9-S6G](https://doi.org/10.25493%2F3YJ9-S6G)\n\nThe most probable delineation of Fastigial Nucleus (Cerebellum) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Fastigial Nucleus (Cerebellum)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/e8abfe3d-8b64-45c2-8853-314d82873273"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Fastigial Nucleus (Cerebellum) (v6.3)",
	"files": [
		{
			"byteSize": 69688.0,
			"hashcode": "3340862e7915aba10ad8a43b0a331c3b",
			"name": "Fastigial-Nucleus_l_N10_nlin2Stdcolin27_6.3_publicP_1f61b4283efff49d2b55740bcf59e2c6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Fastigial-Nucleus_pub/6.3/Fastigial-Nucleus_l_N10_nlin2Stdcolin27_6.3_publicP_1f61b4283efff49d2b55740bcf59e2c6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "1f61b4283efff49d2b55740bcf59e2c6"
		},
		{
			"byteSize": 24.0,
			"hashcode": "748e4a3bf1ed0cd52cdefe4a87dd41b7",
			"name": "subjects_Fastigial-Nucleus.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Fastigial-Nucleus_pub/6.3/subjects_Fastigial-Nucleus.csv",
			"contentType": "text/csv",
			"hash": "f785b921f08fc713afab0994854ee065"
		},
		{
			"byteSize": 39744.0,
			"hashcode": "f3f8f9bf676d883f8adaf0136c51b204",
			"name": "Fastigial-Nucleus_l_N10_nlin2ICBM152asym2009c_6.3_publicP_1f61b4283efff49d2b55740bcf59e2c6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Fastigial-Nucleus_pub/6.3/Fastigial-Nucleus_l_N10_nlin2ICBM152asym2009c_6.3_publicP_1f61b4283efff49d2b55740bcf59e2c6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "3e424c7735ae4932d6018279fcfe10a6"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "509efcd65ac1fbe6f5240c5802fbf0f0",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Fastigial-Nucleus_pub/6.3/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 39338.0,
			"hashcode": "8e6bbee89a26f722c2303f1380efd8a0",
			"name": "Fastigial-Nucleus_r_N10_nlin2ICBM152asym2009c_6.3_publicP_c401617d5e8c782d6c6410e790c98981.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Fastigial-Nucleus_pub/6.3/Fastigial-Nucleus_r_N10_nlin2ICBM152asym2009c_6.3_publicP_c401617d5e8c782d6c6410e790c98981.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "af567de02d03419465ccff86ecc2faea"
		},
		{
			"byteSize": 69517.0,
			"hashcode": "bd6b8ed9b1507cca96872a9364a114a0",
			"name": "Fastigial-Nucleus_r_N10_nlin2Stdcolin27_6.3_publicP_c401617d5e8c782d6c6410e790c98981.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Fastigial-Nucleus_pub/6.3/Fastigial-Nucleus_r_N10_nlin2Stdcolin27_6.3_publicP_c401617d5e8c782d6c6410e790c98981.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "c401617d5e8c782d6c6410e790c98981"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/93910df0-8f9b-432d-8b3d-7320d51be49c",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c3be94cf9c3b2b2ec7c9f730f9fbc886",
			"name": "Minnerop, Martina",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/e4314d3a-cf47-4f5e-99d2-1f2c3f9d804b",
			"shortName": "Minnerop, M."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c745b264c8bfe3b8b55ac510bd065381",
			"name": "Tellmann, Stefanie",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f5087593-7dc5-4f5a-963e-ed8bbe7528d5",
			"shortName": "Tellmann, S."
		}
	],
	"id": "93910df0-8f9b-432d-8b3d-7320d51be49c",
	"kgReference": [
		"10.25493/QWQ3-PD4"
	],
	"publications": [
		{
			"name": "Cytoarchitectonic mapping of the human brain cerebellar nuclei in stereotaxic space and delineation of their co-activation patterns",
			"cite": "Tellmann, S., Bludau, S., Eickhoff, S., Mohlberg, H., Minnerop, M., & Amunts, K. (2015). Cytoarchitectonic mapping of the human brain cerebellar nuclei in stereotaxic space and delineation of their co-activation patterns. Frontiers in Neuroanatomy, 09. ",
			"doi": "10.3389/fnana.2015.00054"
		}
	]
}
