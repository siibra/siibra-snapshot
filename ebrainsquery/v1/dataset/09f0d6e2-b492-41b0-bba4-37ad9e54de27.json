{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Winterer, G., Androsova, G., Bender, O., Boraschi, D., Borchers, F., Dschietzig, T. B., \u2026 Zacharias, N. (2018). Personalized risk prediction of postoperative cognitive impairment \u2013 rationale for the EU-funded BioCog project. European Psychiatry. ",
			"doi": "10.1016/j.eurpsy.2017.10.004"
		},
		{
			"cite": "Androsova, G., Krause, R., Winterer, G., & Schneider, R. (2015). Biomarkers of postoperative delirium and cognitive dysfunction. Frontiers in Aging Neuroscience. ",
			"doi": "10.3389/fnagi.2015.00112"
		},
		{
			"cite": "Feinkohl, Insa, Georg Winterer, Claudia D. Spies, and Tobias Pischon. (2017). Cognitive Reserve and the Risk of Postoperative Cognitive Dysfunction. Deutsches Arzteblatt International.",
			"doi": "10.3238/arztebl.2017.0110"
		},
		{
			"cite": "Feinkohl, Insa, Georg Winterer, and Tobias Pischon. (2018). Associations of Dyslipidaemia and Lipid-Lowering Treatment with Risk of Postoperative Cognitive Dysfunction: A Systematic Review and Meta-Analysis. Journal of Epidemiology and Community Health.",
			"doi": "10.1136/jech-2017-210338"
		},
		{
			"cite": "Feinkohl, I., G. Winterer, and T. Pischon. (2017). Hypertension and Risk of Post-Operative Cognitive Dysfunction (POCD): A Systematic Review and Meta-Analysis. Clinical Practice & Epidemiology in Mental Health.",
			"doi": "10.2174/1745017901713010027"
		},
		{
			"cite": "Feinkohl, I., G. Winterer, and T. Pischon. (2017). Diabetes Is Associated with Risk of Postoperative Cognitive Dysfunction: A Meta-Analysis. Diabetes/Metabolism Research and Reviews.",
			"doi": "10.1002/dmrr.2884"
		},
		{
			"cite": "Feinkohl, Insa, Georg Winterer, and Tobias Pischon. (2016). Obesity and Post-Operative Cognitive Dysfunction: A Systematic Review and Meta-Analysis. Diabetes/Metabolism Research and Reviews.",
			"doi": "10.1002/dmrr.2786"
		},
		{
			"cite": "Gunnar Lachmann, Claudia Spies, Victoria Windmann, Tobias Wollersheim, Lilian Jo Engelhardt, Georg Winterer, and Simone K\u00fchn. (2019). Impact of Intraoperative Hyperglycemia on Brain Structures and Volumes. Journal of Neuroimaging.",
			"doi": "10.1111/jon.12583"
		},
		{
			"cite": "Kant, Ilse M. J., Jeroen de Bresser, Simone J. T. van Montfort, Ellen Aarts, Jorrit Jan Verlaan, Norman Zacharias, Georg Winterer, Claudia Spies, Arjen J. C. Slooter, and Jeroen Hendrikse. (2018). The Association between Brain Volume, Cortical Brain Infarcts, and Physical Frailty. Neurobiology of Aging.",
			"doi": "10.1016/j.neurobiolaging.2018.06.032"
		},
		{
			"cite": "Lammers, Florian, Friedrich Borchers, Insa Feinkohl, Jeroen Hendrikse, Ilse M. J. Kant, Petra Kozma, Tobias Pischon, Arjen J. C. Slooter, Claudia Spies, Simone J. T. van Montfort, Norman Zacharias, Laszlo Zaborszky, and Georg Winterer. (2018). Basal Forebrain Cholinergic System Volume Is Associated with General Cognitive Ability in the Elderly. Neuropsychologia.",
			"doi": "10.1016/j.neuropsychologia.2018.08.005"
		},
		{
			"cite": "Laubach, M., F. Lammers, N. Zacharias, I. Feinkohl, T. Pischon, F. Borchers, A. J. C. Slooter, S. K\u00fchn, C. Spies, and G. Winterer. (2018). Size Matters: Grey Matter Brain Reserve Predicts Executive Functioning in the Elderly. Neuropsychologia.",
			"doi": "10.1016/j.neuropsychologia.2018.08.008"
		}
	],
	"activity": [
		{
			"protocols": [
				"cognitive test battery",
				"cognitive decline in the elderly",
				"biomarkers",
				"simultaneous fMRI/EEG",
				"MPRAGE",
				"T2 SPACE/FLAIR",
				"T2 MRI"
			],
			"preparation": [
				"In vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"magnetic resonance imaging (MRI)",
		"Diffusion Tensor Imaging (DTI)",
		"7-Tesla spectroscopy",
		"pcASL Arterial Spin labeling",
		"functional magnetic resonance imaging (fMRI)",
		"electroencephalography (EEG)",
		"Gene expression measurement/visualization"
	],
	"custodians": [],
	"project": [
		"Neuroimaging, clinical and molecular biomarker data from large human cohort assessed on cognitive impairment (elderly patients)."
	],
	"description": "Rich multicentric dataset of human neuroimaging, molecular biomarker and clinical/ neuropsychological data obtained prior to-, and following scheduled surgical intervention, performed to identify predictors and biomarkers of Postoperative Cognitive Impairments (n=1130 patients enrolled in the study with follow-up data up to one year). \nImaging dataset contains MRI (MPRAGE, T2 SPACE/FLAIR, pcASL Arterial Spin labeling, Diffusion Tensor Imaging, 7-Tesla spectroscopy, T2- weighted high-resolution Hippocampus, SWI susceptibility weighted imaging) and Simultaneous fMRI/EEG resting state recordings. Molecular biomarkers (blood and CSF) and genome-wide array data (DNA, mRNA, miRNA) available. \n\n\n**The dataset is hosted by the data provider. For information concerning institutional agreements and process required to access this dataset, contact:\n[curation-support@ebrains.eu](curation-support@ebrains.eu)**\n\n**Project description homepage: [BioCog] (http://www.biocog.eu/index_en) \u200b**",
	"parcellationAtlas": [],
	"licenseInfo": [],
	"embargoStatus": [
		{
			"identifier": "203e75c5-7a67-48d8-9264-6588e059d292",
			"name": "Externally hosted",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/203e75c5-7a67-48d8-9264-6588e059d292"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [
		"Homo sapiens"
	],
	"name": "Neuroimaging, clinical and molecular biomarker data from large human cohort assessed on cognitive impairments (elderly patients).",
	"files": [],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/09f0d6e2-b492-41b0-bba4-37ad9e54de27",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "08b52a4a-5ca3-4508-90c3-1f911fa19ebd",
			"name": "Winterer, Georg",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/08b52a4a-5ca3-4508-90c3-1f911fa19ebd",
			"shortName": "Winterer, G."
		}
	],
	"id": "09f0d6e2-b492-41b0-bba4-37ad9e54de27",
	"kgReference": [],
	"publications": [
		{
			"name": "Personalized risk prediction of postoperative cognitive impairment \u2013 rationale for the EU-funded BioCog project",
			"cite": "Winterer, G., Androsova, G., Bender, O., Boraschi, D., Borchers, F., Dschietzig, T. B., \u2026 Zacharias, N. (2018). Personalized risk prediction of postoperative cognitive impairment \u2013 rationale for the EU-funded BioCog project. European Psychiatry. ",
			"doi": "10.1016/j.eurpsy.2017.10.004"
		},
		{
			"name": "Biomarkers of postoperative delirium and cognitive dysfunction",
			"cite": "Androsova, G., Krause, R., Winterer, G., & Schneider, R. (2015). Biomarkers of postoperative delirium and cognitive dysfunction. Frontiers in Aging Neuroscience. ",
			"doi": "10.3389/fnagi.2015.00112"
		},
		{
			"name": "Cognitive Reserve and the Risk of Postoperative Cognitive Dysfunction",
			"cite": "Feinkohl, Insa, Georg Winterer, Claudia D. Spies, and Tobias Pischon. (2017). Cognitive Reserve and the Risk of Postoperative Cognitive Dysfunction. Deutsches Arzteblatt International.",
			"doi": "10.3238/arztebl.2017.0110"
		},
		{
			"name": "Associations of dyslipidaemia and lipid-lowering treatment with risk of postoperative cognitive dysfunction: a systematic review and meta-analysis",
			"cite": "Feinkohl, Insa, Georg Winterer, and Tobias Pischon. (2018). Associations of Dyslipidaemia and Lipid-Lowering Treatment with Risk of Postoperative Cognitive Dysfunction: A Systematic Review and Meta-Analysis. Journal of Epidemiology and Community Health.",
			"doi": "10.1136/jech-2017-210338"
		},
		{
			"name": "Hypertension and Risk of Post-Operative Cognitive Dysfunction (POCD): A Systematic Review and Meta-Analysis",
			"cite": "Feinkohl, I., G. Winterer, and T. Pischon. (2017). Hypertension and Risk of Post-Operative Cognitive Dysfunction (POCD): A Systematic Review and Meta-Analysis. Clinical Practice & Epidemiology in Mental Health.",
			"doi": "10.2174/1745017901713010027"
		},
		{
			"name": "Diabetes is associated with risk of postoperative cognitive dysfunction: A meta-analysis",
			"cite": "Feinkohl, I., G. Winterer, and T. Pischon. (2017). Diabetes Is Associated with Risk of Postoperative Cognitive Dysfunction: A Meta-Analysis. Diabetes/Metabolism Research and Reviews.",
			"doi": "10.1002/dmrr.2884"
		},
		{
			"name": "Obesity and post-operative cognitive dysfunction: a systematic review and meta-analysis",
			"cite": "Feinkohl, Insa, Georg Winterer, and Tobias Pischon. (2016). Obesity and Post-Operative Cognitive Dysfunction: A Systematic Review and Meta-Analysis. Diabetes/Metabolism Research and Reviews.",
			"doi": "10.1002/dmrr.2786"
		},
		{
			"name": "Impact of Intraoperative Hyperglycemia on Brain Structures and Volumes",
			"cite": "Gunnar Lachmann, Claudia Spies, Victoria Windmann, Tobias Wollersheim, Lilian Jo Engelhardt, Georg Winterer, and Simone K\u00fchn. (2019). Impact of Intraoperative Hyperglycemia on Brain Structures and Volumes. Journal of Neuroimaging.",
			"doi": "10.1111/jon.12583"
		},
		{
			"name": "The association between brain volume, cortical brain infarcts, and physical frailty",
			"cite": "Kant, Ilse M. J., Jeroen de Bresser, Simone J. T. van Montfort, Ellen Aarts, Jorrit Jan Verlaan, Norman Zacharias, Georg Winterer, Claudia Spies, Arjen J. C. Slooter, and Jeroen Hendrikse. (2018). The Association between Brain Volume, Cortical Brain Infarcts, and Physical Frailty. Neurobiology of Aging.",
			"doi": "10.1016/j.neurobiolaging.2018.06.032"
		},
		{
			"name": "Basal forebrain cholinergic system volume is associated with general cognitive ability in the elderly",
			"cite": "Lammers, Florian, Friedrich Borchers, Insa Feinkohl, Jeroen Hendrikse, Ilse M. J. Kant, Petra Kozma, Tobias Pischon, Arjen J. C. Slooter, Claudia Spies, Simone J. T. van Montfort, Norman Zacharias, Laszlo Zaborszky, and Georg Winterer. (2018). Basal Forebrain Cholinergic System Volume Is Associated with General Cognitive Ability in the Elderly. Neuropsychologia.",
			"doi": "10.1016/j.neuropsychologia.2018.08.005"
		},
		{
			"name": "Size matters: Grey matter brain reserve predicts executive functioning in the elderly",
			"cite": "Laubach, M., F. Lammers, N. Zacharias, I. Feinkohl, T. Pischon, F. Borchers, A. J. C. Slooter, S. K\u00fchn, C. Spies, and G. Winterer. (2018). Size Matters: Grey Matter Brain Reserve Predicts Executive Functioning in the Elderly. Neuropsychologia.",
			"doi": "10.1016/j.neuropsychologia.2018.08.008"
		}
	]
}
