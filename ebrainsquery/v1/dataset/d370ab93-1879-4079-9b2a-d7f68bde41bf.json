{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Kujovic, M., Zilles, K., Malikovic, A., Schleicher, A., Mohlberg, H., Rottschy, C., \u2026 Amunts, K. (2012). Cytoarchitectonic mapping of the human dorsal extrastriate cortex. Brain Structure and Function, 218(1), 157\u2013172. ",
			"doi": "10.1007/s00429-012-0390-9"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area hOc4d (Cuneus) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area hOc4d (Cuneus). The probability map of Area hOc4d (Cuneus) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area hOc4d (Cuneus):\nKujovic et al. (2018) [Data set, v2.2] [DOI: 10.25493/KQ4Y-Q4M](https://doi.org/10.25493%2FKQ4Y-Q4M)\nKujovic et al. (2019) [Data set, v2.4] [DOI: 10.25493/VSK5-DET](https://doi.org/10.25493%2FVSK5-DET)\nKujovic et al. (2020) [Data set, v4.0] [DOI: 10.25493/EJH3-XCQ](https://doi.org/10.25493%2FEJH3-XCQ)\n\nThe most probable delineation of Area hOc4d (Cuneus) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hOc4d (Cuneus)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/8120426c-f65b-4426-8a58-3060e2334921"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area hOc4d (Cuneus) (v4.2)",
	"files": [
		{
			"byteSize": 117219.0,
			"hashcode": "15edd2b1fad84f00cbd015e1c8cbc984",
			"name": "Area-hOc4d_r_N10_nlin2Stdcolin27_4.2_publicP_bc26f3680dfcf640d98c5f0648b4c470.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4d_pub/4.2/Area-hOc4d_r_N10_nlin2Stdcolin27_4.2_publicP_bc26f3680dfcf640d98c5f0648b4c470.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "bc26f3680dfcf640d98c5f0648b4c470"
		},
		{
			"byteSize": 113026.0,
			"hashcode": "3a9fae71e123d929a74466021939d698",
			"name": "Area-hOc4d_l_N10_nlin2ICBM152asym2009c_4.2_publicP_00fcdeb0dc89a7b86774d6ae244b170f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4d_pub/4.2/Area-hOc4d_l_N10_nlin2ICBM152asym2009c_4.2_publicP_00fcdeb0dc89a7b86774d6ae244b170f.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "d79339f6dd6735da759249e3bffb2f02"
		},
		{
			"byteSize": 104618.0,
			"hashcode": "9046a88020417df2605aea9c5fd781f7",
			"name": "Area-hOc4d_r_N10_nlin2ICBM152asym2009c_4.2_publicP_bc26f3680dfcf640d98c5f0648b4c470.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4d_pub/4.2/Area-hOc4d_r_N10_nlin2ICBM152asym2009c_4.2_publicP_bc26f3680dfcf640d98c5f0648b4c470.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "cfa5cc6c5a872ab008476c61425c88d0"
		},
		{
			"byteSize": 131604.0,
			"hashcode": "953dcff2d8fbde5ed94ea0363ecde205",
			"name": "Area-hOc4d_l_N10_nlin2Stdcolin27_4.2_publicP_00fcdeb0dc89a7b86774d6ae244b170f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4d_pub/4.2/Area-hOc4d_l_N10_nlin2Stdcolin27_4.2_publicP_00fcdeb0dc89a7b86774d6ae244b170f.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "00fcdeb0dc89a7b86774d6ae244b170f"
		},
		{
			"byteSize": 20.0,
			"hashcode": "b0acba2d3c53f8d5cf0bed85eaa02a5b",
			"name": "subjects_Area-hOc4d.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4d_pub/4.2/subjects_Area-hOc4d.csv",
			"contentType": "text/csv",
			"hash": "d4fd357f7807fd448ba3bc2240f5df20"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "cf84860810f39cf3a26d71ba5eb9c6c9",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4d_pub/4.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/d370ab93-1879-4079-9b2a-d7f68bde41bf",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "465d8385bb1da6c9b4fde7c570e14cde",
			"name": "Rottschy, Claudia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/960ac8ca-a15c-4362-a565-c04d047bc52d",
			"shortName": "Rottschy, C."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "0b6888c55d248a941a022961b3de4374",
			"name": "Malikovic, Aleksandar",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8267c403-6f87-4c77-b113-3bd50d848e31",
			"shortName": "Malikovic, A."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "0621a40fd626426cdf04ffaf13c63b3a",
			"name": "Kujovic, Milenko",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/cfa0e3b6-c3d8-4c82-a46b-82d24c7fb750",
			"shortName": "Kujovic, M."
		}
	],
	"id": "d370ab93-1879-4079-9b2a-d7f68bde41bf",
	"kgReference": [
		"10.25493/9GRV-14A"
	],
	"publications": [
		{
			"name": "Cytoarchitectonic mapping of the human dorsal extrastriate cortex",
			"cite": "Kujovic, M., Zilles, K., Malikovic, A., Schleicher, A., Mohlberg, H., Rottschy, C., \u2026 Amunts, K. (2012). Cytoarchitectonic mapping of the human dorsal extrastriate cortex. Brain Structure and Function, 218(1), 157\u2013172. ",
			"doi": "10.1007/s00429-012-0390-9"
		}
	]
}
