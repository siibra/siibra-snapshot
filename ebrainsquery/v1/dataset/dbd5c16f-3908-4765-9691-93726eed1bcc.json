{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Rademacher, J., Morosan, P., Schormann, T., Schleicher, A., Werner, C., Freund, H.-J., & Zilles, K. (2001). Probabilistic Mapping and Volume Measurement of Human Primary Auditory Cortex. NeuroImage, 13(4), 669\u2013683. ",
			"doi": "10.1006/nimg.2000.0714"
		},
		{
			"cite": "Morosan, P., Rademacher, J., Schleicher, A., Amunts, K., Schormann, T., & Zilles, K. (2001). Human Primary Auditory Cortex: Cytoarchitectonic Subdivisions and Mapping into a Spatial Reference System. NeuroImage, 13(4), 684\u2013701. ",
			"doi": "10.1006/nimg.2000.0715"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area TE 1.0 (HESCHL) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area TE 1.0 (HESCHL). The probability map of Area TE 1.0 (HESCHL) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area TE 1.0 (HESCHL):\nMorosan et al. (2018) [Data set, v5.0] [DOI: 10.25493/CP2T-FYT](https://doi.org/10.25493%2FCP2T-FYT)\nMorosan et al. (2019) [Data set, v5.1] [DOI: 10.25493/MV3G-RET](https://doi.org/10.25493%2FMV3G-RET) \n\nThe most probable delineation of Area TE 1.0 (HESCHL) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 1.0 (HESCHL)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/13e21153-2ba8-4212-b172-8894f1012225"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area TE 1.0 (HESCHL) (v6.0)",
	"files": [
		{
			"byteSize": 21.0,
			"hashcode": "ce2b47a980a5928526030e3af78f5b57",
			"name": "subjects_Area-TE-1.0.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.0/subjects_Area-TE-1.0.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "b97e0b3e0943e120a2688bf135b6628b",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 87872.0,
			"hashcode": "2b55477391863128e8b5d50896b2b9d5",
			"name": "Area-TE-1.0_r_N10_nlin2Stdcolin27_6.0_publicP_b7c42b54ab253b5b495736ce8f9f19b0.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.0/Area-TE-1.0_r_N10_nlin2Stdcolin27_6.0_publicP_b7c42b54ab253b5b495736ce8f9f19b0.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "b7c42b54ab253b5b495736ce8f9f19b0"
		},
		{
			"byteSize": 68596.0,
			"hashcode": "2b9c53e534799b60461c71988d8f0c35",
			"name": "Area-TE-1.0_l_N10_nlin2ICBM152asym2009c_6.0_publicP_478227a1061b11853478619ea75f32b9.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.0/Area-TE-1.0_l_N10_nlin2ICBM152asym2009c_6.0_publicP_478227a1061b11853478619ea75f32b9.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "478227a1061b11853478619ea75f32b9"
		},
		{
			"byteSize": 62845.0,
			"hashcode": "8d79952cddb91b88aa8f4d168166ba9e",
			"name": "Area-TE-1.0_r_N10_nlin2ICBM152asym2009c_6.0_publicP_ec83a3e80ddbfc3316373e76b4d06f64.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.0/Area-TE-1.0_r_N10_nlin2ICBM152asym2009c_6.0_publicP_ec83a3e80ddbfc3316373e76b4d06f64.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "ec83a3e80ddbfc3316373e76b4d06f64"
		},
		{
			"byteSize": 89081.0,
			"hashcode": "e61ff21895dac9c7af25e0336b73249d",
			"name": "Area-TE-1.0_l_N10_nlin2Stdcolin27_6.0_publicP_0276eecd6cc4266783be664fcb9e5586.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.0/Area-TE-1.0_l_N10_nlin2Stdcolin27_6.0_publicP_0276eecd6cc4266783be664fcb9e5586.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "0276eecd6cc4266783be664fcb9e5586"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/dbd5c16f-3908-4765-9691-93726eed1bcc",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "49e0e76dfff178efd76fd0325be98cd9",
			"name": "Freund, H.-J.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f1fc023f-5f78-4ce3-89a1-04de6b767a2e",
			"shortName": "Freund, H."
		},
		{
			"schema.org/shortName": "Werner, C.",
			"identifier": "c1ffe49cbf938cecbeee703a6602cb2c",
			"name": "Werner, C.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/687b9def-7ed0-48fe-bb24-f779bc9a94bb",
			"shortName": "Werner, C."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3a2dcba69606495850f0112bf23c4b03",
			"name": "Rademacher, J\u00f6rg",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/aa01de8e-a6de-4c9c-af62-8746a3e29ee1",
			"shortName": "Rademacher, J."
		},
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		}
	],
	"id": "dbd5c16f-3908-4765-9691-93726eed1bcc",
	"kgReference": [
		"10.25493/AKAE-ZWP"
	],
	"publications": [
		{
			"name": "Probabilistic Mapping and Volume Measurement of Human Primary Auditory Cortex",
			"cite": "Rademacher, J., Morosan, P., Schormann, T., Schleicher, A., Werner, C., Freund, H.-J., & Zilles, K. (2001). Probabilistic Mapping and Volume Measurement of Human Primary Auditory Cortex. NeuroImage, 13(4), 669\u2013683. ",
			"doi": "10.1006/nimg.2000.0714"
		},
		{
			"name": "Human Primary Auditory Cortex: Cytoarchitectonic Subdivisions and Mapping into a Spatial Reference System",
			"cite": "Morosan, P., Rademacher, J., Schleicher, A., Amunts, K., Schormann, T., & Zilles, K. (2001). Human Primary Auditory Cortex: Cytoarchitectonic Subdivisions and Mapping into a Spatial Reference System. NeuroImage, 13(4), 684\u2013701. ",
			"doi": "10.1006/nimg.2000.0715"
		}
	]
}
