{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Demiraj A, Karozos K, Spartalis I, and Vassalos V. 2019. Meta-data management and quality control for the medical informatics platform. In Proceedings of the 23rd International Database Applications & Engineering Symposium ( IDEAS \u201919 ). Association for Computing Machinery, New York, NY, USA, Article 10, 1\u20139.",
			"doi": "https://doi.org/10.1145/3331076.3331088"
		}
	],
	"activity": [
		{
			"protocols": [
				"Reconstruction"
			],
			"preparation": []
		}
	],
	"referenceSpaces": [],
	"methods": [
		"metadata parsing",
		"analysis method"
	],
	"custodians": [
		{
			"schema.org/shortName": null,
			"identifier": "8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"name": "Vassalos, Vasilis",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"shortName": "Vassalos, V."
		}
	],
	"project": [
		"The Medical Informatics Platform (MIP)"
	],
	"description": "Data in the Medical Informatics Platform (MIP) resides in hospital servers and never leaves the hospital. Analyses and experiments are executed with respect to that principle while preserving patients\u2019 anonymity making it infeasible for their identity to be inferred. \nHospitals importing their data to the Medical Informatics Platform join a federation so as to run analyses on data from other hospital nodes as well. Each federation in the MIP refers to a specific Medical Condition. \nHere the metadata for the federation of hospitals that studies dementia is presented.\n\nThis metadata schema, consisting of a total of 180 variables, has the following main variable categories:\n**1. PET** - 3 variables (AV45, FDG-PET, PIB SUVR) which are average scores of measurements collected by a Positron Emission Tomography\n**2. Brain Anatomy** - 135 volumetric variables based on a brain atlas. Values have been generated from the brain feature extraction pipeline which uses SPM12\n**3. Diagnosis** - 12 polynomial variables related to medical conditions like Alzheimer and Parkinson\n**4. Neuropsychology** - 4 variables for scores in Mini Mental State Examination , 1 Montreal Cognitive Assessment, Hoehn and Yahr scale, Unified Parkinson Disease Rating Scale\n**5. Demographics** - 5 variables for basic demographic information that does not reveal patient\u2019s identity\n**6. Genetic** - 14 variables for Single Nucleotide Polymorphisms which are for genetic variation\n**7. Proteome** - 6 variables which depict the level of some proteins\n\nThe metadata viewing and management is done by Data Catalogue, a central web portal of MIP. Data Catalogue offers presentation, search and hierarchical visualisation of metadata information for datasets imported into the MIP while providing metadata management features to authorized users. One of its features is parsing metadata descriptions in XLSX files and generating their equivalent in a hierarchical JSON format which the MIP uses. In this repository we upload metadata in both XLSX and the generated JSON format.",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-sa/4.0"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [],
	"name": "Metadata Schema of the Common Data Elements for Dementia (v3)",
	"files": [
		{
			"byteSize": 52845.0,
			"hashcode": "91dbd43b4f28b27835bbda3a3e753149",
			"name": "dementia_cdes_v3.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000002_MIPdementia_pub/dementia_cdes_v3.json",
			"contentType": "application/json",
			"hash": "96ed096891d57fbbe7735891757e495d"
		},
		{
			"byteSize": 202888.0,
			"hashcode": "52243dd32b9db2c685a7474f124ab153",
			"name": "DataDescriptor_dementia-metadata.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000002_MIPdementia_pub/DataDescriptor_dementia-metadata.pdf",
			"contentType": "application/pdf",
			"hash": "664658415a12ad906df8c2e401bb8b64"
		},
		{
			"byteSize": 9039.0,
			"hashcode": "b0ad7d3908088d79a307611a99d9a210",
			"name": "dementia_hospitals.xlsx",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000002_MIPdementia_pub/dementia_hospitals.xlsx",
			"contentType": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
			"hash": "0e2cb5968f8ab103dc8ad1c6c16512ef"
		},
		{
			"byteSize": 19869.0,
			"hashcode": "97f1ede5c1b5b2604e99f6cc9bba78e1",
			"name": "dementia_cdes_v3.xlsx",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000002_MIPdementia_pub/dementia_cdes_v3.xlsx",
			"contentType": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
			"hash": "ac423db38f82a733defd457a0e963fe4"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/42b69ce8-9a73-4522-9be9-07f1ea8fb60d",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"name": "Vassalos, Vasilis",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"shortName": "Vassalos, V."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7ddce0b2-ce63-4d4a-b965-60089a749c9a",
			"name": "Spartalis, Iosif",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ddce0b2-ce63-4d4a-b965-60089a749c9a",
			"shortName": "Spartalis I."
		},
		{
			"schema.org/shortName": null,
			"identifier": "a0fdb87f-89ba-481d-8b99-325e03f53050",
			"name": "Abu-Nawwas, Laith",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a0fdb87f-89ba-481d-8b99-325e03f53050",
			"shortName": "Abu-Nawwas, L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "a0eed0ab-9655-4881-ab69-e069e9f57d49",
			"name": "Karozos, Kostis ",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a0eed0ab-9655-4881-ab69-e069e9f57d49",
			"shortName": "Karozos, K."
		}
	],
	"id": "42b69ce8-9a73-4522-9be9-07f1ea8fb60d",
	"kgReference": [
		"10.25493/GCFG-2MW"
	],
	"publications": [
		{
			"name": "Meta-data management and quality control for the medical informatics platform",
			"cite": "Demiraj A, Karozos K, Spartalis I, and Vassalos V. 2019. Meta-data management and quality control for the medical informatics platform. In Proceedings of the 23rd International Database Applications & Engineering Symposium ( IDEAS \u201919 ). Association for Computing Machinery, New York, NY, USA, Article 10, 1\u20139.",
			"doi": "https://doi.org/10.1145/3331076.3331088"
		}
	]
}
