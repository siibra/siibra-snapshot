{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area Id3 (Insula) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area Id3 (Insula). The probability map of Area Id3 (Insula) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.\n\nOther available data versions of Area Id3 (Insula):\nQuabs et al. (2019) [Data set, v7.1] [DOI: 10.25493/AE2S-KT6](https://doi.org/10.25493%2FAE2S-KT6)\nQuabs et al. (2020) [Data set, v8.0] [DOI: 10.25493/X499-RBU](https://doi.org/10.25493%2FX499-RBU)\n\nThe most probable delineation of Area Id3 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493/8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Id3 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/3dcfcfc2-035c-4785-a820-a671f2104ac3"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Id3 (Insula) (v9.1)",
	"files": [
		{
			"byteSize": 79920.0,
			"hashcode": "53e78bd185d54d4556a1d2ce5a21299c",
			"name": "Area-Id3_l_N10_nlin2Stdcolin27_9.1_publicDOI_3667ca630929ed730efab2e478fcc5bf.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id3_pub/9.1/Area-Id3_l_N10_nlin2Stdcolin27_9.1_publicDOI_3667ca630929ed730efab2e478fcc5bf.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "3667ca630929ed730efab2e478fcc5bf"
		},
		{
			"byteSize": 24.0,
			"hashcode": "3ded9dbcc0665c14f373877d99708b35",
			"name": "subjects_Area-Id3.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id3_pub/9.1/subjects_Area-Id3.csv",
			"contentType": "text/csv",
			"hash": "5f65aff2b50cea7b07f7c426a59762b3"
		},
		{
			"byteSize": 54558.0,
			"hashcode": "4244debc648fa685f5b465ea4a0275de",
			"name": "Area-Id3_l_N10_nlin2ICBM152asym2009c_9.1_publicDOI_3667ca630929ed730efab2e478fcc5bf.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id3_pub/9.1/Area-Id3_l_N10_nlin2ICBM152asym2009c_9.1_publicDOI_3667ca630929ed730efab2e478fcc5bf.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "eda7d11240a29ac538c7b61ec2dbf942"
		},
		{
			"byteSize": 59213.0,
			"hashcode": "36d1761c4869bca54837b7b5e08eba1c",
			"name": "Area-Id3_r_N10_nlin2ICBM152asym2009c_9.1_publicDOI_4612d50e0e06dbbeaf3f11c7dda1ac66.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id3_pub/9.1/Area-Id3_r_N10_nlin2ICBM152asym2009c_9.1_publicDOI_4612d50e0e06dbbeaf3f11c7dda1ac66.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "b52ecbff4d89fadc94cfc02429a50ea7"
		},
		{
			"byteSize": 83576.0,
			"hashcode": "be1d02655283c8c411d610542866c980",
			"name": "Area-Id3_r_N10_nlin2Stdcolin27_9.1_publicDOI_4612d50e0e06dbbeaf3f11c7dda1ac66.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id3_pub/9.1/Area-Id3_r_N10_nlin2Stdcolin27_9.1_publicDOI_4612d50e0e06dbbeaf3f11c7dda1ac66.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4612d50e0e06dbbeaf3f11c7dda1ac66"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "c7e8d17c2a6fe5fd0d40063de26da32a",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id3_pub/9.1/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/d7a071d6-01d2-4642-8c6f-5b678abd601a",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "005d97d8-cd14-4cc2-a6fc-bca56655e7e7",
			"name": "Kossakowski, Klaudia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/005d97d8-cd14-4cc2-a6fc-bca56655e7e7",
			"shortName": "Kossakowski, K."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"name": "Quabs, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"shortName": "Quabs, J."
		}
	],
	"id": "d7a071d6-01d2-4642-8c6f-5b678abd601a",
	"kgReference": [
		"10.25493/5DC3-K2A"
	],
	"publications": []
}
