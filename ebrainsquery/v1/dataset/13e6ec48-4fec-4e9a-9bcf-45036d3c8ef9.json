{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Wagstyl, K. , Larocque, S., Cucurull, G., Lepage, C., Cohen, J. P., Bludau, S., Palomero-Gallagher, N., Funck, T., Spitzer, H., Dickscheid, T., Fletcher, P. C., Romero, A., Zilles, K., Amunts, K., Bengio, Y., Evans, A. C (2019) Automated segmentation of cortical layers in BigBrain reveals divergent cortical and laminar thickness gradients in sensory and motor cortices. bioRxiv 580597; Now published in PLOS Biology doi: 10.1371/journal.pbio.3000678",
			"doi": "10.1101/580597"
		},
		{
			"cite": "Wagstyl, K. , Larocque, S., Cucurull, G., Lepage, C., Cohen, J. P., Bludau, S., Palomero-Gallagher, N., Lewis, L. B., Funck, T., Spitzer, H., Dickscheid, T., Fletcher, P. C., Romero, A., Zilles, K., Amunts, K., Bengio, Y., Evans, A. C (2020). BigBrain 3D atlas of cortical layers: Cortical and laminar thickness gradients diverge in sensory and motor cortices. PLoS Biol. 18(4): e3000678",
			"doi": "10.1371/journal.pbio.3000678"
		}
	],
	"activity": [
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"analysis technique"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "BigBrain",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/a1655b99-82f1-420f-a3c2-fe80fd4c8588"
		}
	],
	"methods": [
		"Spatial atlas registration",
		"cytoarchitectonic mapping",
		"silver staining",
		"magnetic resonance imaging (MRI)"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "baec0381f1f9af275705aeecf62f691c",
			"name": "Evans, Alan C.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/62e2002e-456e-4f15-91cc-5c1538520e38",
			"shortName": "Evans, A. C."
		}
	],
	"project": [],
	"description": "The cerebral isocortex has six cytoarchitectonic layers that vary depending on cortical area and local morphology. This datasets provides a 3D segmentation of all cortical and laminar surfaces in the BigBrain, a high-resolution, 3D histological model of the human brain. The segmentation has been computed automatically based on histological intensities along 3D cortical profiles which were sampled between the pial and white matter throughout the dataset. These cortical profiles were segmented into layers using a convolutional neural network. Training profiles were generated from examples of manually segmented layers on cortical regions from 2D histological sections of the BigBrain. From the segmented intensity profiles, surface meshes and voxel masks of all six cortical layers in the space of the Big Brain have been computed.\n\nIn Wagstyl et al. (2019), these surfaces were used to evaluate cortical thickness gradients and the contributions of different cortical lamina to these gradients. Geodesic surface distances from primary visual, auditory, somatosensory and motor areas were calculated and used as a marker of hierarchical progression.\n",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [
		"Homo sapiens"
	],
	"name": "Automated segmentation of cortical layers of the BigBrain",
	"files": [],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/13e6ec48-4fec-4e9a-9bcf-45036d3c8ef9",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "baec0381f1f9af275705aeecf62f691c",
			"name": "Evans, Alan C.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/62e2002e-456e-4f15-91cc-5c1538520e38",
			"shortName": "Evans, A. C."
		},
		{
			"schema.org/shortName": "Bengio, Y.",
			"identifier": "3191a2d4-d695-4ab4-9fe4-2537f77996df",
			"name": "Bengio, Yoshua",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3191a2d4-d695-4ab4-9fe4-2537f77996df",
			"shortName": "Bengio, Y."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Romero, A.",
			"identifier": "9ba5addf-e74e-45f4-90f0-0f1ee960f36b",
			"name": "Romero, Adriana",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9ba5addf-e74e-45f4-90f0-0f1ee960f36b",
			"shortName": "Romero, A."
		},
		{
			"schema.org/shortName": "Flechter, P.C.",
			"identifier": "054226e1-16a1-4c72-943d-d36226b309fb",
			"name": "Fletcher, Paul C",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/054226e1-16a1-4c72-943d-d36226b309fb",
			"shortName": "Fletcher, P.C."
		},
		{
			"schema.org/shortName": "Dickscheid, T.",
			"identifier": "6815ac666477e233eb244713ed5cf8dd",
			"name": "Dickscheid, Timo",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ac2d5fde-4202-47da-97c5-87f7a35a8efd",
			"shortName": "Dickscheid, T."
		},
		{
			"schema.org/shortName": "Spitzer, H.",
			"identifier": "39870547-5786-42b7-af4c-63db65fce5e0",
			"name": "Spitzer, Hannah",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/39870547-5786-42b7-af4c-63db65fce5e0",
			"shortName": "Spitzer, H."
		},
		{
			"schema.org/shortName": "Funck, T.",
			"identifier": "731057f1-a0e0-453e-9c20-ec274b47d7fd",
			"name": "Funck, Thomas",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/731057f1-a0e0-453e-9c20-ec274b47d7fd",
			"shortName": "Funck, T."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Cohen, J.P.",
			"identifier": "13102cad-d926-4522-8835-63f28ba220ae",
			"name": "Cohen, Joseph Paul ",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/13102cad-d926-4522-8835-63f28ba220ae",
			"shortName": "Cohen, J.P."
		},
		{
			"schema.org/shortName": "Lepage, C.",
			"identifier": "fded1c90-14c6-4191-98d4-df603d60e049",
			"name": "Lepage, Claude",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/fded1c90-14c6-4191-98d4-df603d60e049",
			"shortName": "Lepage, C."
		},
		{
			"schema.org/shortName": "Cucurull, G.",
			"identifier": "3a321146-5c78-450c-99c9-c9d4a8568577",
			"name": "Cucurull, Guillem",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3a321146-5c78-450c-99c9-c9d4a8568577",
			"shortName": "Cucurull, G."
		},
		{
			"schema.org/shortName": "Larocque, S.",
			"identifier": "7ddc84ee-4cd5-45fd-9356-bf74caba9fbb",
			"name": "Larocque, St\u00e9phanie",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ddc84ee-4cd5-45fd-9356-bf74caba9fbb",
			"shortName": "Larocque, S."
		},
		{
			"schema.org/shortName": "Wagstyl, K.",
			"identifier": "82826594-9928-4b90-a12c-019adf01584e",
			"name": "Wagstyl, Konrad ",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/82826594-9928-4b90-a12c-019adf01584e",
			"shortName": "Wagstyl, K."
		}
	],
	"id": "13e6ec48-4fec-4e9a-9bcf-45036d3c8ef9",
	"kgReference": [
		"10.1371/journal.pbio.3000678"
	],
	"publications": [
		{
			"name": "Automated segmentation of cortical layers in BigBrain reveals divergent cortical and laminar thickness gradients in sensory and motor cortices.",
			"cite": "Wagstyl, K. , Larocque, S., Cucurull, G., Lepage, C., Cohen, J. P., Bludau, S., Palomero-Gallagher, N., Funck, T., Spitzer, H., Dickscheid, T., Fletcher, P. C., Romero, A., Zilles, K., Amunts, K., Bengio, Y., Evans, A. C (2019) Automated segmentation of cortical layers in BigBrain reveals divergent cortical and laminar thickness gradients in sensory and motor cortices. bioRxiv 580597; Now published in PLOS Biology doi: 10.1371/journal.pbio.3000678",
			"doi": "10.1101/580597"
		},
		{
			"name": "BigBrain 3D atlas of cortical layers: Cortical and laminar thickness gradients diverge in sensory and motor cortices ",
			"cite": "Wagstyl, K. , Larocque, S., Cucurull, G., Lepage, C., Cohen, J. P., Bludau, S., Palomero-Gallagher, N., Lewis, L. B., Funck, T., Spitzer, H., Dickscheid, T., Fletcher, P. C., Romero, A., Zilles, K., Amunts, K., Bengio, Y., Evans, A. C (2020). BigBrain 3D atlas of cortical layers: Cortical and laminar thickness gradients diverge in sensory and motor cortices. PLoS Biol. 18(4): e3000678",
			"doi": "10.1371/journal.pbio.3000678"
		}
	]
}
