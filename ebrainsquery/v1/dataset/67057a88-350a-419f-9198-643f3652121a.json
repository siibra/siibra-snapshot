{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Palomero-Gallagher, N., Eickhoff, S. B., Hoffstaedter, F., Schleicher, A., Mohlberg, H., Vogt, B. A., \u2026 Zilles, K. (2015). Functional organization of human subgenual cortical areas: Relationship between architectonical segregation and connectional heterogeneity. NeuroImage, 115, 177\u2013190. ",
			"doi": "10.1016/j.neuroimage.2015.04.053"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area s32 (sACC) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area s32 (sACC). The probability map of Area s32 (sACC) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area s32 (sACC):\nPalomero-Gallagher et al. (2019) [Data set, v16.1] [DOI: 10.25493/XTRR-172](https://doi.org/10.25493%2FXTRR-172)\n\nThe most probable delineation of Area s32 (sACC) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area s32 (sACC)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/61b44255-ae3a-4a23-b1bc-7d303a48dbd3"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area s32 (sACC) (v16.0)",
	"files": [
		{
			"byteSize": 91238.0,
			"hashcode": "c262f0a9c9e75512de1372d3450f5009",
			"name": "Area-s32_l_N10_nlin2Stdcolin27_16.0_publicP_caa4e0f5b7576f23798f6d4df3d0a86d.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-s32_pub/16.0/Area-s32_l_N10_nlin2Stdcolin27_16.0_publicP_caa4e0f5b7576f23798f6d4df3d0a86d.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "caa4e0f5b7576f23798f6d4df3d0a86d"
		},
		{
			"byteSize": 69098.0,
			"hashcode": "1a74568ab870091027981d37858a3ee2",
			"name": "Area-s32_l_N10_nlin2ICBM152ASYM2009C_16.0_publicP_36217f45c5a516c4b2d2729790ce263e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-s32_pub/16.0/Area-s32_l_N10_nlin2ICBM152ASYM2009C_16.0_publicP_36217f45c5a516c4b2d2729790ce263e.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "36217f45c5a516c4b2d2729790ce263e"
		},
		{
			"byteSize": 25.0,
			"hashcode": "47bea83e571adc81ef1187819816a25d",
			"name": "subjects_Area-s32.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-s32_pub/16.0/subjects_Area-s32.csv",
			"contentType": "text/csv",
			"hash": "ffd504b3c0f37d8e6d7e02d0a9a6b36c"
		},
		{
			"byteSize": 90777.0,
			"hashcode": "ac58ff0209bdfba2648b3f94041eea5a",
			"name": "Area-s32_r_N10_nlin2Stdcolin27_16.0_publicP_16f2e8a95caf1d10cf8cd08e02bc37e9.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-s32_pub/16.0/Area-s32_r_N10_nlin2Stdcolin27_16.0_publicP_16f2e8a95caf1d10cf8cd08e02bc37e9.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "16f2e8a95caf1d10cf8cd08e02bc37e9"
		},
		{
			"byteSize": 69847.0,
			"hashcode": "72c3febdd41c32b686e060269a7de4e1",
			"name": "Area-s32_r_N10_nlin2ICBM152ASYM2009C_16.0_publicP_15e36bc4b5da8a2f9541e6f198fdb5c1.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-s32_pub/16.0/Area-s32_r_N10_nlin2ICBM152ASYM2009C_16.0_publicP_15e36bc4b5da8a2f9541e6f198fdb5c1.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "15e36bc4b5da8a2f9541e6f198fdb5c1"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/67057a88-350a-419f-9198-643f3652121a",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "34c04d30add1e771ef4cedf54c59d89d",
			"name": "Vogt, Brent A.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5f88e480-660a-4b6e-a6da-57979529ab6f",
			"shortName": "Vogt, B."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Hoffstaedter, F.",
			"identifier": "fea7d2c30b595f5a45a7093d7beea7b7",
			"name": "Hoffstaedter, Felix",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/86af969b-c570-4f51-80ba-571de55e817b",
			"shortName": "Hoffstaedter, F."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "439d1470b132315451f25156c73a1f01",
	"kgReference": [
		"10.25493/3PBV-WH0"
	],
	"publications": [
		{
			"name": "Functional organization of human subgenual cortical areas: Relationship between architectonical segregation and connectional heterogeneity",
			"cite": "Palomero-Gallagher, N., Eickhoff, S. B., Hoffstaedter, F., Schleicher, A., Mohlberg, H., Vogt, B. A., \u2026 Zilles, K. (2015). Functional organization of human subgenual cortical areas: Relationship between architectonical segregation and connectional heterogeneity. NeuroImage, 115, 177\u2013190. ",
			"doi": "10.1016/j.neuroimage.2015.04.053"
		}
	]
}
