{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Geyer, S., Ledberg, A., Schleicher, A., Kinomura, S., Schormann, T., B\u00fcrgel, U., \u2026 Roland, P. E. (1996). Two different areas within the primary motor cortex of man. Nature, 382(6594), 805\u2013807. ",
			"doi": "10.1038/382805a0"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area 4p (PreCG) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area 4p (PreCG). The probability map of Area 4p (PreCG) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area 4p (PreCG):\nGeyer et al. (2019) [Data set, v9.4] [DOI: 10.25493/5HSF-81J](https://doi.org/10.25493%2F5HSF-81J)\n\nThe most probable delineation of Area 4p (PreCG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 4p (PreCG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/82e6e826-a439-41db-84ff-4674ca3d643a"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area 4p (PreCG) (v9.2)",
	"files": [
		{
			"byteSize": 129068.0,
			"hashcode": "c5fdb69f4e5fafd69434eee48fd965d4",
			"name": "Area-4p_r_N10_nlin2Stdcolin27_9.2_publicP_1fec559a7712d875e5926b8a1050f5a6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-4p_pub/9.2/Area-4p_r_N10_nlin2Stdcolin27_9.2_publicP_1fec559a7712d875e5926b8a1050f5a6.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "1fec559a7712d875e5926b8a1050f5a6"
		},
		{
			"byteSize": 121310.0,
			"hashcode": "9667b8f780a5591bf3e9aa7f4ec303df",
			"name": "Area-4p_r_N10_nlin2ICBM152ASYM2009C_9.2_publicP_b060996859a000377ffa7e8aa666d362.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-4p_pub/9.2/Area-4p_r_N10_nlin2ICBM152ASYM2009C_9.2_publicP_b060996859a000377ffa7e8aa666d362.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "b060996859a000377ffa7e8aa666d362"
		},
		{
			"byteSize": 131745.0,
			"hashcode": "e315f5d0439f55bcc97daf2e4f60913c",
			"name": "Area-4p_l_N10_nlin2ICBM152ASYM2009C_9.2_publicP_015166cbecfab0495643f76dd2abd388.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-4p_pub/9.2/Area-4p_l_N10_nlin2ICBM152ASYM2009C_9.2_publicP_015166cbecfab0495643f76dd2abd388.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "015166cbecfab0495643f76dd2abd388"
		},
		{
			"byteSize": 132973.0,
			"hashcode": "64ea864dda8f2411a592b91183e86ed3",
			"name": "Area-4p_l_N10_nlin2Stdcolin27_9.2_publicP_ee0af6a47221e13e2ec2ff70686e3d1e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-4p_pub/9.2/Area-4p_l_N10_nlin2Stdcolin27_9.2_publicP_ee0af6a47221e13e2ec2ff70686e3d1e.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "ee0af6a47221e13e2ec2ff70686e3d1e"
		},
		{
			"byteSize": 21.0,
			"hashcode": "1dfd419526565ab8ce0ce558593df033",
			"name": "subjects_Area-4p.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-4p_pub/9.2/subjects_Area-4p.csv",
			"contentType": "text/csv",
			"hash": "895362c2073b778e508605738bd96a63"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/0a9acf33-2bc4-40af-a0cd-d85e3777aa19",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "88170aec3154260af796dcfe393fc9fa",
			"name": "Roland, Per E.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/14181b4a-fd94-4e19-885b-70d8a0dd8146",
			"shortName": "Roland, P."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "85578cdf71b7726dbf0bc8b520974561",
			"name": "Larsson, Jonas",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f99849d2-d882-412c-a500-385d9ee1df14",
			"shortName": "Larsson, J."
		},
		{
			"schema.org/shortName": null,
			"identifier": "6bdec65e966cc8d762ba03d103e91bc1",
			"name": "Klingberg, Torkel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/0d13a827-b3c1-4721-a180-fc228e398bae",
			"shortName": "Klingberg, T."
		},
		{
			"schema.org/shortName": null,
			"identifier": "d1d4965e13b1ba7a7c3c93ccd1fb7020",
			"name": "B\u00fcrgel, Uli",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8f12ca15-042f-4233-9dfb-871a018f686c",
			"shortName": "B\u00fcrgel, U."
		},
		{
			"schema.org/shortName": null,
			"identifier": "e537fd8ddd6926b8b364e4bdb49c255e",
			"name": "Kinomura, Shigeo",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f1cc57fb-4339-44af-bf47-4cd66b87b723",
			"shortName": "Kinomura, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3cdff58ec3ceb1f1daf84ef5cf7fbd27",
			"name": "Ledberg, Anders",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/6dc6cd88-4c6b-4947-aafa-ae8861fe5c30",
			"shortName": "Ledberg, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "183ac28a5978832f25623307522cba98",
			"name": "Geyer, Stefan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a38f341-5b88-4cb2-b8b7-6473a676d892",
			"shortName": "Geyer, S."
		}
	],
	"id": "9c8bf7b9620288e37de914e14514a7cb",
	"kgReference": [
		"10.25493/ECYA-0D1"
	],
	"publications": [
		{
			"name": "Two different areas within the primary motor cortex of man",
			"cite": "Geyer, S., Ledberg, A., Schleicher, A., Kinomura, S., Schormann, T., B\u00fcrgel, U., \u2026 Roland, P. E. (1996). Two different areas within the primary motor cortex of man. Nature, 382(6594), 805\u2013807. ",
			"doi": "10.1038/382805a0"
		}
	]
}
