{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Mikulan E, Russo S, Parmigiani S, Sarasso S, Zauli F, Rubino A, Avanzini P, Cattani A, Sorrentino A, Gibbs S, Cardinale F, Sartori I, Nobili L, Massimini M, Pigorini A, (2020) Localize-MI. G-Node.",
			"doi": "https://doi.org/10.12751/g-node.1cc1ae"
		},
		{
			"cite": "Mikulan, E., Russo, S., Parmigiani, S. et al. Simultaneous human intracerebral stimulation and HD-EEG, ground-truth for source localization methods. Sci Data 7, 127 (2020).",
			"doi": "https://doi.org/10.1038/s41597-020-0467-x"
		}
	],
	"activity": [
		{
			"protocols": [],
			"preparation": [
				"In vivo"
			]
		},
		{
			"protocols": [
				"sleep",
				"simultaneous recordings"
			],
			"preparation": [
				"In vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"magnetic resonance imaging (MRI)",
		"high-density electroencephalography (hd-EEG)",
		"stereotactic electroencephalography (sEEG)",
		"single pulse electrical stimulation (SPES)"
	],
	"custodians": [
		{
			"schema.org/shortName": null,
			"identifier": "f7889135-f3af-45d0-81f5-b45cac873769",
			"name": "Massimini, Marcello",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f7889135-f3af-45d0-81f5-b45cac873769",
			"shortName": "Massimini M."
		}
	],
	"project": [],
	"description": "Precisely localizing the sources of brain activity as recorded by EEG is a fundamental procedure and a major challenge for both research and clinical practice. Even though many methods and algorithms have been proposed, their relative advantages and limitations are still not well established. Moreover, these methods involve tuning multiple parameters, for which no principled way of selection exists yet. These uncertainties are emphasized due to the lack of ground-truth for their validation and testing. Here we present the Localize-MI dataset, which constitutes the first open dataset that comprises EEG recorded electrical activity originating from precisely known locations inside the brain of living humans. High-density EEG was recorded as single-pulse biphasic currents were delivered at intensities ranging from 0.1 to 5 mA through stereotactically implanted electrodes in diverse brain regions during pre-surgical evaluation of patients with drug- resistant epilepsy. The uses of this dataset range from the estimation of in vivo tissue conductivity to the development, validation and testing of forward and inverse solution methods. \n\nThe data descriptor of this dataset is published in *Scientific Data* (Mikulan et al. 2020). The online version of this article can be found here: [https://doi.org/10.1038/s41597-020-0467-x](https://doi.org/10.1038/s41597-020-0467-x).",
	"parcellationAtlas": [
		{
			"name": "Desikan-Killiany Atlas (2006)",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/fe5ac517-1720-43cc-bbf6-e50be7cf08ff",
			"id": "fe5ac517-1720-43cc-bbf6-e50be7cf08ff"
		}
	],
	"licenseInfo": [
		{
			"name": "The use of this dataset requires that the user cites the associated DOI and adheres to the conditions of use that are contained in the Data Use Agreement.",
			"url": null
		}
	],
	"embargoStatus": [
		{
			"identifier": "1d726b76-b176-47ed-96f0-b4f2e17d5f19",
			"name": "Under review",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/1d726b76-b176-47ed-96f0-b4f2e17d5f19"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "isthmus cingulate cortex (right hemisphere)",
			"alias": "isthmuscingulate_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/12f7fe2f-d4a0-4a2a-b460-67adfe454a27"
		},
		{
			"species": [],
			"name": "middle temporal gyrus (right hemisphere)",
			"alias": "middletemporal_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/b52d3921-3620-4cd4-ab1c-020c54610015"
		},
		{
			"species": [],
			"name": "parahippocampal gyrus (left hemisphere)",
			"alias": "parahippocampal_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/65aa0e7e-ed47-4b11-a625-8689fd1daea1"
		},
		{
			"species": [],
			"name": "inferior temporal gyrus (left hemisphere)",
			"alias": "inferiortemporal_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/3867bde3-6387-401f-9212-674d35b1e327"
		},
		{
			"species": [],
			"name": "lingual gyrus (left hemisphere)",
			"alias": "lingual_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/64f7903e-5612-43da-bd1b-c4eaa4d88a34"
		},
		{
			"species": [],
			"name": "superior frontal gyrus (left hemisphere)",
			"alias": "superiorfrontal_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/5a6ab8ac-e5a6-4c14-9593-ac8f62c8a9bc"
		},
		{
			"species": [],
			"name": "medial orbito frontal cortex (left hemisphere)",
			"alias": "medialorbitofrontal_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/1bd4ba02-6feb-4b7e-b43b-fc9831f6647c"
		},
		{
			"species": [],
			"name": "rostral anterior cingulate cortex (left hemisphere)",
			"alias": "rostralanteriorcingulate_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/755a3c47-aee5-4d87-8510-b9db78c4d82b"
		},
		{
			"species": [],
			"name": "rostral middle frontal_left hemisphere",
			"alias": "rostralmiddlefrontal_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/9354c09c-6cd6-4413-b978-f82a72dadd3c"
		},
		{
			"species": [],
			"name": "superior temporal gyrus (right hemisphere)",
			"alias": "superiortemporal_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/9fec82ef-648b-47dd-a50b-a1ba310d2eac"
		},
		{
			"species": [],
			"name": "inferior parietal cortex (right hemisphere)",
			"alias": "inferiorparietal_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/5e4e0c60-22d2-489c-966a-d5464069bbcd"
		},
		{
			"species": [],
			"name": "precuneus cortex (right hemisphere)",
			"alias": "precuneus_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/40e4148f-305b-4bd7-8bde-0b765606755c"
		},
		{
			"species": [],
			"name": "lateral occipital cortex (right hemisphere)",
			"alias": "lateraloccipital_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/241c5922-97c9-4481-8cc7-0e6054541ed9"
		},
		{
			"species": [],
			"name": "pericalcarine cortex (right hemisphere)",
			"alias": "pericalcarine_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/d1e317a1-bde8-42cb-b3d6-57562becee2c"
		},
		{
			"species": [],
			"name": "precentral gryrus (left hemisphere)",
			"alias": "precentral_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/0a8f2a71-461f-49fd-abb1-df21d9539ca0"
		},
		{
			"species": [],
			"name": "superior temporal gyrus (left hemisphere)",
			"alias": "superiortemporal_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/6c5fedb4-1568-4462-99fe-a9e4edb7c737"
		},
		{
			"species": [],
			"name": "transverse temporal cortex (left hemisphere)",
			"alias": "transversetemporal_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/588ebee9-4d77-461f-a814-f608d69e59f2"
		},
		{
			"species": [],
			"name": "middle temporal (left hemisphere)",
			"alias": "middletemporal _lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/c5a0da0d-d888-4706-88ae-d264beeadc9d"
		},
		{
			"species": [],
			"name": "pars triangularis (left hemisphere)",
			"alias": "parstriangularis_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/db6f34bc-a1a9-41b6-a6af-1683783e25ed"
		},
		{
			"species": [],
			"name": "pars opercularis (left hemisphere)",
			"alias": "parsopercularis_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/5a45c431-30d8-4576-85c0-fdc719a6b301"
		},
		{
			"species": [],
			"name": "insula (left hemisphere)",
			"alias": "insula_lh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/0fefc558-3db4-4801-928a-3df2aaf76429"
		},
		{
			"species": [],
			"name": "supramarginal gyrus (right hemisphere)",
			"alias": "supramarginal_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/c8f32262-b4ed-4068-ae71-9129c86f426a"
		},
		{
			"species": [],
			"name": "Insula (right hemisphere)",
			"alias": "insula_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/46b553a9-0104-4844-beec-aaf41411be68"
		},
		{
			"species": [],
			"name": "paracentral lobule (right hemisphere)",
			"alias": "paracentral_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/e28c126e-d55e-4b56-981b-8ec2a352ac95"
		},
		{
			"species": [],
			"name": "postcentral gyrus (right hemisphere)",
			"alias": "postcentral_rh",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/6d1e7a61-8dd5-4688-9aaa-8c6fdfca4ab1"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Simultaneous human intracerebral stimulation and HD-EEG, ground-truth for source localization methods",
	"files": [],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/f557d71e-fe11-43d7-8225-7c2d432f34b9",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "c34bcb3c-fc36-40ae-92fd-d2adb4689f6d",
			"name": "Pigorini, Andrea",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c34bcb3c-fc36-40ae-92fd-d2adb4689f6d",
			"shortName": "Pigorini A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "f7889135-f3af-45d0-81f5-b45cac873769",
			"name": "Massimini, Marcello",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f7889135-f3af-45d0-81f5-b45cac873769",
			"shortName": "Massimini M."
		},
		{
			"schema.org/shortName": null,
			"identifier": "d39d5d72-6581-4413-8b68-1e12b4acfb1f",
			"name": "Nobili, Lino",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d39d5d72-6581-4413-8b68-1e12b4acfb1f",
			"shortName": "Nobili L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "8ada78b8-a20a-4d50-98aa-12bc5ad7b8f6",
			"name": "Sartori, Ivana",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8ada78b8-a20a-4d50-98aa-12bc5ad7b8f6",
			"shortName": "Sartori I."
		},
		{
			"schema.org/shortName": null,
			"identifier": "e3044e58-e2b2-4ef4-b60c-b706230d8391",
			"name": "Cardinale, Francesco",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/e3044e58-e2b2-4ef4-b60c-b706230d8391",
			"shortName": "Cardinale F."
		},
		{
			"schema.org/shortName": null,
			"identifier": "0263955c-3a51-4830-94d9-3c85d7416534",
			"name": "Gibbs, Steve",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/0263955c-3a51-4830-94d9-3c85d7416534",
			"shortName": "Gibbs S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "713fd34e-2e8c-4b94-b52f-e8720044e43f",
			"name": "Sorrentino, Alberto",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/713fd34e-2e8c-4b94-b52f-e8720044e43f",
			"shortName": "Sorrentino A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7e93aba4-cd6b-427f-a077-84f91ee59ccc",
			"name": "Cattani, Anna",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e93aba4-cd6b-427f-a077-84f91ee59ccc",
			"shortName": "Cattani A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "6af522e6-cc74-4d36-88c1-1f77bade8c64",
			"name": "Avanzini, Pietro",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/6af522e6-cc74-4d36-88c1-1f77bade8c64",
			"shortName": "Avanzini P."
		},
		{
			"schema.org/shortName": null,
			"identifier": "859629b2-165c-4937-8d0e-823bf39d2427",
			"name": "Rubino, Annalisa",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/859629b2-165c-4937-8d0e-823bf39d2427",
			"shortName": "Rubino A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "5571b15c-b8e1-4058-a51b-2264a3eddc4d",
			"name": "Zauli, Flavia Maria",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5571b15c-b8e1-4058-a51b-2264a3eddc4d",
			"shortName": "Zauli F.M."
		},
		{
			"schema.org/shortName": null,
			"identifier": "045b6b0a-512c-41ee-88dd-861efcd53794",
			"name": "Sarasso, Simone",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/045b6b0a-512c-41ee-88dd-861efcd53794",
			"shortName": "Sarasso S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "31719148-bc77-4682-a73f-58b8749357ec",
			"name": "Parmigiani, Sara",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/31719148-bc77-4682-a73f-58b8749357ec",
			"shortName": "Parmigiani S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "a9bd1df9-7846-4dd8-868d-1cbac976533e",
			"name": "Russo, Simone",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a9bd1df9-7846-4dd8-868d-1cbac976533e",
			"shortName": "Russo S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "af9f857f-f070-4376-a799-4b373d6a893f",
			"name": "Mikulan, Ezequiel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/af9f857f-f070-4376-a799-4b373d6a893f",
			"shortName": "Mikulan E."
		}
	],
	"id": "f557d71e-fe11-43d7-8225-7c2d432f34b9",
	"kgReference": [
		"10.25493/NXN2-05W"
	],
	"publications": [
		{
			"name": "Localize-MI",
			"cite": "Mikulan E, Russo S, Parmigiani S, Sarasso S, Zauli F, Rubino A, Avanzini P, Cattani A, Sorrentino A, Gibbs S, Cardinale F, Sartori I, Nobili L, Massimini M, Pigorini A, (2020) Localize-MI. G-Node.",
			"doi": "https://doi.org/10.12751/g-node.1cc1ae"
		},
		{
			"name": "Simultaneous human intracerebral stimulation and HD-EEG, ground- truth for source localization methods7",
			"cite": "Mikulan, E., Russo, S., Parmigiani, S. et al. Simultaneous human intracerebral stimulation and HD-EEG, ground-truth for source localization methods. Sci Data 7, 127 (2020).",
			"doi": "https://doi.org/10.1038/s41597-020-0467-x"
		}
	]
}
