{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Kurth, F., Eickhoff, S. B., Schleicher, A., Hoemke, L., Zilles, K., & Amunts, K. (2009). Cytoarchitecture and Probabilistic Maps of the Human Posterior Insular Cortex. Cerebral Cortex, 20(6), 1448\u20131461. ",
			"doi": "10.1093/cercor/bhp208"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Ig2 (Insula) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area Ig2 (Insula). The probability map of Area Ig2 (Insula) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area Ig2 (Insula):\nKurth et al. (2018) [Data set, v11.0] [DOI: 10.25493/N7C7-BHW](https://doi.org/10.25493%2FN7C7-BHW)\n\nThe most probable delineation of Area Ig2 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Ig2 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/49092952-1eef-4b89-b8bf-1bf1f25f149a"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Ig2 (Insula) (v13.1)",
	"files": [
		{
			"byteSize": 24.0,
			"hashcode": "872c6daf625cf485ed4c978d1afe5558",
			"name": "subjects_Area-Ig2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/13.1/subjects_Area-Ig2.csv",
			"contentType": "text/csv",
			"hash": "5f65aff2b50cea7b07f7c426a59762b3"
		},
		{
			"byteSize": 79678.0,
			"hashcode": "228e64a376fa0888a57021994d205a03",
			"name": "Area-Ig2_r_N10_nlin2Stdcolin27_13.1_publicP_d7cbe960f4d01de62a6621ba53084fae.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/13.1/Area-Ig2_r_N10_nlin2Stdcolin27_13.1_publicP_d7cbe960f4d01de62a6621ba53084fae.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "d7cbe960f4d01de62a6621ba53084fae"
		},
		{
			"byteSize": 52536.0,
			"hashcode": "ad4419413a636ccb96c5cc839d319482",
			"name": "Area-Ig2_l_N10_nlin2MNI152ASYM2009C_13.1_publicP_e1defc10cb006c64ad31bf10ddb5b1a1.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/13.1/Area-Ig2_l_N10_nlin2MNI152ASYM2009C_13.1_publicP_e1defc10cb006c64ad31bf10ddb5b1a1.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "e1defc10cb006c64ad31bf10ddb5b1a1"
		},
		{
			"byteSize": 53093.0,
			"hashcode": "be160383d12a6ac31abd4611ca767b04",
			"name": "Area-Ig2_r_N10_nlin2MNI152ASYM2009C_13.1_publicP_cf1b3cc5b32d89ea8937c189f218f726.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/13.1/Area-Ig2_r_N10_nlin2MNI152ASYM2009C_13.1_publicP_cf1b3cc5b32d89ea8937c189f218f726.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "cf1b3cc5b32d89ea8937c189f218f726"
		},
		{
			"byteSize": 78189.0,
			"hashcode": "2847d21abc49b4aea64d6a0348b18184",
			"name": "Area-Ig2_l_N10_nlin2Stdcolin27_13.1_publicP_f02dd3446cff31f94e4f75b1dd145122.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig2_pub/13.1/Area-Ig2_l_N10_nlin2Stdcolin27_13.1_publicP_f02dd3446cff31f94e4f75b1dd145122.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "f02dd3446cff31f94e4f75b1dd145122"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/f06b5ac4-3dff-4f92-a9b5-9a16fd4cf147",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "274457d14fdc8db443d4765a3ac061ba",
			"name": "Hoemke, L.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/323aa606-da3a-42a1-b16f-e07887dc43ec",
			"shortName": "Hoemke, L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "28cd2bbf034cb2518c8d67db992c5d64",
			"name": "Kurth, F.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9705b35c-3857-438b-b04f-f6a8ab79926c",
			"shortName": "Kurth, F."
		}
	],
	"id": "f06b5ac4-3dff-4f92-a9b5-9a16fd4cf147",
	"kgReference": [
		"10.25493/662G-E0W"
	],
	"publications": [
		{
			"name": "Cytoarchitecture and Probabilistic Maps of the Human Posterior Insular Cortex",
			"cite": "Kurth, F., Eickhoff, S. B., Schleicher, A., Hoemke, L., Zilles, K., & Amunts, K. (2009). Cytoarchitecture and Probabilistic Maps of the Human Posterior Insular Cortex. Cerebral Cortex, 20(6), 1448\u20131461. ",
			"doi": "10.1093/cercor/bhp208"
		}
	]
}
