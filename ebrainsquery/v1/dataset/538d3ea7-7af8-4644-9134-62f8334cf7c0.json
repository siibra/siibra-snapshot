{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Rademacher, J., Morosan, P., Schormann, T., Schleicher, A., Werner, C., Freund, H.-J., & Zilles, K. (2001). Probabilistic Mapping and Volume Measurement of Human Primary Auditory Cortex. NeuroImage, 13(4), 669\u2013683. ",
			"doi": "10.1006/nimg.2000.0714"
		},
		{
			"cite": "Morosan, P., Rademacher, J., Schleicher, A., Amunts, K., Schormann, T., & Zilles, K. (2001). Human Primary Auditory Cortex: Cytoarchitectonic Subdivisions and Mapping into a Spatial Reference System. NeuroImage, 13(4), 684\u2013701. ",
			"doi": "10.1006/nimg.2000.0715"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area TE 1.0 (HESCHL) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area TE 1.0 (HESCHL). The probability map of Area TE 1.0 (HESCHL) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area TE 1.0 (HESCHL):\nMorosan et al. (2018) [Data set, v5.0] [DOI: 10.25493/CP2T-FYT](https://doi.org/10.25493%2FCP2T-FYT)\nMorosan et al. (2019) [Data set, v5.1] [DOI: 10.25493/MV3G-RET](https://doi.org/10.25493%2FMV3G-RET) \nMorosan et al. (2020) [Data set, v6.0] [DOI: 10.25493/AKAE-ZWP](https://doi.org/10.25493%2FAKAE-ZWP)\n\nThe most probable delineation of Area TE 1.0 (HESCHL) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 1.0 (HESCHL)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/13e21153-2ba8-4212-b172-8894f1012225"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area TE 1.0 (HESCHL) (v6.2)",
	"files": [
		{
			"byteSize": 87085.0,
			"hashcode": "7b8f992c9f4f8217f37292516b35ae64",
			"name": "Area-TE-1.0_r_N10_nlin2Stdcolin27_6.2_publicP_ffa7638b2ef23a1e7694c20b348e2b05.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.2/Area-TE-1.0_r_N10_nlin2Stdcolin27_6.2_publicP_ffa7638b2ef23a1e7694c20b348e2b05.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "ffa7638b2ef23a1e7694c20b348e2b05"
		},
		{
			"byteSize": 88307.0,
			"hashcode": "e5e9841ef548d89106b70ab95bafab1a",
			"name": "Area-TE-1.0_l_N10_nlin2Stdcolin27_6.2_publicP_10def007b81c5c27fb67e4664e1567a6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.2/Area-TE-1.0_l_N10_nlin2Stdcolin27_6.2_publicP_10def007b81c5c27fb67e4664e1567a6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "10def007b81c5c27fb67e4664e1567a6"
		},
		{
			"byteSize": 67915.0,
			"hashcode": "1ade4a9ff51efd604dc72babe1bded82",
			"name": "Area-TE-1.0_l_N10_nlin2ICBM152asym2009c_6.2_publicP_10def007b81c5c27fb67e4664e1567a6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.2/Area-TE-1.0_l_N10_nlin2ICBM152asym2009c_6.2_publicP_10def007b81c5c27fb67e4664e1567a6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "722ac5374c3439f51720d2207a295037"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "e8d55b795dcbcdd0176f4637b936bb99",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 21.0,
			"hashcode": "ab4079896e6a1bf2bf3317b844e35045",
			"name": "subjects_Area-TE-1.0.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.2/subjects_Area-TE-1.0.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		},
		{
			"byteSize": 62482.0,
			"hashcode": "0119030cc3a241adeded5469a681880f",
			"name": "Area-TE-1.0_r_N10_nlin2ICBM152asym2009c_6.2_publicP_ffa7638b2ef23a1e7694c20b348e2b05.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-1.0_pub/6.2/Area-TE-1.0_r_N10_nlin2ICBM152asym2009c_6.2_publicP_ffa7638b2ef23a1e7694c20b348e2b05.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "dbae2362441af947093f86ef8b910e3a"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/538d3ea7-7af8-4644-9134-62f8334cf7c0",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "49e0e76dfff178efd76fd0325be98cd9",
			"name": "Freund, H.-J.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f1fc023f-5f78-4ce3-89a1-04de6b767a2e",
			"shortName": "Freund, H."
		},
		{
			"schema.org/shortName": "Werner, C.",
			"identifier": "c1ffe49cbf938cecbeee703a6602cb2c",
			"name": "Werner, C.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/687b9def-7ed0-48fe-bb24-f779bc9a94bb",
			"shortName": "Werner, C."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3a2dcba69606495850f0112bf23c4b03",
			"name": "Rademacher, J\u00f6rg",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/aa01de8e-a6de-4c9c-af62-8746a3e29ee1",
			"shortName": "Rademacher, J."
		},
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		}
	],
	"id": "538d3ea7-7af8-4644-9134-62f8334cf7c0",
	"kgReference": [
		"10.25493/RMZ5-16J"
	],
	"publications": [
		{
			"name": "Probabilistic Mapping and Volume Measurement of Human Primary Auditory Cortex",
			"cite": "Rademacher, J., Morosan, P., Schormann, T., Schleicher, A., Werner, C., Freund, H.-J., & Zilles, K. (2001). Probabilistic Mapping and Volume Measurement of Human Primary Auditory Cortex. NeuroImage, 13(4), 669\u2013683. ",
			"doi": "10.1006/nimg.2000.0714"
		},
		{
			"name": "Human Primary Auditory Cortex: Cytoarchitectonic Subdivisions and Mapping into a Spatial Reference System",
			"cite": "Morosan, P., Rademacher, J., Schleicher, A., Amunts, K., Schormann, T., & Zilles, K. (2001). Human Primary Auditory Cortex: Cytoarchitectonic Subdivisions and Mapping into a Spatial Reference System. NeuroImage, 13(4), 684\u2013701. ",
			"doi": "10.1006/nimg.2000.0715"
		}
	]
}
