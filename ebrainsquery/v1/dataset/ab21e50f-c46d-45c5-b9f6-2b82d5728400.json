{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Wojtasik, M., Bludau, S., Eickhoff, S. B., Mohlberg, H., Gerboga, F., Caspers, S., Amunts, K. (2020) Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex. Frontiers in Neuroanatomy,14(2)",
			"doi": "10.3389/fnana.2020.00002"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Fo6 (OFC) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces where each voxel is assigned with the probability to belong to Area Fo6 (OFC). The probability map of Area Fo6 (OFC) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\n\nThe most probable delineation of Area Fo6 (OFC) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493/8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Fo6 (OFC)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/330ae178-557c-4bd0-a932-f138c0a05345"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Fo6 (OFC) (v2.1)",
	"files": [
		{
			"byteSize": 105695.0,
			"hashcode": "8d3b513eb9857877c066c5423375fdde",
			"name": "Area-Fo6_l_N10_nlin2Stdcolin27_2.1_publicDOI_e0280f196fcc134583b19342a2b4b407.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/2.1/Area-Fo6_l_N10_nlin2Stdcolin27_2.1_publicDOI_e0280f196fcc134583b19342a2b4b407.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "e0280f196fcc134583b19342a2b4b407"
		},
		{
			"byteSize": 25.0,
			"hashcode": "1a131731a69708c58cf9d492a955937d",
			"name": "subjects_Area-Fo6.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/2.1/subjects_Area-Fo6.csv",
			"contentType": "text/csv",
			"hash": "658c268110d8c8b4ee55196b76839999"
		},
		{
			"byteSize": 69503.0,
			"hashcode": "09a24142be4eebe359cd887f8fddf0dd",
			"name": "Area-Fo6_r_N10_nlin2MNI152ASYM2009C_2.1_publicDOI_af30bdb7baf3287cd9f417c9e70992e2.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/2.1/Area-Fo6_r_N10_nlin2MNI152ASYM2009C_2.1_publicDOI_af30bdb7baf3287cd9f417c9e70992e2.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "af30bdb7baf3287cd9f417c9e70992e2"
		},
		{
			"byteSize": 72857.0,
			"hashcode": "fb3c61eaad4b5d4cf9ed622404b0737c",
			"name": "Area-Fo6_l_N10_nlin2MNI152ASYM2009C_2.1_publicDOI_374b5da5692230b1dde61b8f24e5a869.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/2.1/Area-Fo6_l_N10_nlin2MNI152ASYM2009C_2.1_publicDOI_374b5da5692230b1dde61b8f24e5a869.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "374b5da5692230b1dde61b8f24e5a869"
		},
		{
			"byteSize": 102703.0,
			"hashcode": "a28262b01d85eb6ff73bccc75150addd",
			"name": "Area-Fo6_r_N10_nlin2Stdcolin27_2.1_publicDOI_db7ea44b1acf9a9a11c471874ae702cf.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/2.1/Area-Fo6_r_N10_nlin2Stdcolin27_2.1_publicDOI_db7ea44b1acf9a9a11c471874ae702cf.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "db7ea44b1acf9a9a11c471874ae702cf"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/ab21e50f-c46d-45c5-b9f6-2b82d5728400",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Wojtasik, M.",
			"identifier": "ca2960dc-6ef0-415b-bf89-b047283e7257",
			"name": "Wojtasik, Magdalena",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ca2960dc-6ef0-415b-bf89-b047283e7257",
			"shortName": "Wojtasik, M."
		}
	],
	"id": "ab21e50f-c46d-45c5-b9f6-2b82d5728400",
	"kgReference": [
		"10.25493/34Q4-H62"
	],
	"publications": [
		{
			"name": "Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex",
			"cite": "Wojtasik, M., Bludau, S., Eickhoff, S. B., Mohlberg, H., Gerboga, F., Caspers, S., Amunts, K. (2020) Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex. Frontiers in Neuroanatomy,14(2)",
			"doi": "10.3389/fnana.2020.00002"
		}
	]
}
