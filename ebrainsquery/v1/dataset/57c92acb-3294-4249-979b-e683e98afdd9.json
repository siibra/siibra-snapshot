{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Lorenz, S., Weiner, K. S., Caspers, J., Mohlberg, H., Schleicher, A., Bludau, S., \u2026 Amunts, K. (2015). Two New Cytoarchitectonic Areas on the Human Mid-Fusiform Gyrus. Cerebral Cortex, bhv225. ",
			"doi": "10.1093/cercor/bhv225"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area FG4 (FusG) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area FG4 (FusG). The probability map of Area FG4 (FusG) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.\n\nOther available data versions of Area FG4 (FusG):\nLorenz et al. (2019) [Data set, v6.1] [DOI: 10.25493/13RG-FYV](https://doi.org/10.25493%2F13RG-FYV)\n\nThe most probable delineation of Area FG4 (FusG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area FG4 (FusG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/fa602743-5f6e-49d1-9734-29dffaa95ff5"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area FG4 (FusG) (v6.0)",
	"files": [
		{
			"byteSize": 125969.0,
			"hashcode": "d6125a0adcc6241c07615abec59504d4",
			"name": "Area-FG4_l_N10_nlin2ICBM152ASYM2009C_6.0_publicP_bd73b9ec9e5ca3e5114e1038396085bf.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG4_pub/6.0/Area-FG4_l_N10_nlin2ICBM152ASYM2009C_6.0_publicP_bd73b9ec9e5ca3e5114e1038396085bf.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "bd73b9ec9e5ca3e5114e1038396085bf"
		},
		{
			"byteSize": 21.0,
			"hashcode": "f2047a30829ed39c738885a007941e8f",
			"name": "subjects_Area-FG4.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG4_pub/6.0/subjects_Area-FG4.csv",
			"contentType": "text/csv",
			"hash": "904a772642783578c6bce7f59daeb0f0"
		},
		{
			"byteSize": 129276.0,
			"hashcode": "629f6a2a0bbc3d7ec35278b856cd614c",
			"name": "Area-FG4_r_N10_nlin2Stdcolin27_6.0_publicP_14c122df94a8d74a651aca86d845c8af.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG4_pub/6.0/Area-FG4_r_N10_nlin2Stdcolin27_6.0_publicP_14c122df94a8d74a651aca86d845c8af.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "14c122df94a8d74a651aca86d845c8af"
		},
		{
			"byteSize": 142942.0,
			"hashcode": "6466717cac1c181982418bda87890fd8",
			"name": "Area-FG4_l_N10_nlin2Stdcolin27_6.0_publicP_d510862af9075187d48ee6865dd65cf5.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG4_pub/6.0/Area-FG4_l_N10_nlin2Stdcolin27_6.0_publicP_d510862af9075187d48ee6865dd65cf5.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "d510862af9075187d48ee6865dd65cf5"
		},
		{
			"byteSize": 107960.0,
			"hashcode": "c98201d26ea1b763e9a37c6c93af006f",
			"name": "Area-FG4_r_N10_nlin2ICBM152ASYM2009C_6.0_publicP_faa00565eb68d9835363d046c7466d12.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG4_pub/6.0/Area-FG4_r_N10_nlin2ICBM152ASYM2009C_6.0_publicP_faa00565eb68d9835363d046c7466d12.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "faa00565eb68d9835363d046c7466d12"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/57c92acb-3294-4249-979b-e683e98afdd9",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "b8c53763dc95a6b4af3ab4057690fac9",
			"name": "Grill-Spector, Kalanit",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f45aae93-f262-42d3-b6a2-6fe922f6e00c",
			"shortName": "Grill-Spector, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "21ec0b4ae3f94c4d7e701862718f2ae2",
			"name": "Caspers, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c62b7a14-880d-45ec-8e82-967cd3626dd5",
			"shortName": "Caspers, J."
		},
		{
			"schema.org/shortName": null,
			"identifier": "dddfaa11fb9c7af6f9e2a54f9e3ef163",
			"name": "Weiner, Kevin S.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/041d9e23-1327-40e4-8283-82138eb1db13",
			"shortName": "Weiner, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c149471847daff78577093fa451d9a94",
			"name": "Lorenz, Simon",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/e6e71927-86e4-487e-ad0d-d3d1db4dce57",
			"shortName": "Lorenz, S."
		}
	],
	"id": "c89cb70ede397da7c18dc56793a6d1eb",
	"kgReference": [
		"10.25493/6A73-MZS"
	],
	"publications": [
		{
			"name": "Two New Cytoarchitectonic Areas on the Human Mid-Fusiform Gyrus",
			"cite": "Lorenz, S., Weiner, K. S., Caspers, J., Mohlberg, H., Schleicher, A., Bludau, S., \u2026 Amunts, K. (2015). Two New Cytoarchitectonic Areas on the Human Mid-Fusiform Gyrus. Cerebral Cortex, bhv225. ",
			"doi": "10.1093/cercor/bhv225"
		}
	]
}
