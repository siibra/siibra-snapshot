{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Id4 (Insula) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces where each voxel is assigned with the probability to belong to Area Id4 (Insula). The probability map of Area Id4 (Insula) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\n\nThe most probable delineation of Area Id4 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493/8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Id4 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/f480ed72-5ca5-4d1f-8905-cbe9bedcfaee"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Id4 (Insula) (v3.1)",
	"files": [
		{
			"byteSize": 25.0,
			"hashcode": "c2c4b50778dae65aa732f04eabe470cc",
			"name": "subjects_Area-Id4.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id4_pub/3.1/subjects_Area-Id4.csv",
			"contentType": "text/csv",
			"hash": "a0e96551db320ca8fbbeac38dc5d27ae"
		},
		{
			"byteSize": 56734.0,
			"hashcode": "df1899aa52830b339ee65a5b204f70ef",
			"name": "Area-Id4_r_N10_nlin2MNI152ASYM2009C_3.1_publicDOI_5389bc4c23b416171b9f8defacac1a0a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id4_pub/3.1/Area-Id4_r_N10_nlin2MNI152ASYM2009C_3.1_publicDOI_5389bc4c23b416171b9f8defacac1a0a.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "5389bc4c23b416171b9f8defacac1a0a"
		},
		{
			"byteSize": 78801.0,
			"hashcode": "ba3e613ccfc20a616ae79f8c7689f6dd",
			"name": "Area-Id4_l_N10_nlin2Stdcolin27_3.1_publicDOI_f18ef7c60cb32c3641014f0909f93e02.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id4_pub/3.1/Area-Id4_l_N10_nlin2Stdcolin27_3.1_publicDOI_f18ef7c60cb32c3641014f0909f93e02.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "f18ef7c60cb32c3641014f0909f93e02"
		},
		{
			"byteSize": 51487.0,
			"hashcode": "fe5e5c416ff482e20004b97b9eedc6a8",
			"name": "Area-Id4_l_N10_nlin2MNI152ASYM2009C_3.1_publicDOI_e96ae430032349aac6a4a61aaee79b4d.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id4_pub/3.1/Area-Id4_l_N10_nlin2MNI152ASYM2009C_3.1_publicDOI_e96ae430032349aac6a4a61aaee79b4d.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "e96ae430032349aac6a4a61aaee79b4d"
		},
		{
			"byteSize": 82635.0,
			"hashcode": "1f62286d8add9693c6a0cf1ebf116ecb",
			"name": "Area-Id4_r_N10_nlin2Stdcolin27_3.1_publicDOI_2a6668950e5a00f4a74bf38acca581aa.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id4_pub/3.1/Area-Id4_r_N10_nlin2Stdcolin27_3.1_publicDOI_2a6668950e5a00f4a74bf38acca581aa.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "2a6668950e5a00f4a74bf38acca581aa"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/49a74d3a-9290-4ff9-9a5a-522c0a096d1b",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"name": "Quabs, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"shortName": "Quabs, J."
		}
	],
	"id": "49a74d3a-9290-4ff9-9a5a-522c0a096d1b",
	"kgReference": [
		"10.25493/K63G-89H"
	],
	"publications": []
}
