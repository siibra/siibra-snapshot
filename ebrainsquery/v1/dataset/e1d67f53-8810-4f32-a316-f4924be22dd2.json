{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Caspers, S., Eickhoff, S. B., Geyer, S., Scheperjans, F., Mohlberg, H., Zilles, K., & Amunts, K. (2008). The human inferior parietal lobule in stereotaxic space. Brain Structure and Function, 212(6), 481\u2013495. ",
			"doi": "10.1007/s00429-008-0195-z"
		},
		{
			"cite": "Caspers, S., Geyer, S., Schleicher, A., Mohlberg, H., Amunts, K., & Zilles, K. (2006). The human inferior parietal cortex: Cytoarchitectonic parcellation and interindividual variability. NeuroImage, 33(2), 430\u2013448. ",
			"doi": "10.1016/j.neuroimage.2006.06.054"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area PFt (IPL) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area PFt (IPL). The probability map of Area PFt (IPL) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area PFt (IPL):\nCaspers et al. (2018) [Data set, v9.2] [DOI: 10.25493/2PNK-M62](https://doi.org/10.25493%2F2PNK-M62)\nCaspers et al. (2019) [Data set, v9.4] [DOI: 10.25493/JGM9-ZET](https://doi.org/10.25493%2FJGM9-ZET)\n\nThe most probable delineation of Area PFt (IPL) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area PFt (IPL)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/847cef50-7340-470d-8580-327b4ce9db19"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area PFt (IPL) (v11.0)",
	"files": [
		{
			"byteSize": 122314.0,
			"hashcode": "56da366a57618b73ea2609100f8c733e",
			"name": "Area-PFt_l_N10_nlin2Stdcolin27_11.0_publicP_1eb7cded7b7906e660d2fe01f2686eb7.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-PFt_pub/11.0/Area-PFt_l_N10_nlin2Stdcolin27_11.0_publicP_1eb7cded7b7906e660d2fe01f2686eb7.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "1eb7cded7b7906e660d2fe01f2686eb7"
		},
		{
			"byteSize": 104577.0,
			"hashcode": "58266b078edafd66254e6844579f0465",
			"name": "Area-PFt_l_N10_nlin2ICBM152asym2009c_11.0_publicP_41ce8d74cb45b8c0186a69b0afef4d30.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-PFt_pub/11.0/Area-PFt_l_N10_nlin2ICBM152asym2009c_11.0_publicP_41ce8d74cb45b8c0186a69b0afef4d30.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "41ce8d74cb45b8c0186a69b0afef4d30"
		},
		{
			"byteSize": 125813.0,
			"hashcode": "38ac3378fff542dff0e16d3d6f9fb4a2",
			"name": "Area-PFt_r_N10_nlin2Stdcolin27_11.0_publicP_a08ad7ee9c0c68f7070beede0dae8188.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-PFt_pub/11.0/Area-PFt_r_N10_nlin2Stdcolin27_11.0_publicP_a08ad7ee9c0c68f7070beede0dae8188.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "a08ad7ee9c0c68f7070beede0dae8188"
		},
		{
			"byteSize": 24.0,
			"hashcode": "fb76fd926f10646e77b022dff590b4d5",
			"name": "subjects_Area-PFt.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-PFt_pub/11.0/subjects_Area-PFt.csv",
			"contentType": "text/csv",
			"hash": "2efa0c79b58e324c00f4cacc7fe5dc0f"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "31171b3951b85598eeece776250aeb83",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-PFt_pub/11.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 106916.0,
			"hashcode": "a5f2c76149ca551775ea6b99ead547fe",
			"name": "Area-PFt_r_N10_nlin2ICBM152asym2009c_11.0_publicP_7a6484d6a9728d9bc930cc4ff5659e8b.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-PFt_pub/11.0/Area-PFt_r_N10_nlin2ICBM152asym2009c_11.0_publicP_7a6484d6a9728d9bc930cc4ff5659e8b.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "7a6484d6a9728d9bc930cc4ff5659e8b"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/e1d67f53-8810-4f32-a316-f4924be22dd2",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7be384119cf6b1bfbff17cdda3476f2e",
			"name": "Scheperjans, Filip",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/eac78bec-6b4c-4652-bea0-0e927cd558b2",
			"shortName": "Scheperjans, F."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "183ac28a5978832f25623307522cba98",
			"name": "Geyer, Stefan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a38f341-5b88-4cb2-b8b7-6473a676d892",
			"shortName": "Geyer, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "32d434732ac4fd11981ff0d898c6d6ab",
			"name": "Caspers, S.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/53042082-4177-4256-84a5-0d4a693c48c9",
			"shortName": "Caspers, S."
		}
	],
	"id": "e1d67f53-8810-4f32-a316-f4924be22dd2",
	"kgReference": [
		"10.25493/4N6F-VCG"
	],
	"publications": [
		{
			"name": "The human inferior parietal lobule in stereotaxic space",
			"cite": "Caspers, S., Eickhoff, S. B., Geyer, S., Scheperjans, F., Mohlberg, H., Zilles, K., & Amunts, K. (2008). The human inferior parietal lobule in stereotaxic space. Brain Structure and Function, 212(6), 481\u2013495. ",
			"doi": "10.1007/s00429-008-0195-z"
		},
		{
			"name": "The human inferior parietal cortex: Cytoarchitectonic parcellation and interindividual variability",
			"cite": "Caspers, S., Geyer, S., Schleicher, A., Mohlberg, H., Amunts, K., & Zilles, K. (2006). The human inferior parietal cortex: Cytoarchitectonic parcellation and interindividual variability. NeuroImage, 33(2), 430\u2013448. ",
			"doi": "10.1016/j.neuroimage.2006.06.054"
		}
	]
}
