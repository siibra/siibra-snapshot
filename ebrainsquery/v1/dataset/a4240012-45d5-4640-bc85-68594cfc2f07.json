{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Eickhoff, S. B., Amunts, K., Mohlberg, H., & Zilles, K. (2005). The Human Parietal Operculum. II. Stereotaxic Maps and Correlation with Functional Imaging Results. Cerebral Cortex, 16(2), 268\u2013279. ",
			"doi": "10.1093/cercor/bhi106"
		},
		{
			"cite": "Eickhoff, S. B., Schleicher, A., Zilles, K., & Amunts, K. (2005). The Human Parietal Operculum. I. Cytoarchitectonic Mapping of Subdivisions. Cerebral Cortex, 16(2), 254\u2013267. ",
			"doi": "10.1093/cercor/bhi105"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area OP2 (POperc) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area OP2 (POperc). The probability map of Area OP2 (POperc) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area OP2 (POperc): \nEickhoff et al. (2018) [Data set, v9.2] [DOI: 10.25493/F8W5-HNB](https://doi.org/10.25493%2FF8W5-HNB)\nEickhoff et al. (2019) [Data set, v9.4] [DOI: 10.25493/5KBV-36J](https://doi.org/10.25493%2F5KBV-36J)\n\nThe most probable delineation of Area OP2 (POperc) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area OP2 (POperc)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/ab26cefd-f7d6-4442-8020-a6e418e673ff"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area OP2 (POperc) (v11.0)",
	"files": [
		{
			"byteSize": 24.0,
			"hashcode": "8bd1e9cafe65bcf718f3da873960c4ca",
			"name": "subjects_Area-OP2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP2_pub/11.0/subjects_Area-OP2.csv",
			"contentType": "text/csv",
			"hash": "2efa0c79b58e324c00f4cacc7fe5dc0f"
		},
		{
			"byteSize": 51497.0,
			"hashcode": "3a4d383a21d76c78b073932693a25b4d",
			"name": "Area-OP2_l_N10_nlin2MNI152ASYM2009C_11.0_publicP_fddb12a6183c72c638e38437d2f9a930.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP2_pub/11.0/Area-OP2_l_N10_nlin2MNI152ASYM2009C_11.0_publicP_fddb12a6183c72c638e38437d2f9a930.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "fddb12a6183c72c638e38437d2f9a930"
		},
		{
			"byteSize": 81190.0,
			"hashcode": "4529c3e24abced6320e82d9dcf200b92",
			"name": "Area-OP2_r_N10_nlin2Stdcolin27_11.0_publicP_1eae6be77173d46c0ae7bd32e27bac94.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP2_pub/11.0/Area-OP2_r_N10_nlin2Stdcolin27_11.0_publicP_1eae6be77173d46c0ae7bd32e27bac94.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "1eae6be77173d46c0ae7bd32e27bac94"
		},
		{
			"byteSize": 55527.0,
			"hashcode": "6eba09770bcac56c9cc7e5ed43b6b1bf",
			"name": "Area-OP2_r_N10_nlin2MNI152ASYM2009C_11.0_publicP_f17ac7cb64cdcf4b11f1cee835c6eb88.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP2_pub/11.0/Area-OP2_r_N10_nlin2MNI152ASYM2009C_11.0_publicP_f17ac7cb64cdcf4b11f1cee835c6eb88.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "f17ac7cb64cdcf4b11f1cee835c6eb88"
		},
		{
			"byteSize": 77211.0,
			"hashcode": "8308b3b0978238e6260a7a0e1d795a48",
			"name": "Area-OP2_l_N10_nlin2Stdcolin27_11.0_publicP_4442cc5c4e018d8b9505f311e003b1d3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP2_pub/11.0/Area-OP2_l_N10_nlin2Stdcolin27_11.0_publicP_4442cc5c4e018d8b9505f311e003b1d3.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4442cc5c4e018d8b9505f311e003b1d3"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/a4240012-45d5-4640-bc85-68594cfc2f07",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		}
	],
	"id": "a4240012-45d5-4640-bc85-68594cfc2f07",
	"kgReference": [
		"10.25493/SDW0-YEZ"
	],
	"publications": [
		{
			"name": "The Human Parietal Operculum. II. Stereotaxic Maps and Correlation with Functional Imaging Results",
			"cite": "Eickhoff, S. B., Amunts, K., Mohlberg, H., & Zilles, K. (2005). The Human Parietal Operculum. II. Stereotaxic Maps and Correlation with Functional Imaging Results. Cerebral Cortex, 16(2), 268\u2013279. ",
			"doi": "10.1093/cercor/bhi106"
		},
		{
			"name": "The Human Parietal Operculum. I. Cytoarchitectonic Mapping of Subdivisions",
			"cite": "Eickhoff, S. B., Schleicher, A., Zilles, K., & Amunts, K. (2005). The Human Parietal Operculum. I. Cytoarchitectonic Mapping of Subdivisions. Cerebral Cortex, 16(2), 254\u2013267. ",
			"doi": "10.1093/cercor/bhi105"
		}
	]
}
