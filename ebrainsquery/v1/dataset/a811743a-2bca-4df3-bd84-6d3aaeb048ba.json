{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Geyer, S., Schormann, T., Mohlberg, H., & Zilles, K. (2000). Areas 3a, 3b, and 1 of Human Primary Somatosensory Cortex. NeuroImage, 11(6), 684\u2013696. ",
			"doi": "10.1006/nimg.2000.0548"
		},
		{
			"cite": "Geyer, S., Schleicher, A., & Zilles, K. (1999). Areas 3a, 3b, and 1 of Human Primary Somatosensory Cortex. NeuroImage, 10(1), 63\u201383. ",
			"doi": "10.1006/nimg.1999.0440"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area 3b (PostCG) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area 3b (PostCG). The probability map of Area 3b (PostCG) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.\n\nOther available data versions of Area 3b (PostCG):\nGeyer et al. (2019) [Data set, v8.4] [DOI: 10.25493/2JK3-QXR](https://doi.org/10.25493%2F2JK3-QXR)\n\nThe most probable delineation of Area 3b (PostCG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 3b (PostCG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/b84f67bb-5d9f-4daf-a8d6-15f63f901bd4"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area 3b (PostCG) (v8.2)",
	"files": [
		{
			"byteSize": 156121.0,
			"hashcode": "5b31580ce68656b3afde773f319d6af0",
			"name": "Area-3b_l_N10_nlin2ICBM152ASYM2009C_8.2_publicP_4821f2ffbdb4335546c6e9383b922336.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-3b_pub/8.2/Area-3b_l_N10_nlin2ICBM152ASYM2009C_8.2_publicP_4821f2ffbdb4335546c6e9383b922336.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "4821f2ffbdb4335546c6e9383b922336"
		},
		{
			"byteSize": 181543.0,
			"hashcode": "de1bfd6bb3041f466d133f92e1c06027",
			"name": "Area-3b_r_N10_nlin2ICBM152ASYM2009C_8.2_publicP_c9dbd04f9971b12cafb985c6acb5ce03.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-3b_pub/8.2/Area-3b_r_N10_nlin2ICBM152ASYM2009C_8.2_publicP_c9dbd04f9971b12cafb985c6acb5ce03.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "c9dbd04f9971b12cafb985c6acb5ce03"
		},
		{
			"byteSize": 21.0,
			"hashcode": "26ad5325782cd3a71a26a2d13ffff92a",
			"name": "subjects_Area-3b.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-3b_pub/8.2/subjects_Area-3b.csv",
			"contentType": "text/csv",
			"hash": "895362c2073b778e508605738bd96a63"
		},
		{
			"byteSize": 159584.0,
			"hashcode": "40282d092ec80efabfe7b7b8fa8b66e9",
			"name": "Area-3b_l_N10_nlin2Stdcolin27_8.2_publicP_44308653658945ad86f5e4a3112867a0.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-3b_pub/8.2/Area-3b_l_N10_nlin2Stdcolin27_8.2_publicP_44308653658945ad86f5e4a3112867a0.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "44308653658945ad86f5e4a3112867a0"
		},
		{
			"byteSize": 180661.0,
			"hashcode": "9bd37c049fd4e825cf2c40cdb2805495",
			"name": "Area-3b_r_N10_nlin2Stdcolin27_8.2_publicP_4ed36ef0d67297fee28a607eb802bf12.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-3b_pub/8.2/Area-3b_r_N10_nlin2Stdcolin27_8.2_publicP_4ed36ef0d67297fee28a607eb802bf12.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "4ed36ef0d67297fee28a607eb802bf12"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/a811743a-2bca-4df3-bd84-6d3aaeb048ba",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "183ac28a5978832f25623307522cba98",
			"name": "Geyer, Stefan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a38f341-5b88-4cb2-b8b7-6473a676d892",
			"shortName": "Geyer, S."
		}
	],
	"id": "db08224bd75c21ece58b145871b5be40",
	"kgReference": [
		"10.25493/W4WS-B3P"
	],
	"publications": [
		{
			"name": "Areas 3a, 3b, and 1 of Human Primary Somatosensory Cortex",
			"cite": "Geyer, S., Schormann, T., Mohlberg, H., & Zilles, K. (2000). Areas 3a, 3b, and 1 of Human Primary Somatosensory Cortex. NeuroImage, 11(6), 684\u2013696. ",
			"doi": "10.1006/nimg.2000.0548"
		},
		{
			"name": "Areas 3a, 3b, and 1 of Human Primary Somatosensory Cortex",
			"cite": "Geyer, S., Schleicher, A., & Zilles, K. (1999). Areas 3a, 3b, and 1 of Human Primary Somatosensory Cortex. NeuroImage, 10(1), 63\u201383. ",
			"doi": "10.1006/nimg.1999.0440"
		}
	]
}
