{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Lorenz, S., Weiner, K. S., Caspers, J., Mohlberg, H., Schleicher, A., Bludau, S., \u2026 Amunts, K. (2015). Two New Cytoarchitectonic Areas on the Human Mid-Fusiform Gyrus. Cerebral Cortex, bhv225. ",
			"doi": "10.1093/cercor/bhv225"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area FG3 (FusG) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area FG3 (FusG). The probability map of Area FG3 (FusG) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area FG3 (FusG):\nLorenz et al. (2019) [Data set, v6.1] [DOI: 10.25493/Z0F6-0SY](https://doi.org/10.25493%2FZ0F6-0SY)\n\nThe most probable delineation of Area FG3 (FusG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area FG3 (FusG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/023f8ef7-c266-4c45-8bf2-4a17dc52985b"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area FG3 (FusG) (v6.0)",
	"files": [
		{
			"byteSize": 107638.0,
			"hashcode": "598dd90df1ff361a46c31eb53b826bed",
			"name": "Area-FG3_l_N10_nlin2ICBM152ASYM2009C_6.0_publicP_0ef7f75202fed1c07fd76484080f4820.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG3_pub/6.0/Area-FG3_l_N10_nlin2ICBM152ASYM2009C_6.0_publicP_0ef7f75202fed1c07fd76484080f4820.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "0ef7f75202fed1c07fd76484080f4820"
		},
		{
			"byteSize": 125156.0,
			"hashcode": "01712a0ea9a90aa3f5b53fecdb5e93ae",
			"name": "Area-FG3_l_N10_nlin2Stdcolin27_6.0_publicP_002998e1c5e8591e270341c00c014d75.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG3_pub/6.0/Area-FG3_l_N10_nlin2Stdcolin27_6.0_publicP_002998e1c5e8591e270341c00c014d75.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "002998e1c5e8591e270341c00c014d75"
		},
		{
			"byteSize": 21.0,
			"hashcode": "1694b8b18d3d1f23bb02c2d7f226d311",
			"name": "subjects_Area-FG3.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG3_pub/6.0/subjects_Area-FG3.csv",
			"contentType": "text/csv",
			"hash": "904a772642783578c6bce7f59daeb0f0"
		},
		{
			"byteSize": 114042.0,
			"hashcode": "75f02bd8b31cdae066dbde2152486cf6",
			"name": "Area-FG3_r_N10_nlin2Stdcolin27_6.0_publicP_ac3f7d7832dbabf084fa9ce40c69148e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG3_pub/6.0/Area-FG3_r_N10_nlin2Stdcolin27_6.0_publicP_ac3f7d7832dbabf084fa9ce40c69148e.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "ac3f7d7832dbabf084fa9ce40c69148e"
		},
		{
			"byteSize": 92918.0,
			"hashcode": "c7b0b1e20e154be7f8988ee028878ebb",
			"name": "Area-FG3_r_N10_nlin2ICBM152ASYM2009C_6.0_publicP_183c7c20de011c1412fea3c2c8dca234.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG3_pub/6.0/Area-FG3_r_N10_nlin2ICBM152ASYM2009C_6.0_publicP_183c7c20de011c1412fea3c2c8dca234.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "183c7c20de011c1412fea3c2c8dca234"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/cac40b93-b244-479e-8300-c286a1172ba3",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "b8c53763dc95a6b4af3ab4057690fac9",
			"name": "Grill-Spector, Kalanit",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f45aae93-f262-42d3-b6a2-6fe922f6e00c",
			"shortName": "Grill-Spector, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "21ec0b4ae3f94c4d7e701862718f2ae2",
			"name": "Caspers, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c62b7a14-880d-45ec-8e82-967cd3626dd5",
			"shortName": "Caspers, J."
		},
		{
			"schema.org/shortName": null,
			"identifier": "dddfaa11fb9c7af6f9e2a54f9e3ef163",
			"name": "Weiner, Kevin S.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/041d9e23-1327-40e4-8283-82138eb1db13",
			"shortName": "Weiner, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c149471847daff78577093fa451d9a94",
			"name": "Lorenz, Simon",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/e6e71927-86e4-487e-ad0d-d3d1db4dce57",
			"shortName": "Lorenz, S."
		}
	],
	"id": "67d75cb8a7ae67e9008fde9c6798bd9f",
	"kgReference": [
		"10.25493/E7VG-4NG"
	],
	"publications": [
		{
			"name": "Two New Cytoarchitectonic Areas on the Human Mid-Fusiform Gyrus",
			"cite": "Lorenz, S., Weiner, K. S., Caspers, J., Mohlberg, H., Schleicher, A., Bludau, S., \u2026 Amunts, K. (2015). Two New Cytoarchitectonic Areas on the Human Mid-Fusiform Gyrus. Cerebral Cortex, bhv225. ",
			"doi": "10.1093/cercor/bhv225"
		}
	]
}
