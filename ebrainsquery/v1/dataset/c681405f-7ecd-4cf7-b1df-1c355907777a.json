{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Grodzinsky, Y., Deschamps, I., Pieperhoff, P., Iannilli, F., Agmon, G., Loewenstein, Y., Amunts, K. (2020) Logical negation mapped onto the brain. Brain Structure and Function, 225(1):19-31",
			"doi": "10.1007/s00429-019-01975-w"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Id7 (Insula) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains, obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently, the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces, where each voxel is assigned with the probability to belong to Area Id7 (Insula). The probability map of Area Id7 (Insula) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets. \n\nOther available data versions of Area Id7 (Insula):\nAmunts et al. (2019) [Data set, v6.0] [DOI: 10.25493/B2E3-JQR](https://doi.org/10.25493%2FB2E3-JQR)\nAmunts et al. (2019) [Data set, v6.1] [DOI: 10.25493/88QG-JMS](https://doi.org/10.25493%2F88QG-JMS)\nAmunts et al. (2020) [Data set, v7.0] [DOI: 10.25493/AFP6-CQ](https://doi.org/10.25493%2FAFP6-CQ)\n\nThe most probable delineation of Area Id7 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Id7 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/3d5729f5-55c6-412a-8fc1-41a95c71b13a"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Id7 (Insula) (v8.1)",
	"files": [
		{
			"byteSize": 84901.0,
			"hashcode": "ea3808bfd99451349064238431d977d7",
			"name": "Area-Id7_r_N10_nlin2Stdcolin27_8.1_publicDOI_4c318e3366522748924ddbc7bf00d7fd.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/8.1/Area-Id7_r_N10_nlin2Stdcolin27_8.1_publicDOI_4c318e3366522748924ddbc7bf00d7fd.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4c318e3366522748924ddbc7bf00d7fd"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "a3b6fa8958f883404cf0cbae9db9923e",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/8.1/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 59316.0,
			"hashcode": "46ac2588c67c5eb462dafce983e42a5b",
			"name": "Area-Id7_r_N10_nlin2ICBM152asym2009c_8.1_publicDOI_4c318e3366522748924ddbc7bf00d7fd.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/8.1/Area-Id7_r_N10_nlin2ICBM152asym2009c_8.1_publicDOI_4c318e3366522748924ddbc7bf00d7fd.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "42aed7ece7b49d71b4836942c0472606"
		},
		{
			"byteSize": 24.0,
			"hashcode": "2fcbc1f01ba1e17b4b3dd30bcc4f2de6",
			"name": "subjects_Area-Id7.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/8.1/subjects_Area-Id7.csv",
			"contentType": "text/csv",
			"hash": "5f65aff2b50cea7b07f7c426a59762b3"
		},
		{
			"byteSize": 82852.0,
			"hashcode": "47fa059dc80a68cac20f3f92263ecded",
			"name": "Area-Id7_l_N10_nlin2Stdcolin27_8.1_publicDOI_74f6362723ecfe29be41b75ac0d98e23.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/8.1/Area-Id7_l_N10_nlin2Stdcolin27_8.1_publicDOI_74f6362723ecfe29be41b75ac0d98e23.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "74f6362723ecfe29be41b75ac0d98e23"
		},
		{
			"byteSize": 55864.0,
			"hashcode": "282f69cca5cc65d046251c66dc9bd139",
			"name": "Area-Id7_l_N10_nlin2ICBM152asym2009c_8.1_publicDOI_74f6362723ecfe29be41b75ac0d98e23.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Id7_pub/8.1/Area-Id7_l_N10_nlin2ICBM152asym2009c_8.1_publicDOI_74f6362723ecfe29be41b75ac0d98e23.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "cb0ab6c1b7b46ab68a6022a32ce74269"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/c681405f-7ecd-4cf7-b1df-1c355907777a",
	"contributors": [
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "ca58d45c-c0b4-46cd-912a-82f581f77ce6303708",
			"name": "Iannilli, Francesca",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ca58d45c-c0b4-46cd-912a-82f581f77ce6",
			"shortName": "Iannilli, F."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"id": "c681405f-7ecd-4cf7-b1df-1c355907777a",
	"kgReference": [
		"10.25493/CT6W-GBH"
	],
	"publications": [
		{
			"name": "Logical negation mapped onto the brain",
			"cite": "Grodzinsky, Y., Deschamps, I., Pieperhoff, P., Iannilli, F., Agmon, G., Loewenstein, Y., Amunts, K. (2020) Logical negation mapped onto the brain. Brain Structure and Function, 225(1):19-31",
			"doi": "10.1007/s00429-019-01975-w"
		}
	]
}
