{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Richter, M., Amunts, K., Mohlberg, H., Bludau, S., Eickhoff, S. B., Zilles, K., Caspers, S. (2018). Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions. Cerebral Cortex, 29(3), 1305-1327",
			"doi": "10.1093/cercor/bhy245"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area hIP6 (IPS) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area hIP6 (IPS). The probability map of Area hIP6 (IPS) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area hIP6 (IPS):\nRichter et al. (2019) [Data set, v7.0] [DOI: 10.25493/SVEY-ZBS](https://doi.org/10.25493%2FSVEY-ZBS)\n\nThe most probable delineation of Area hIP6 (IPS) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hIP6 (IPS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/b9975f8e-f484-4e82-883a-5fd765855ae0"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area hIP6 (IPS) (v7.1)",
	"files": [
		{
			"byteSize": 84304.0,
			"hashcode": "558208436447925fc047cdb30b3be4be",
			"name": "Area-hIP6_r_N10_nlin2ICBM152asym2009c_7.1_publicP_84d2e626c3a65071260ea37239c88f03.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP6_pub/7.1/Area-hIP6_r_N10_nlin2ICBM152asym2009c_7.1_publicP_84d2e626c3a65071260ea37239c88f03.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "84d2e626c3a65071260ea37239c88f03"
		},
		{
			"byteSize": 23.0,
			"hashcode": "3771dfde5d1126e632c097af7e425a62",
			"name": "subjects_Area-hIP6.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP6_pub/7.1/subjects_Area-hIP6.csv",
			"contentType": "text/csv",
			"hash": "662f05cadecb8a40e61bebf252676853"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "0b6f9aa8cacf5bb0bf71d7a3fbe662ff",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP6_pub/7.1/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 105068.0,
			"hashcode": "db78a186a44639917986430ae38dd563",
			"name": "Area-hIP6_r_N10_nlin2Stdcolin27_7.1_publicP_51c73cc3a8694a0a9f8534dd4abc5eb9.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP6_pub/7.1/Area-hIP6_r_N10_nlin2Stdcolin27_7.1_publicP_51c73cc3a8694a0a9f8534dd4abc5eb9.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "51c73cc3a8694a0a9f8534dd4abc5eb9"
		},
		{
			"byteSize": 94310.0,
			"hashcode": "65d8df41bd5304f142efa4ccf644980c",
			"name": "Area-hIP6_l_N10_nlin2ICBM152asym2009c_7.1_publicP_9e704c4153bb70ac75df5d3a70859c3e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP6_pub/7.1/Area-hIP6_l_N10_nlin2ICBM152asym2009c_7.1_publicP_9e704c4153bb70ac75df5d3a70859c3e.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "9e704c4153bb70ac75df5d3a70859c3e"
		},
		{
			"byteSize": 108568.0,
			"hashcode": "e50047c2f2dc77d18f3a29ad8ec6e3cd",
			"name": "Area-hIP6_l_N10_nlin2Stdcolin27_7.1_publicP_efee72bc83372484e2268a5da0bf4b38.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP6_pub/7.1/Area-hIP6_l_N10_nlin2Stdcolin27_7.1_publicP_efee72bc83372484e2268a5da0bf4b38.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "efee72bc83372484e2268a5da0bf4b38"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/e51da69b-f5de-49d5-9566-6fde3ef14d80",
	"contributors": [
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"name": "Richter, Monika",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"shortName": "Richter, M."
		}
	],
	"id": "e51da69b-f5de-49d5-9566-6fde3ef14d80",
	"kgReference": [
		"10.25493/AFQR-50Q"
	],
	"publications": [
		{
			"name": " Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions",
			"cite": "Richter, M., Amunts, K., Mohlberg, H., Bludau, S., Eickhoff, S. B., Zilles, K., Caspers, S. (2018). Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions. Cerebral Cortex, 29(3), 1305-1327",
			"doi": "10.1093/cercor/bhy245"
		}
	]
}
