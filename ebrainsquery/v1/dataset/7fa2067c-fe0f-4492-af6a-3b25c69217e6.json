{
	"formats": [],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"Pyramidal cell",
				"CA1",
				"hippocampus",
				"Amyloid Precursor Protein KM670/671NL Swedish mutation"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"electrophysiology recording",
		"Single electrode recording",
		"Voltage clamp recording"
	],
	"custodians": [
		{
			"schema.org/shortName": null,
			"identifier": "2e1f9eaefe1c854d811d0e63d07bcd2a",
			"name": "Cherubini, Enrico",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/feca5f91-2b53-4c90-85b5-8b75e775c434",
			"shortName": "Cherubini, E."
		}
	],
	"project": [
		"Action potential dependent sIPSCs - from 3-4 months old Tg2576 female mice"
	],
	"description": "A general procedure to fit individual synaptic events recorded from voltage clamp experiments was tested with various sets of experimental data on GABAergic synapses; gephyrin and gephyrin-dependent pathways were chosen as a suitable example of a kinetic model of synaptic transmission. The data used here are obtained from coronal hippocampal slices (300 \u03bcm thick) from 3 to 4 months old Tg2576 (carrying the Amyloid Precursor Protein KM670/671NL Swedish mutation) female mice. The dataset contains 1000 individual events recorded from 10 different neurons (expE1-E10). The first column of each txt file contains the time in ms and the other columns contain spontaneous inhibitory synaptic currents (sIPSCs) in pA.\nComputational modeling of brain circuits requires the definition of many parameters that are difficult to determine from experimental findings. One way to help interpret these data is to fit them using a particular kinetic model. In [1] we proposed a general procedure to fit individual synaptic events recorded from voltage clamp experiments. Starting from any given model description (mod file) in the NEURON simulation environment, the procedure exploits user-defined constraints, dependencies, and rules for the parameters of the model to fit the time course of individual spontaneous synaptic events that are recorded experimentally. A Python version is available for public use, as a Jupyter Notebook in the Collaboratory Portal of the Human Brain Project. To illustrate the potential application of the procedure, we tested its use with various sets of experimental data on GABAergic synapses; gephyrin and gephyrin-dependent pathways were chosen as a suitable example of a kinetic model of synaptic transmission. For individual spontaneous inhibitory events in hippocampal pyramidal CA1 neurons, we found that gephyrin-dependent subcellular pathways may shape synaptic events at different levels, and can be correlated with cell- or event-specific activity history and/or pathological conditions.\nFitting experimental data against a number of different models is a common way to do this (reviewed in [2]), and can help in the subsequent interpretation of the data. In general, experimental traces are fitted using specific approaches for specific purposes (e.g., [3], [4]). However, to the best of our knowledge, there is no easy, user-friendly, general procedure available for this purpose, especially in computational neuroscience. Our aim was thus to identify the appropriate conceptual structure of a procedure to obtain good, reliable fits of raw experimental traces of spontaneous synaptic events. This is an important step because spontaneous synaptic events have been so far exclusively analyzed using traces obtained by averaging many events. However, as can be easily imagined, each synapse in any given neuron has its own, independent, history of activation. The most likely physiological consequence is that the variables relative to the subcellular processes underlying synaptic transmission are different for each synapse. If a researcher is interested in testing a specific kinetic scheme implemented for specific biochemical pathways, the use of individual events is the most appropriate choice, since this approach would give information on the different combinations of model parameters that are consistent with the observed events. Averaging traces will lose a lot of relevant information.\nWe therefore present here the implementation of a procedure leading to the development of a unifying optimization method for individual synaptic events. Experimental data, kinetic models of synaptic transmission, and fitting parameters and their dependencies can be user defined/provided or gathered from databases. They can be used to generate optimized groups of parameters able to represent a population of synapses, either for simulation purposes or to study the functional consequences of a particular protein or subcellular synaptic transmission pathway.\n[1] Lupascu CA, Morabito A, Merenda E, et al. A General Procedure to Study Subcellular Models of Transsynaptic Signaling at Inhibitory Synapses. Frontiers in Neuroinformatics. 2016;10:23. doi:10.3389/fninf.2016.00023.  \n[2] Van Geit W., De Schutter E., Achard P. (2008). Automated neuron model optimization techniques: a review. Biol. Cybern. 99, 241\u2013251. 10.1007/s00422-008-0257-6  \n[3] Bekkers J. M. (2003). Convolution of mini distributions for fitting evoked synaptic amplitude histograms. J. Neurosci. Methods. 130, 105\u2013114. 10.1016/j.jneumeth.2003.09.018  \n[4] Meisl G., Kirkegaard J. B., Arosio P., Michaels T. C., Vendruscolo M., Dobson C. M., et al. . (2016). Molecular mechanisms of protein aggregation from global fitting of kinetic models. Nat. Protoc. 11, 252\u2013272. 10.1038/nprot.2016.010",
	"parcellationAtlas": [
		{
			"name": "Allen Mouse Common Coordinate Framework v3 2015",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/39a1384b-8413-4d27-af8d-22432225401f",
			"id": "39a1384b-8413-4d27-af8d-22432225401f"
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"relativeUrl": "minds/core/licensetype/v1.0.0/88e71f2d-4fcc-4cb1-bc9f-cbab9ab2058b"
		}
	],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Mouse Field CA1 (v3 2015)",
			"alias": "Field CA1",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/82170354-680f-4fde-b6a8-f3316497634c"
		}
	],
	"species": [
		"Mus musculus"
	],
	"name": "Spontaneous inhibitory post-synaptic currents recorded from CA1 pyramidal neurons of adult wild type Tg2576 mice",
	"files": [
		{
			"byteSize": 549.0,
			"hashcode": "131fa4c84c409ffc618fd624ea483dd6",
			"name": "hbp-00011_README.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/hbp-00011_README.txt",
			"contentType": "text/plain",
			"hash": "ecb9544363c1241731d65e91c7a8a8ce"
		},
		{
			"byteSize": 166.0,
			"hashcode": "ec555846628e7b5a12bee86f4d57db26",
			"name": "hbp-00011_Tg2576_Sub1_Samp2__expE2_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp2/hbp-00011_Tg2576_Sub1_Samp2__expE2_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 372021.0,
			"hashcode": "0c5730197c5c242e4e63a35f10cba149",
			"name": "hbp-00011_Tg2576_Sub2_Samp2__expE6.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp2/hbp-00011_Tg2576_Sub2_Samp2__expE6.txt",
			"contentType": "text/plain",
			"hash": "cf12ec8828117d0e49aedec14e724e4c"
		},
		{
			"byteSize": 166.0,
			"hashcode": "01b9ba2ab867b848eafc48457e948b38",
			"name": "hbp-00011_Tg2576_Sub1_Samp1__expE1_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp1/hbp-00011_Tg2576_Sub1_Samp1__expE1_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 166.0,
			"hashcode": "2fc3d97a1aea81240506b1177dda685e",
			"name": "hbp-00011_Tg2576_Sub3_Samp2__expE10_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub3/Samp2/hbp-00011_Tg2576_Sub3_Samp2__expE10_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 372447.0,
			"hashcode": "2a3a4fc0dbef018f07fcd0d7a2c3eef5",
			"name": "hbp-00011_Tg2576_Sub2_Samp1__expE5.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp1/hbp-00011_Tg2576_Sub2_Samp1__expE5.txt",
			"contentType": "text/plain",
			"hash": "ff7ce9cc3aadd0f8915817ab75d517fd"
		},
		{
			"byteSize": 325274.0,
			"hashcode": "8954fbb02da30a0a88fe7116822b0c21",
			"name": "hbp-00011_Tg2576_Sub1_Samp3__expE3.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp3/hbp-00011_Tg2576_Sub1_Samp3__expE3.txt",
			"contentType": "text/plain",
			"hash": "26c88d2ec3c6508a3b8360cb42d2118a"
		},
		{
			"byteSize": 166.0,
			"hashcode": "a11ab8a7306ed8ab8b2b4cf3b1616ca0",
			"name": "hbp-00011_Tg2576_Sub2_Samp3__expE7_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp3/hbp-00011_Tg2576_Sub2_Samp3__expE7_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 429069.0,
			"hashcode": "a048f748b097a6c133c328532a79af5b",
			"name": "hbp-00011_Tg2576_Sub1_Samp4__expE4.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp4/hbp-00011_Tg2576_Sub1_Samp4__expE4.txt",
			"contentType": "text/plain",
			"hash": "29219aa9653200815289e23f3af8efca"
		},
		{
			"byteSize": 166.0,
			"hashcode": "728d50a4751278904c55d26e3426ed12",
			"name": "hbp-00011_Tg2576_Sub3_Samp1__expE9_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub3/Samp1/hbp-00011_Tg2576_Sub3_Samp1__expE9_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 361603.0,
			"hashcode": "3efae76c75f7da1e75dbd14a1214248d",
			"name": "hbp-00011_Tg2576_Sub2_Samp4__expE8.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp4/hbp-00011_Tg2576_Sub2_Samp4__expE8.txt",
			"contentType": "text/plain",
			"hash": "37a6f0e839c25359ad77cca77b1de34b"
		},
		{
			"byteSize": 166.0,
			"hashcode": "f6ca3a4ab26c5e0ecb0013a921986136",
			"name": "hbp-00011_Tg2576_Sub2_Samp2__expE6_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp2/hbp-00011_Tg2576_Sub2_Samp2__expE6_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 11763.0,
			"hashcode": "0a97f4466e6f40aafb2bb9cc206fb93f",
			"name": "hbp-00011_DataDescriptor_v1p1.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/hbp-00011_DataDescriptor_v1p1.txt",
			"contentType": "text/plain",
			"hash": "b02b220b8974427c653106fd563c1819"
		},
		{
			"byteSize": 350217.0,
			"hashcode": "d57a519a25ff75b54989a4bc71991d4a",
			"name": "hbp-00011_Tg2576_Sub3_Samp1__expE9.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub3/Samp1/hbp-00011_Tg2576_Sub3_Samp1__expE9.txt",
			"contentType": "text/plain",
			"hash": "889471412964ec87ad5a7002dde199bc"
		},
		{
			"byteSize": 149042.0,
			"hashcode": "e2ccab7292ad6d6fe01d5dd98fa77622",
			"name": "hbp-00011_DataDescriptor_v1p1.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/hbp-00011_DataDescriptor_v1p1.pdf",
			"contentType": "application/pdf",
			"hash": "abc5e38a59de446491ac6c4723a59554"
		},
		{
			"byteSize": 414984.0,
			"hashcode": "7df1de4cefe5dca3999635da327c422c",
			"name": "hbp-00011_Tg2576_Sub1_Samp1__expE1.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp1/hbp-00011_Tg2576_Sub1_Samp1__expE1.txt",
			"contentType": "text/plain",
			"hash": "38441337865069eabae7754b29bb43e1"
		},
		{
			"byteSize": 166.0,
			"hashcode": "f7f1491f5b4c2818abb903f6d1aa1e9a",
			"name": "hbp-00011_Tg2576_Sub1_Samp4__expE4_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp4/hbp-00011_Tg2576_Sub1_Samp4__expE4_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 368573.0,
			"hashcode": "febd667c30a10f52ec94a1c2840e583d",
			"name": "hbp-00011_Tg2576_Sub3_Samp2__expE10.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub3/Samp2/hbp-00011_Tg2576_Sub3_Samp2__expE10.txt",
			"contentType": "text/plain",
			"hash": "f27e46979035706eb0aaf58c26e09585"
		},
		{
			"byteSize": 333308.0,
			"hashcode": "b5281b72956faf79186cf7f971cf62cf",
			"name": "hbp-00011_Tg2576_Sub1_Samp2__expE2.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp2/hbp-00011_Tg2576_Sub1_Samp2__expE2.txt",
			"contentType": "text/plain",
			"hash": "e1040d9546423c823944120de0e5c46c"
		},
		{
			"byteSize": 166.0,
			"hashcode": "f6fe8596607b99f2ea0ba5ceefc5f35a",
			"name": "hbp-00011_Tg2576_Sub1_Samp3__expE3_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub1/Samp3/hbp-00011_Tg2576_Sub1_Samp3__expE3_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 166.0,
			"hashcode": "287469e1601c2005066f567c591c2109",
			"name": "hbp-00011_Tg2576_Sub2_Samp4__expE8_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp4/hbp-00011_Tg2576_Sub2_Samp4__expE8_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 166.0,
			"hashcode": "e7888c4b1f84164c9e5c0c3608db90ac",
			"name": "hbp-00011_Tg2576_Sub2_Samp1__expE5_about.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp1/hbp-00011_Tg2576_Sub2_Samp1__expE5_about.json",
			"contentType": "application/json",
			"hash": "d36e36f3e87aaafeade1ed88da974d63"
		},
		{
			"byteSize": 352235.0,
			"hashcode": "954e9f1d8b806244f144d26cba3efdd8",
			"name": "hbp-00011_Tg2576_Sub2_Samp3__expE7.txt",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/Cherubini_RUP_T1.1.6_nar030/hbp-00011/Tg2576/Sub2/Samp3/hbp-00011_Tg2576_Sub2_Samp3__expE7.txt",
			"contentType": "text/plain",
			"hash": "6d4e1e99b522b025f7a176e9dc695815"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/7fa2067c-fe0f-4492-af6a-3b25c69217e6",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "c585f75ab24f3551098bf12a3b97861c",
			"name": "Marinelli, Silvia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8ccf3fff-90cf-4004-80b1-89a8d992aa14",
			"shortName": "Marinelli, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "2e1f9eaefe1c854d811d0e63d07bcd2a",
			"name": "Cherubini, Enrico",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/feca5f91-2b53-4c90-85b5-8b75e775c434",
			"shortName": "Cherubini, E."
		},
		{
			"schema.org/shortName": null,
			"identifier": "b9dc08ad567117caf30e6aeddbc6ca32",
			"name": "Marchetti, Cristina",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3d8fbcda-7599-406c-b257-4c7c9e9d12f8",
			"shortName": "Marchetti, C."
		}
	],
	"id": "57f6354117bbee6d961f8e36d6360c77",
	"kgReference": [
		"10.25493/CTAV-Q45"
	],
	"publications": []
}
