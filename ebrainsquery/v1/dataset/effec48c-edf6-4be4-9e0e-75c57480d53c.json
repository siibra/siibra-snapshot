{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Palomero-Gallagher, N., Kedo, O., Mohlberg, H., Zilles, K., Amunts, K. (2020) Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus. Brain Struct Funct., 225(3):881-907.",
			"doi": "10.1007/s00429-019-02022-4"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of CA1 (Hippocampus) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to CA1 (Hippocampus). The probability map of CA1 (Hippocampus) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of CA1 (Hippocampus):\nAmunts et al. (2018) [Data set, v11b.0] [DOI: 10.25493/W4WK-QSK](https://doi.org/10.25493%2FW4WK-QSK)\n\nThe most probable delineation of CA1 (Hippocampus) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "CA1 (Hippocampus)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/bfc0beb7-310c-4c57-b810-2adc464bd02c"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of CA1 (Hippocampus) (v11.1)",
	"files": [
		{
			"byteSize": 115608.0,
			"hashcode": "29c10675feb6744a56e130e7d44d8727",
			"name": "CA1_l_N10_nlin2Stdcolin27_11.1_publicP_6a386128df7bcc27ed7846d6e90343ef.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA1_pub/11.1/CA1_l_N10_nlin2Stdcolin27_11.1_publicP_6a386128df7bcc27ed7846d6e90343ef.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "6a386128df7bcc27ed7846d6e90343ef"
		},
		{
			"byteSize": 97741.0,
			"hashcode": "055b389637b4d39518cd40e427cdf1db",
			"name": "CA1_r_N10_nlin2MNI152ASYM2009C_11.1_publicP_23c95d20bd183c30c7c0b6d1b71634e0.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA1_pub/11.1/CA1_r_N10_nlin2MNI152ASYM2009C_11.1_publicP_23c95d20bd183c30c7c0b6d1b71634e0.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "23c95d20bd183c30c7c0b6d1b71634e0"
		},
		{
			"byteSize": 21.0,
			"hashcode": "90f6af84cb64b4f1fac252eaf2fe1779",
			"name": "subjects_CA1.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA1_pub/11.1/subjects_CA1.csv",
			"contentType": "text/csv",
			"hash": "b53cceeb6a14da4074456a7f89f87ab9"
		},
		{
			"byteSize": 117513.0,
			"hashcode": "088e29df831527b059e53c2c3a02f801",
			"name": "CA1_r_N10_nlin2Stdcolin27_11.1_publicP_50db79a14155739e1cae0400c2830204.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA1_pub/11.1/CA1_r_N10_nlin2Stdcolin27_11.1_publicP_50db79a14155739e1cae0400c2830204.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "50db79a14155739e1cae0400c2830204"
		},
		{
			"byteSize": 99484.0,
			"hashcode": "ea4c0986e72fc69cfdd09d5210261af6",
			"name": "CA1_l_N10_nlin2MNI152ASYM2009C_11.1_publicP_8608fd3351abb7c7e39e72827a46c776.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-CA1_pub/11.1/CA1_l_N10_nlin2MNI152ASYM2009C_11.1_publicP_8608fd3351abb7c7e39e72827a46c776.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "8608fd3351abb7c7e39e72827a46c776"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/effec48c-edf6-4be4-9e0e-75c57480d53c",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3e871e11c214a058a43cb1fc38788eea",
			"name": "Kedo, O.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9dabc7a3-fa77-481f-ab9e-8ef6ac6e7d42",
			"shortName": "Kedo, O."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "effec48c-edf6-4be4-9e0e-75c57480d53c",
	"kgReference": [
		"10.25493/4A6X-6F0"
	],
	"publications": [
		{
			"name": "Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus.",
			"cite": "Palomero-Gallagher, N., Kedo, O., Mohlberg, H., Zilles, K., Amunts, K. (2020) Multimodal mapping and analysis of the cyto- and receptorarchitecture of the human hippocampus. Brain Struct Funct., 225(3):881-907.",
			"doi": "10.1007/s00429-019-02022-4"
		}
	]
}
