{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Amunts, K., Kedo, O., Kindler, M., Pieperhoff, P., Mohlberg, H., Shah, N. J., \u2026 Zilles, K. (2005). Cytoarchitectonic mapping of the human amygdala, hippocampal region and entorhinal cortex: intersubject variability and probability maps. Anatomy and Embryology, 210(5-6), 343\u2013352. ",
			"doi": "10.1007/s00429-005-0025-5"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic DG (Hippocampus) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to DG (Hippocampus). The probability map of DG (Hippocampus) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of DG (Hippocampus):\nAmunts et al. (2018) [Data set, v11b.0] [DOI: 10.25493/1KV8-B9G](https://doi.org/10.25493%2F1KV8-B9G)\n\nThe most probable delineation of DG (Hippocampus) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "DG (Hippocampus)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/0bea7e03-bfb2-4907-9d45-db9071ce627d"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of DG (Hippocampus) (v11.1)",
	"files": [
		{
			"byteSize": 83242.0,
			"hashcode": "7e0a50e170853101ebcd9e543966970a",
			"name": "DG_l_N10_nlin2MNI152ASYM2009C_11.1_publicP_f56d6af9b18d378060c91acd2189f5b2.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-DG_pub/11.1/DG_l_N10_nlin2MNI152ASYM2009C_11.1_publicP_f56d6af9b18d378060c91acd2189f5b2.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "f56d6af9b18d378060c91acd2189f5b2"
		},
		{
			"byteSize": 105091.0,
			"hashcode": "a639ef6cc2732e784e32123f919c640f",
			"name": "DG_r_N10_nlin2Stdcolin27_11.1_publicP_e3fa939acecddade8c39c1c55a421c28.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-DG_pub/11.1/DG_r_N10_nlin2Stdcolin27_11.1_publicP_e3fa939acecddade8c39c1c55a421c28.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "e3fa939acecddade8c39c1c55a421c28"
		},
		{
			"byteSize": 21.0,
			"hashcode": "41ce467806f645c2814ab6a2245e5568",
			"name": "subjects_DG.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-DG_pub/11.1/subjects_DG.csv",
			"contentType": "text/csv",
			"hash": "b53cceeb6a14da4074456a7f89f87ab9"
		},
		{
			"byteSize": 83019.0,
			"hashcode": "3ba19b4224bf0102a0ac163220de6482",
			"name": "DG_r_N10_nlin2MNI152ASYM2009C_11.1_publicP_6fe4e1ff07604e82573426ff9d507757.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-DG_pub/11.1/DG_r_N10_nlin2MNI152ASYM2009C_11.1_publicP_6fe4e1ff07604e82573426ff9d507757.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "6fe4e1ff07604e82573426ff9d507757"
		},
		{
			"byteSize": 104058.0,
			"hashcode": "cb7db538eac2383d0ad00ffc6fd396c5",
			"name": "DG_l_N10_nlin2Stdcolin27_11.1_publicP_4d4162b28b535e77fe7ef3d85ee658cb.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-DG_pub/11.1/DG_l_N10_nlin2Stdcolin27_11.1_publicP_4d4162b28b535e77fe7ef3d85ee658cb.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "4d4162b28b535e77fe7ef3d85ee658cb"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/677c84c3-56b2-4671-bc63-15d3dda730a2",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c05e03ddce0de72b2b2b6c3c3b9ca01e",
			"name": "Schneider, F.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d3d619d9-a1fa-4795-abc3-ec78c730cd8a",
			"shortName": "Schneider, F."
		},
		{
			"schema.org/shortName": null,
			"identifier": "f0d8c279b7687c2649debfc4435f110a",
			"name": "Habel, U.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bc3c1bc5-8186-4f81-af2d-dcb1fb55c357",
			"shortName": "Habel, U."
		},
		{
			"schema.org/shortName": null,
			"identifier": "572527acec93ccba7befe3b8fa72d4ca",
			"name": "Shah, Nadim J.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3048fc49-e9c7-43a7-bf7e-ab0aafc6facc",
			"shortName": "Shah, N. J."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "8f85d02ddf1de227f39d91a83c67f28c",
			"name": "Pieperhoff, Peter",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5d838c31-6dcf-44a6-b16b-4d19d770c36b",
			"shortName": "Pieperhoff, P."
		},
		{
			"schema.org/shortName": null,
			"identifier": "e05394ccaa2664b012ee2389e425455e",
			"name": "Kindler, M.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/6f1924d2-616a-45cc-a5c6-54fc3a4e450b",
			"shortName": "Kindler, M."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3e871e11c214a058a43cb1fc38788eea",
			"name": "Kedo, O.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/9dabc7a3-fa77-481f-ab9e-8ef6ac6e7d42",
			"shortName": "Kedo, O."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"id": "677c84c3-56b2-4671-bc63-15d3dda730a2",
	"kgReference": [
		"10.25493/M8JP-XQT"
	],
	"publications": [
		{
			"name": "Cytoarchitectonic mapping of the human amygdala, hippocampal region and entorhinal cortex: intersubject variability and probability maps",
			"cite": "Amunts, K., Kedo, O., Kindler, M., Pieperhoff, P., Mohlberg, H., Shah, N. J., \u2026 Zilles, K. (2005). Cytoarchitectonic mapping of the human amygdala, hippocampal region and entorhinal cortex: intersubject variability and probability maps. Anatomy and Embryology, 210(5-6), 343\u2013352. ",
			"doi": "10.1007/s00429-005-0025-5"
		}
	]
}
