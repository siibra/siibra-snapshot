{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Grodzinsky, Y., Deschamps, I., Pieperhoff, P., Iannilli, F., Agmon, G., Loewenstein, Y., Amunts, K. (2020) Logical negation mapped onto the brain. Brain Structure and Function, 225(1):19-31",
			"doi": "10.1007/s00429-019-01975-w"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains cytoarchitectonic probability maps of the Area Id9 (Insula) in the single subject template of the MNI (\u201cColin 27\u201d) and the MNI ICBM 152 reference space, as part of the Julich-Brain atlas. The area was identified using cytoarchitectonic analysis on cell-body-stained histological sections in both hemispheres of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The maps show the probability to belong to the Area Id9 (Insula) for each voxel of the reference spaces. Data is provided in the NifTi format. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create probabilistic brain maps. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nDelineations of the Area Id9 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here: \nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Id9 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/003bf1cc-a3b4-4457-bd89-20d6b5303c1b"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Id9 (Insula) (v4.0)",
	"files": [
		{
			"byteSize": 83010.0,
			"hashcode": "23c0586c20597f907719cbeed3b30c21",
			"name": "Area-Id9_r_N10_nlin2Stdcolin27_4.0_publicDOI_48ba2358871dc641ce222a0c6b858146.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas_dev/PMs/Area-Id9/4.0/Area-Id9_r_N10_nlin2Stdcolin27_4.0_publicDOI_48ba2358871dc641ce222a0c6b858146.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "48ba2358871dc641ce222a0c6b858146"
		},
		{
			"byteSize": 55093.0,
			"hashcode": "8aefd74c3868fc55c9cf12d119071ccb",
			"name": "Area-Id9_l_N10_nlin2ICBM152asym2009c_4.0_publicDOI_82becd70303e72b31eef33ff96875fad.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas_dev/PMs/Area-Id9/4.0/Area-Id9_l_N10_nlin2ICBM152asym2009c_4.0_publicDOI_82becd70303e72b31eef33ff96875fad.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "716b9cbbdfd996ab1f35d4488d375c38"
		},
		{
			"byteSize": 79926.0,
			"hashcode": "f30f10cad4124dd07c2da23f0e7d9165",
			"name": "Area-Id9_l_N10_nlin2Stdcolin27_4.0_publicDOI_82becd70303e72b31eef33ff96875fad.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas_dev/PMs/Area-Id9/4.0/Area-Id9_l_N10_nlin2Stdcolin27_4.0_publicDOI_82becd70303e72b31eef33ff96875fad.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "82becd70303e72b31eef33ff96875fad"
		},
		{
			"byteSize": 56120.0,
			"hashcode": "62668de84856d201400fa3e5d31c5397",
			"name": "Area-Id9_r_N10_nlin2ICBM152asym2009c_4.0_publicDOI_48ba2358871dc641ce222a0c6b858146.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas_dev/PMs/Area-Id9/4.0/Area-Id9_r_N10_nlin2ICBM152asym2009c_4.0_publicDOI_48ba2358871dc641ce222a0c6b858146.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "0e4751e2f243144bd38f890aa0cc3d4f"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/d9814dcb-1d50-4d95-8014-77dba2ee7d73",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"name": "Quabs, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"shortName": "Quabs, J."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "3918c86f-bb64-428e-a9a3-5031a653e20f",
			"name": "Hein, Marten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3918c86f-bb64-428e-a9a3-5031a653e20f",
			"shortName": "Hein, M."
		}
	],
	"id": "d9814dcb-1d50-4d95-8014-77dba2ee7d73",
	"kgReference": [
		"10.25493/JMCR-ZNQ"
	],
	"publications": [
		{
			"name": "Logical negation mapped onto the brain",
			"cite": "Grodzinsky, Y., Deschamps, I., Pieperhoff, P., Iannilli, F., Agmon, G., Loewenstein, Y., Amunts, K. (2020) Logical negation mapped onto the brain. Brain Structure and Function, 225(1):19-31",
			"doi": "10.1007/s00429-019-01975-w"
		}
	]
}
