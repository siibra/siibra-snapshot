{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Schubert N, Axer M, Schober M, Huynh A-M, Huysegoms M, Palomero-Gallagher N, Bjaalie JG, Leergaard TB, Kirlangic ME, Amunts K and Zilles K (2016) 3D Reconstructed Cyto-, Muscarinic M2 Receptor, and Fiber Architecture of the Rat Brain Registered to the Waxholm Space Atlas. Front. Neuroanat. 10:51. doi: 10.3389/fnana.2016.00051",
			"doi": "10.3389/fnana.2016.00051"
		}
	],
	"activity": [
		{
			"protocols": [
				"3H-Oxotremorine autoradiography"
			],
			"preparation": [
				"In vitro"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"Spatial atlas registration",
		"In vitro receptor autoradiography"
	],
	"custodians": [
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		}
	],
	"project": [],
	"description": "Transmitter receptors are key molecules of signal transmission. They are heterogeneously distributed throughout the brain. Therefore, it is necessary to analyze their spatial organization and the relationship between regional and laminar receptor densities with defined anatomical structures in order to understand the anatomical and molecular organization of the brain. Quantitative in vitro receptor autoradiography is a well-established technique to demonstrate receptor binding sites at a high spatial resolution. To compensate non-linear distortions, autoradiographs visualizing the regional and laminar distribution patterns of muscarinic cholinergic M2 receptors throughout the rat brain were 3D-reconstructed and the ensuing volume was aligned with an undistorted reference volume, the common rat reference template: the Waxholm Space atlas (Papp et al, 2014). Alignment of data to the Waxholm space was done in collaboration with SP5 (Jan Bjaalie).",
	"parcellationAtlas": [
		{
			"name": "Waxholm Space rat brain atlas v2",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/2449a7f0-6dd0-4b5a-8f1e-aec0db03679d",
			"id": "2449a7f0-6dd0-4b5a-8f1e-aec0db03679d"
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-sa/4.0"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [
		"Rattus norvegicus"
	],
	"name": "Atlas of muscarinic M2 receptor distributions in the rat brain",
	"files": [
		{
			"byteSize": 536871264.0,
			"hashcode": "f7dde2a9ca5ed26d00029bf3f41a7f3a",
			"name": "hbp-00044_R8_m2_receptor.nii",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_63ea6845b1d34ad7a43c8158d9572867/hbp-d00007_Atlas-M2-receptor-rat_pub/hbp-00044/44/8/m2_receptor/hbp-00044_R8_m2_receptor.nii",
			"contentType": "application/octet-stream",
			"hash": "86eac821cc0d98de5a45d8fd279d98cd"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/b06569ef-3dc2-464a-b5d9-42d6abdd9123",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "3b3ad0ec-11be-44b4-bf32-13838cfa7776",
			"name": "Schubert, Nicole",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/3b3ad0ec-11be-44b4-bf32-13838cfa7776",
			"shortName": "Schubert, N."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		}
	],
	"id": "b06569ef-3dc2-464a-b5d9-42d6abdd9123",
	"kgReference": [
		"10.25493/QFN4-BPS"
	],
	"publications": [
		{
			"name": "3D Reconstructed Cyto-, Muscarinic M2 Receptor, and Fiber Architecture of the Rat Brain Registered to the Waxholm Space Atlas.",
			"cite": "Schubert N, Axer M, Schober M, Huynh A-M, Huysegoms M, Palomero-Gallagher N, Bjaalie JG, Leergaard TB, Kirlangic ME, Amunts K and Zilles K (2016) 3D Reconstructed Cyto-, Muscarinic M2 Receptor, and Fiber Architecture of the Rat Brain Registered to the Waxholm Space Atlas. Front. Neuroanat. 10:51. doi: 10.3389/fnana.2016.00051",
			"doi": "10.3389/fnana.2016.00051"
		}
	]
}
