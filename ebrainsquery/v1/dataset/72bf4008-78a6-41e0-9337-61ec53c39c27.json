{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "BigBrain",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/a1655b99-82f1-420f-a3c2-fe80fd4c8588"
		}
	],
	"methods": [
		"magnetic resonance imaging (MRI)",
		"silver staining",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Ultrahigh resolution 3D maps of cytoarchitectonic areas in the Big Brain model"
	],
	"description": "This dataset contains cytoarchitectonic maps of Area 6d3 (SFS) in the Big Brain dataset [Amunts et al. 2013]. The mappings were created using the semi-automatic method presented in Schleicher et al. 1999, based on coronal histological sections on 1 micron resolution. Mappings are available on approximately every 15-60th section of this region. They were then aligned to the corresponding sections of the 3D reconstructed Big Brain space, using the transformations used in Amunts et al. 2013, kindly provided by Claude Lepage (McGill University).\n\nFrom these delineations, a preliminary 3D map of Area 6d3 (SFS) has been created by simple interpolation of the coronal contours in the 3D anatomical space of the Big Brain. This map gives a first impression of the location of this area in the Big Brain, and can be viewed in the atlas viewer using the URL below.\n\nA full mapping of this area in every histological section using a Deep Learning approach is in progress.\n\n\n**Additional information:**\nThe reference delineations used for this map are part of the work for the corresponding probabilistic map of Area 6d3 (SFS) of the JuBrain Cytoarchitectonic Atlas, published in:\nSigl et al. (2019) [Data set, v4.0] [DOI: 10.25493/40J8-TAK](https://doi.org/10.25493%2F40J8-TAK)\nSigl et al. (2019) [Data set, v4.1] [DOI: 10.25493/NVJ5-JJ](https://doi.org/10.25493%2FNVJ5-JJ)\n\n\n",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 6d3 (SFS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/266c1ada-1840-462f-8223-7ff2df457552"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Interpolated 3D map of Area 6d3 (SFS) in the BigBrain",
	"files": [
		{
			"byteSize": 116603.0,
			"hashcode": "b417d1144c03aaffe804e26fcc14a86d",
			"name": "area6d_6d3.zip",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-6d3/annotations/area6d_6d3.zip",
			"contentType": "application/zip",
			"hash": "c09bb8b478ce095d619ad93a312cf3aa"
		},
		{
			"byteSize": 1483085.0,
			"hashcode": "538c907de4899661cb2984630be03052",
			"name": "preview_volume_area6d_6d3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-6d3/volumes/preview_volume_area6d_6d3.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "9107e6d78e47ded0a8a29a4a7ae4afc7"
		},
		{
			"byteSize": 144219262.0,
			"hashcode": "46bb23f5323b6868f95b9c754b387e9f",
			"name": "volume_area6d_6d3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-6d3/volumes/volume_area6d_6d3.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "8339fcf6c2aec09ac0aa06ca263b5efb"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/72bf4008-78a6-41e0-9337-61ec53c39c27",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c5d27bb9-4353-41f5-ab4b-089d8453391d303708",
			"name": "Sigl, Benjamin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c5d27bb9-4353-41f5-ab4b-089d8453391d",
			"shortName": "Sigl, B."
		}
	],
	"id": "72bf4008-78a6-41e0-9337-61ec53c39c27",
	"kgReference": [
		"10.25493/B87N-ZDX"
	],
	"publications": []
}
