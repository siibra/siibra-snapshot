{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Zilles, K., Bacha-Trams, M., Palomero-Gallagher, N., Amunts, K., & Friederici, A. D. (2015). Common molecular basis of the sentence comprehension network revealed by neurotransmitter receptor fingerprints. Cortex, 63, 79\u201389. ",
			"doi": "10.1016/j.cortex.2014.07.007"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology",
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"Semantic atlas registration",
		"autoradiography - quantitative analysis",
		"autoradiography - imaging"
	],
	"custodians": [
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		}
	],
	"project": [
		"Quantitative receptor data"
	],
	"description": "This dataset contains the densities (in fmol/mg protein) of receptors for classical neurotransmitters in Area TE 1.0 (HESCHL) obtained by means of quantitative _in vitro_ autoradiography. The receptor densities are visualized as _fingerprints_ (**fp**), which provide the mean density and standard deviation for each of the analyzed receptor types, averaged across samples. \n\nOverview of available measurements [ **receptor** \\| **_neurotransmitter_** \\| _labeling agent_ ]:\n**AMPA** \\| **_glutamate_** \\| _[\u00b3H]AMPA_\n**kainate** \\| **_glutamate_** \\| _[\u00b3H]kainate_\n**NMDA** \\| **_glutamate_** \\| _[\u00b3H]MK-801_\n**mGluR2/3** \\| **_glutamate_** \\| _[\u00b3H]LY341495_\n**GABA<sub>A</sub>** \\| **_GABA_** \\| _[\u00b3H]muscimol_\n**GABA<sub>B</sub>** \\| **_GABA_** \\| _[\u00b3H]CGP54626_\n**GABA<sub>A</sub>/BZ** \\| **_GABA_** \\| _[\u00b3H]flumazenil_\n**muscarinic M<sub>1</sub>** \\| **_acetylcholine_** \\| _[\u00b3H]pirenzepine_\n**muscarinic M<sub>2</sub>** \\| **_acetylcholine_** \\| _[\u00b3H]oxotremorine-M_\n**muscarinic M<sub>3</sub>** \\| **_acetylcholine_** \\| _[\u00b3H]4-DAMP_\n**nicotinic \u03b1<sub>4</sub>\u03b2<sub>2</sub>** \\| **_acetylcholine_** \\| _[\u00b3H]epibatidine_\n**\u03b1<sub>1</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[\u00b3H]prazosin_\n**\u03b1<sub>2</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[\u00b3H]UK-14,304_\n**5-HT<sub>1A</sub>** \\| **_serotonin_** \\| _[\u00b3H]8-OH-DPAT_\n**5-HT<sub>2</sub>** \\| **_serotonin_** \\| _[\u00b3H]ketanserin_\n**D<sub>1</sub>** \\| **_dopamine_** \\| _[\u00b3H]SCH23390_\n\nInformation on the used tissue samples and corresponding subjects, as well as analyzed receptors accompanies the provided dataset.\n\n**For methodological details, see:**\nZilles, K. et al. (2002). Quantitative analysis of cyto- and receptorarchitecture of the human brain, pp. 573-602. In: Brain Mapping: The Methods, 2nd edition (A.W. Toga and J.C. Mazziotta, eds.). San Diego, Academic Press.\n\nPalomero-Gallagher N, Zilles K. (2018) Cyto- and receptorarchitectonic mapping of the human brain. In: Handbook of Clinical Neurology 150: 355-387\n\n**The receptor density data of this area were also published in:**\nMorosan P, Rademacher J, Palomero-Gallagher N, and Zilles K (2005) Anatomical organization of the human auditory cortex: Cytoarchitecture and transmitter receptors. In Heil P, Scheich H, Bundinger E, and K\u00f6nig R (Eds), The Auditory Cortex - Towards a Synthesis of Human and Animal Research. (pp. 27-50) [Mahwah, New Jersey, USA: Lawrence Erlbaum Associates](https://www.taylorfrancis.com/books/e/9781135613365/chapters/10.4324%2F9781410613066-9)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 1.0 (HESCHL)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/13e21153-2ba8-4212-b172-8894f1012225"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Density measurements of different receptors for Area TE 1.0 (HESCHL) [human, v1.0]",
	"files": [
		{
			"byteSize": 298.0,
			"hashcode": "683f74a254620a9c5c7a2d58473c1376",
			"name": "tissue-samples.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-TE1_0_pub/v1.0/tissue-samples.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "a8205282c97f8e89c7b5a32c806bae28"
		},
		{
			"byteSize": 145.0,
			"hashcode": "aa0ef25171107a21e5b3a3e5bd30403d",
			"name": "subjects.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-TE1_0_pub/v1.0/subjects.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "fc5c373975520510faf3a620846ca655"
		},
		{
			"byteSize": 1075.0,
			"hashcode": "04373c61964edfa4d33cd071671c6752",
			"name": "labelling-agents.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-TE1_0_pub/v1.0/labelling-agents.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "2735fccdb87a9b381bbba9c0f177634b"
		},
		{
			"byteSize": 1795.0,
			"hashcode": "38e7e553ce4814cf5f7cce91d50cdd9a",
			"name": "receptors.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-TE1_0_pub/v1.0/receptors.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "db83d8ad18c7b47048f70c3c4056a3b1"
		},
		{
			"byteSize": 410.0,
			"hashcode": "048001f3f3a8e95e44cb68bd6370bc43",
			"name": "TE1_0_fp_20180405.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-TE1_0_pub/v1.0/TE1_0_fp_20180405.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "86829f2fe2c596e815d10a3e323cdf1c"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/68ae438a-d92f-4d51-9f33-4cd53ac94de3",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		}
	],
	"id": "06136a4b8c6c01a38db5063aef8c53b9",
	"kgReference": [
		"10.25493/AHX0-9PU"
	],
	"publications": [
		{
			"name": "Common molecular basis of the sentence comprehension network revealed by neurotransmitter receptor fingerprints",
			"cite": "Zilles, K., Bacha-Trams, M., Palomero-Gallagher, N., Amunts, K., & Friederici, A. D. (2015). Common molecular basis of the sentence comprehension network revealed by neurotransmitter receptor fingerprints. Cortex, 63, 79\u201389. ",
			"doi": "10.1016/j.cortex.2014.07.007"
		}
	]
}
