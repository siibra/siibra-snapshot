{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "BigBrain",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/a1655b99-82f1-420f-a3c2-fe80fd4c8588"
		}
	],
	"methods": [
		"magnetic resonance imaging (MRI)",
		"silver staining",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Ultrahigh resolution 3D maps of cytoarchitectonic areas in the Big Brain model"
	],
	"description": "This dataset contains cytoarchitectonic maps of Area TE 3 (STG) in the Big Brain dataset [Amunts et al. 2013]. The mappings were created using the semi-automatic method presented in Schleicher et al. 1999, based on coronal histological sections on 1 micron resolution. Mappings are available on approximately every 15-60th section of this region. They were then aligned to the corresponding sections of the 3D reconstructed Big Brain space, using the transformations used in Amunts et al. 2013, kindly provided by Claude Lepage (McGill University).\n\nFrom these delineations, a preliminary 3D map of Area TE 3 (STG) has been created by simple interpolation of the coronal contours in the 3D anatomical space of the Big Brain. This map gives a first impression of the location of this area in the Big Brain, and can be viewed in the atlas viewer using the URL below.\n\nA full mapping of this area in every histological section using a Deep Learning approach is in progress.\n\n\n**Additional information:**\nThe reference delineations used for this map are part of the work for the corresponding probabilistic map of Area TE 3 (STG) of the JuBrain Cytoarchitectonic Atlas, published in:\nGeyer et al. (2018) [Data set] [DOI: 10.25493/V09X-3EW](https://doi.org/10.25493%2FV09X-3EW)\nGeyer et al. (2019) [Data set, v5.1] [DOI: 10.25493/BN5J-JT8](https://doi.org/10.25493%2FBN5J-JT8)\n\nIn addition, the dataset of the probabilistic cytoarchitectonic map of Area TE 3 (STG) is part of the following research publications:\nSchleicher, A., Amunts, K., Geyer, S., Morosan, P., & Zilles, K. (1999). Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics. NeuroImage, 9(1), 165\u2013177. [DOI: 10.1006/nimg.1998.0385](https://doi.org/10.1006%2Fnimg.1998.0385)\n\nMorosan, P., Schleicher, A., Amunts, K., & Zilles, K. (2005). Multimodal architectonic mapping of human superior temporal gyrus. Anatomy and Embryology, 210(5-6), 401\u2013406. [DOI: 10.1007/s00429-005-0029-1](https://doi.org/10.1007%2Fs00429-005-0029-1)\n",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 3 (STG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/7e1a3291-efdc-4ca6-a3d0-6c496c34639f"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Interpolated 3D map of Area TE 3 (STG) in the BigBrain",
	"files": [
		{
			"byteSize": 262128150.0,
			"hashcode": "46bdde80b28cb3160d25bd9d826d7011",
			"name": "volume_auditory_Te3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-TE-3/volumes/volume_auditory_Te3.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "097ea802ae236b3b5bc61a68cb4390c5"
		},
		{
			"byteSize": 453161.0,
			"hashcode": "bf6774190dc5023f0543bc5ab54b37c9",
			"name": "auditory_Te3.zip",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-TE-3/annotations/auditory_Te3.zip",
			"contentType": "application/zip",
			"hash": "955fddfe4108fda26ed1062b38b3f47a"
		},
		{
			"byteSize": 2762059.0,
			"hashcode": "ebc3f0e8d95d5ef95f68fec2fde6266c",
			"name": "preview_volume_auditory_Te3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-TE-3/volumes/preview_volume_auditory_Te3.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "5b074c0afcfd711a50d4ac80f665f400"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/45689ca2-df48-4260-a908-56a9d4eb344e",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "183ac28a5978832f25623307522cba98",
			"name": "Geyer, Stefan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a38f341-5b88-4cb2-b8b7-6473a676d892",
			"shortName": "Geyer, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		}
	],
	"id": "45689ca2-df48-4260-a908-56a9d4eb344e",
	"kgReference": [
		"10.25493/N127-HWS"
	],
	"publications": []
}
