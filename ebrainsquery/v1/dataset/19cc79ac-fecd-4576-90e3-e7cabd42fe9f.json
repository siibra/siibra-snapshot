{
	"formats": [],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology",
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"Semantic atlas registration",
		"autoradiography - quantitative analysis",
		"autoradiography - imaging"
	],
	"custodians": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"project": [
		"Quantitative receptor data"
	],
	"description": "This dataset contains the densities (in fmol/mg protein) of receptors for classical neurotransmitters of CA, stratum cellulare (hippocampus) obtained by means of quantitative _in vitro_ autoradiography. The receptor densities are visualized as _fingerprints_ (**fp**), which provide the mean density and standard deviation for each of the analyzed receptor types, averaged across samples. \n\nOverview of available measurements [ **receptor** \\| **_neurotransmitter_** \\| _labeling agent_ ]:\n**AMPA** \\| **_glutamate_** \\| _[<sup>3</sup>H]AMPA_\n**kainate** \\| **_glutamate_** \\| _[<sup>3</sup>H]kainate_\n**NMDA** \\| **_glutamate_** \\| _[<sup>3</sup>H]MK-801_\n**GABA<sub>A</sub>** \\| **_GABA_** \\| _[<sup>3</sup>H]muscimol_\n**muscarinic M<sub>1</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]pirenzepine_\n**muscarinic M<sub>2</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]oxotremorine-M_\n**&#945<sub>1</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[<sup>3</sup>H]prazosin_\n**&#945<sub>2</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[<sup>3</sup>H]UK-14,304_\n**5-HT<sub>1A</sub>** \\| **_serotonin_** \\| _[<sup>3</sup>H]8-OH-DPAT_\n**5-HT<sub>2</sub>** \\| **_serotonin_** \\| _[<sup>3</sup>H]ketanserin_\n\nInformation on the used tissue samples and corresponding subjects, as well as analyzed receptors accompanies the provided dataset.\n\n**For methodological details, see:**\nZilles, K. et al. (2002). Quantitative analysis of cyto- and receptorarchitecture of the human brain, pp. 573-602. In: Brain Mapping: The Methods, 2nd edition (A.W. Toga and J.C. Mazziotta, eds.). San Diego, Academic Press.\n\nPalomero-Gallagher N, Zilles K. (2018) Cyto- and receptorarchitectonic mapping of the human brain. In: Handbook of Clinical Neurology 150: 355-387",
	"parcellationAtlas": [
		{
			"name": "Waxholm Space rat brain atlas v3",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/ebb923ba-b4d5-4b82-8088-fa9215c2e1fe",
			"id": "ebb923ba-b4d5-4b82-8088-fa9215c2e1fe"
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Rat Cornu ammonis 3 (v3)",
			"alias": "Cornu ammonis 3",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/7811b7c2-3cb7-41aa-9f31-5855a95f7388"
		},
		{
			"species": [],
			"name": "Rat Cornu ammonis 2 (v3)",
			"alias": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/ad68b464-3b7f-4c60-9874-8ef79b9340e4"
		},
		{
			"species": [],
			"name": "Rat Cornu ammonis 1 (v3) ",
			"alias": "Cornu ammonis 1",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/be6a45c8-a7bd-454b-b44d-2220e06651c4"
		}
	],
	"species": [
		"Rattus norvegicus"
	],
	"name": "Density measurements of different receptors for CA, stratum cellulare (Hippocampus) [rat, v2.0]",
	"files": [
		{
			"byteSize": 327.0,
			"hashcode": "5ce67e3f8addaa0384e27a4d096587a3",
			"name": "CAcell_fp_20200217.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CAcell_pub/v2.0/CAcell_fp_20200217.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "75a80df730e0b6119effb5bc87e1aa57"
		},
		{
			"byteSize": 827.0,
			"hashcode": "82bdd4fa0b9b113c91ca919d7e66c71d",
			"name": "labelling-agents.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CAcell_pub/v2.0/labelling-agents.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "974697e97f1effaeb73a4ab2aaa1f125"
		},
		{
			"byteSize": 168.0,
			"hashcode": "41bddf68024171cf1f4e99015aaf0796",
			"name": "tissue-samples.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CAcell_pub/v2.0/tissue-samples.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "dc8736b61ff9074874935518983fe553"
		},
		{
			"byteSize": 270.0,
			"hashcode": "2a26843b5bf919b32ecab4dfa3882ffb",
			"name": "subjects.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CAcell_pub/v2.0/subjects.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "ff1dede7313d846a04e81e6f0ac42a4d"
		},
		{
			"byteSize": 1236.0,
			"hashcode": "5d3b281307127c50d629ce5dabf039aa",
			"name": "receptors.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-rat-CAcell_pub/v2.0/receptors.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "6be5139f4cd307dc058a2771472a9cb6"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/19cc79ac-fecd-4576-90e3-e7cabd42fe9f",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "19cc79ac-fecd-4576-90e3-e7cabd42fe9f",
	"kgReference": [
		"10.25493/P4DW-4RN"
	],
	"publications": []
}
