{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Schleicher, A., Amunts, K., Geyer, S., Morosan, P., & Zilles, K. (1999). Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics. NeuroImage, 9(1), 165\u2013177. ",
			"doi": "10.1006/nimg.1998.0385"
		},
		{
			"cite": "Morosan, P., Schleicher, A., Amunts, K., & Zilles, K. (2005). Multimodal architectonic mapping of human superior temporal gyrus. Anatomy and Embryology, 210(5-6), 401\u2013406. ",
			"doi": "10.1007/s00429-005-0029-1"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area TE 3 (STG) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area TE 3 (STG). The probability map of Area TE 3 (STG) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area TE 3 (STG):\nMorosan et al. (2019) [Data set, v5.1] [DOI: 10.25493/BN5J-JT8](https://doi.org/10.25493%2FBN5J-JT8)\n\nThe most probable delineation of Area TE 3 (STG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 3 (STG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/7e1a3291-efdc-4ca6-a3d0-6c496c34639f"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area TE 3 (STG) (v5.0)",
	"files": [
		{
			"byteSize": 135533.0,
			"hashcode": "e50e502a9212c59a63218860d9f1ec7b",
			"name": "Area-TE-3_r_N10_nlin2Stdcolin27_5.0_publicP_39a70d3a55358d2fbcfdd819b0238f56.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/5.0/Area-TE-3_r_N10_nlin2Stdcolin27_5.0_publicP_39a70d3a55358d2fbcfdd819b0238f56.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "39a70d3a55358d2fbcfdd819b0238f56"
		},
		{
			"byteSize": 113914.0,
			"hashcode": "b9fd2f354e4baa01ce1b7493c826c5a6",
			"name": "Area-TE-3_r_N10_nlin2ICBM152ASYM2009C_5.0_publicP_5b360366ee3e95e3309231d0775b1e59.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/5.0/Area-TE-3_r_N10_nlin2ICBM152ASYM2009C_5.0_publicP_5b360366ee3e95e3309231d0775b1e59.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "5b360366ee3e95e3309231d0775b1e59"
		},
		{
			"byteSize": 133030.0,
			"hashcode": "72f86ab9de44bb697ca2ee02195ad1fb",
			"name": "Area-TE-3_l_N10_nlin2Stdcolin27_5.0_publicP_6a685798247827f71d9d5c85334bf3c4.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/5.0/Area-TE-3_l_N10_nlin2Stdcolin27_5.0_publicP_6a685798247827f71d9d5c85334bf3c4.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "6a685798247827f71d9d5c85334bf3c4"
		},
		{
			"byteSize": 21.0,
			"hashcode": "aa903c1daff4c726508ac092a4905595",
			"name": "subjects_Area-TE-3.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/5.0/subjects_Area-TE-3.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		},
		{
			"byteSize": 104084.0,
			"hashcode": "10b06519efcc5b478982a79563d1da4b",
			"name": "Area-TE-3_l_N10_nlin2ICBM152ASYM2009C_5.0_publicP_4839d06d731847a38f2216a4c119c1e0.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/5.0/Area-TE-3_l_N10_nlin2ICBM152ASYM2009C_5.0_publicP_4839d06d731847a38f2216a4c119c1e0.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "4839d06d731847a38f2216a4c119c1e0"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/e2dfa735-7822-4216-a145-dccd8fca0773",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "183ac28a5978832f25623307522cba98",
			"name": "Geyer, Stefan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a38f341-5b88-4cb2-b8b7-6473a676d892",
			"shortName": "Geyer, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		}
	],
	"id": "e8a38320b02668ffce3df8b9024163ff",
	"kgReference": [
		"10.25493/V09X-3EW"
	],
	"publications": [
		{
			"name": "Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics",
			"cite": "Schleicher, A., Amunts, K., Geyer, S., Morosan, P., & Zilles, K. (1999). Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics. NeuroImage, 9(1), 165\u2013177. ",
			"doi": "10.1006/nimg.1998.0385"
		},
		{
			"name": "Multimodal architectonic mapping of human superior temporal gyrus",
			"cite": "Morosan, P., Schleicher, A., Amunts, K., & Zilles, K. (2005). Multimodal architectonic mapping of human superior temporal gyrus. Anatomy and Embryology, 210(5-6), 401\u2013406. ",
			"doi": "10.1007/s00429-005-0029-1"
		}
	]
}
