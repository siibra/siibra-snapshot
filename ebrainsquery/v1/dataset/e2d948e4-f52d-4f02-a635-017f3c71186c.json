{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Rottschy, C., Eickhoff, S. B., Schleicher, A., Mohlberg, H., Kujovic, M., Zilles, K., & Amunts, K. (2007). Ventral visual cortex in humans: Cytoarchitectonic mapping of two extrastriate areas. Human Brain Mapping, 28(10), 1045\u20131059. ",
			"doi": "10.1002/hbm.20348"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area hOc4v (LingG) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area hOc4v (LingG). The probability map of Area hOc4v (LingG) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area hOc4v (LingG):\nRottschy et al. (2018) [Data set, v3.2] [DOI: 10.25493/E0YJ-ADE](https://doi.org/10.25493%2FE0YJ-ADE)\n\nThe most probable delineation of Area hOc4v (LingG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hOc4v (LingG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/27d91cbb-5611-4d38-bd17-c0f1ac22b4cc"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area hOc4v (LingG) (v3.4)",
	"files": [
		{
			"byteSize": 20.0,
			"hashcode": "c8c2604a0c6b842cba64d323b75a4e97",
			"name": "subjects_Area-hOc4v.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4v_pub/3.4/subjects_Area-hOc4v.csv",
			"contentType": "text/csv",
			"hash": "d4fd357f7807fd448ba3bc2240f5df20"
		},
		{
			"byteSize": 105946.0,
			"hashcode": "e1f770f4553d597a90ea1231455cf58d",
			"name": "Area-hOc4v_r_N10_nlin2MNI152ASYM2009C_3.4_publicP_520fad8687876ce9cbbb2d4ef43de4d2.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4v_pub/3.4/Area-hOc4v_r_N10_nlin2MNI152ASYM2009C_3.4_publicP_520fad8687876ce9cbbb2d4ef43de4d2.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "520fad8687876ce9cbbb2d4ef43de4d2"
		},
		{
			"byteSize": 125561.0,
			"hashcode": "3d5877b01709b0efefc39f9c4622156e",
			"name": "Area-hOc4v_r_N10_nlin2Stdcolin27_3.4_publicP_82034ecd08bd39b16df0f465c94ceac0.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4v_pub/3.4/Area-hOc4v_r_N10_nlin2Stdcolin27_3.4_publicP_82034ecd08bd39b16df0f465c94ceac0.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "82034ecd08bd39b16df0f465c94ceac0"
		},
		{
			"byteSize": 135974.0,
			"hashcode": "43bb37f8c7b3ab51c530f1fe5cbcdf01",
			"name": "Area-hOc4v_l_N10_nlin2Stdcolin27_3.4_publicP_45fabb5f075b26870141ef823fa15afb.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4v_pub/3.4/Area-hOc4v_l_N10_nlin2Stdcolin27_3.4_publicP_45fabb5f075b26870141ef823fa15afb.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "45fabb5f075b26870141ef823fa15afb"
		},
		{
			"byteSize": 121349.0,
			"hashcode": "b5c9f112eb5b36fcfa93390d4a0e3310",
			"name": "Area-hOc4v_l_N10_nlin2MNI152ASYM2009C_3.4_publicP_411fef399d0549ef7e4b5a717fdd4c23.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc4v_pub/3.4/Area-hOc4v_l_N10_nlin2MNI152ASYM2009C_3.4_publicP_411fef399d0549ef7e4b5a717fdd4c23.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "411fef399d0549ef7e4b5a717fdd4c23"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/e2d948e4-f52d-4f02-a635-017f3c71186c",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "0621a40fd626426cdf04ffaf13c63b3a",
			"name": "Kujovic, Milenko",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/cfa0e3b6-c3d8-4c82-a46b-82d24c7fb750",
			"shortName": "Kujovic, M."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "465d8385bb1da6c9b4fde7c570e14cde",
			"name": "Rottschy, Claudia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/960ac8ca-a15c-4362-a565-c04d047bc52d",
			"shortName": "Rottschy, C."
		}
	],
	"id": "e2d948e4-f52d-4f02-a635-017f3c71186c",
	"kgReference": [
		"10.25493/AASR-M8P"
	],
	"publications": [
		{
			"name": "Ventral visual cortex in humans: Cytoarchitectonic mapping of two extrastriate areas",
			"cite": "Rottschy, C., Eickhoff, S. B., Schleicher, A., Mohlberg, H., Kujovic, M., Zilles, K., & Amunts, K. (2007). Ventral visual cortex in humans: Cytoarchitectonic mapping of two extrastriate areas. Human Brain Mapping, 28(10), 1045\u20131059. ",
			"doi": "10.1002/hbm.20348"
		}
	]
}
