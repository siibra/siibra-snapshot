{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Richter, M., Amunts, K., Mohlberg, H., Bludau, S., Eickhoff, S. B., Zilles, K., Caspers, S. (2018). Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions. Cerebral Cortex, 29(3), 1305-1327",
			"doi": "10.1093/cercor/bhy245"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area hIP8 (IPS) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area hIP8 (IPS). The probability map of Area hIP8 (IPS) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.  \n\nOther available data versions of Area hIP8 (IPS):\nRichter et al. (2019) [Data set, v7.1] [DOI: 10.25493/YYT8-FT8](https://doi.org/10.25493%2FYYT8-FT8)\n\nThe most probable delineation of Area hIP8 (IPS) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hIP8 (IPS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/a2c1acc7-7fdc-4fbd-90ee-729eda7fdff3"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area hIP8 (IPS) (v7.0)",
	"files": [
		{
			"byteSize": 82500.0,
			"hashcode": "a3ffd267c355bd49cefa21604715ea27",
			"name": "Area-hIP8_l_N10_nlin2ICBM152ASYM2009C_7.0_publicP_276083e310580526283a5f9abf1ba53e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP8_pub/7.0/Area-hIP8_l_N10_nlin2ICBM152ASYM2009C_7.0_publicP_276083e310580526283a5f9abf1ba53e.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "276083e310580526283a5f9abf1ba53e"
		},
		{
			"byteSize": 103409.0,
			"hashcode": "e1d51a648be8fb87f2afe4442a10618f",
			"name": "Area-hIP8_l_N10_nlin2Stdcolin27_7.0_publicP_655bda170ac9f11a630c6990bad23b49.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP8_pub/7.0/Area-hIP8_l_N10_nlin2Stdcolin27_7.0_publicP_655bda170ac9f11a630c6990bad23b49.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "655bda170ac9f11a630c6990bad23b49"
		},
		{
			"byteSize": 23.0,
			"hashcode": "528c17fa4f75d6f4da4f9029f300e7e3",
			"name": "subjects_Area-hIP8.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP8_pub/7.0/subjects_Area-hIP8.csv",
			"contentType": "text/csv",
			"hash": "662f05cadecb8a40e61bebf252676853"
		},
		{
			"byteSize": 105981.0,
			"hashcode": "0f28366258d94673636f52d949e93446",
			"name": "Area-hIP8_r_N10_nlin2Stdcolin27_7.0_publicP_58b087718ad50bf286a9842be8761185.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP8_pub/7.0/Area-hIP8_r_N10_nlin2Stdcolin27_7.0_publicP_58b087718ad50bf286a9842be8761185.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "58b087718ad50bf286a9842be8761185"
		},
		{
			"byteSize": 85078.0,
			"hashcode": "9ad63965fa12ad0d4ed4995595fce8f6",
			"name": "Area-hIP8_r_N10_nlin2ICBM152ASYM2009C_7.0_publicP_36f841254f97698eeb6ff493f9fd0ff8.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hIP8_pub/7.0/Area-hIP8_r_N10_nlin2ICBM152ASYM2009C_7.0_publicP_36f841254f97698eeb6ff493f9fd0ff8.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "36f841254f97698eeb6ff493f9fd0ff8"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/e743b8bf-238a-4e89-afeb-3338f83ac9fc",
	"contributors": [
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"name": "Richter, Monika",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"shortName": "Richter, M."
		}
	],
	"id": "e743b8bf-238a-4e89-afeb-3338f83ac9fc",
	"kgReference": [
		"10.25493/JBV5-J8J"
	],
	"publications": [
		{
			"name": " Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions",
			"cite": "Richter, M., Amunts, K., Mohlberg, H., Bludau, S., Eickhoff, S. B., Zilles, K., Caspers, S. (2018). Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions. Cerebral Cortex, 29(3), 1305-1327",
			"doi": "10.1093/cercor/bhy245"
		}
	]
}
