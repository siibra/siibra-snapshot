{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Amunts, K., Mohlberg, H., Bludau, S., Zilles, K. (2020). Julich-Brain \u2013 A 3D probabilistic atlas of human brain\u2019s cytoarchitecture. Science 369, 988-992",
			"doi": "10.1126/science.abb4588"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"maximum probability mapping",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the \u201cGap Map Frontal II\u201d in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. In order to provide whole-brain coverage for the cortex within the Julich-Brain Atlas, yet uncharted parts of the frontal cortex have been combined to the brain region \u201cGap Map Frontal II\u201d. The distributions were modeled so that probabilistic gap maps were computed in analogy to other maps of the Julich-Brain Atlas. The probabilistic map of \u201cGap Map Frontal II\u201d is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. New maps are continuously replacing parts of \u201cGap Map Frontal II\u201d with progress in mapping.\n\nOther available data versions of Gap Map Frontal II:\nAmunts et al. (2020) [Data set, v9.0] [DOI: 10.25493/XCKH-NF9](https://doi.org/10.25493%2FXCKH-NF9)\nAmunts et al. (2020) [Data set, v9.1] [DOI: 10.25493/H54A-PEB](https://doi.org/10.25493%2FH54A-PEB)\nAmunts et al. (2020) [Data set, v9.2] [DOI: 10.25493/Q0MY-250](https://doi.org/10.25493%2FQ0MY-250)\nAmunts et al. (2020) [Data set, v10.0] [DOI: 10.25493/9M00-CJN](https://doi.org/10.25493%2F9M00-CJN)\n\nThe most probable delineation of Gap Map Frontal II derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "GapMap Frontal II",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/0f5868cb-bd82-4e38-8eeb-fdbffa839247"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "GapMap Frontal II (v11.3)",
	"files": [
		{
			"byteSize": 213057.0,
			"hashcode": "dc14ab42d18825f8ae15d7154534881e",
			"name": "GapMapPublic_Frontal-II_r_N10_nlin2ICBM152asym2009c_11.3_publicDOI_f045564772c4094785e9876e796e2a0a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-GapMap_Frontal-II_pub/11.3/GapMapPublic_Frontal-II_r_N10_nlin2ICBM152asym2009c_11.3_publicDOI_f045564772c4094785e9876e796e2a0a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "70a56099e0c967328149326fbc9d77e9"
		},
		{
			"byteSize": 65.0,
			"hashcode": "05ef8a1cbbf60d269a24c0e2c1739465",
			"name": "subjects_GapMapPublic_Frontal-II.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-GapMap_Frontal-II_pub/11.3/subjects_GapMapPublic_Frontal-II.csv",
			"contentType": "text/csv",
			"hash": "781fb25041225b894a1110f290925a94"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "37032b7423adec0e1536a7fddfb4d95c",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-GapMap_Frontal-II_pub/11.3/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 179267.0,
			"hashcode": "55980b0a88e7cf7bb4f5c79e478a27dc",
			"name": "GapMapPublic_Frontal-II_l_N10_nlin2ICBM152asym2009c_11.3_publicDOI_db2d90b1cb921b239d20588120f8d33c.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-GapMap_Frontal-II_pub/11.3/GapMapPublic_Frontal-II_l_N10_nlin2ICBM152asym2009c_11.3_publicDOI_db2d90b1cb921b239d20588120f8d33c.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "993766035e4d567a4d3a6ef462c3353d"
		},
		{
			"byteSize": 143292.0,
			"hashcode": "baffc978dfcff9172b8c80dcfdbfd738",
			"name": "GapMapPublic_Frontal-II_l_N10_nlin2StdColin27_11.3_publicDOI_db2d90b1cb921b239d20588120f8d33c.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-GapMap_Frontal-II_pub/11.3/GapMapPublic_Frontal-II_l_N10_nlin2StdColin27_11.3_publicDOI_db2d90b1cb921b239d20588120f8d33c.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "db2d90b1cb921b239d20588120f8d33c"
		},
		{
			"byteSize": 163180.0,
			"hashcode": "39e1d39442ad69c94faca29de7b5c55f",
			"name": "GapMapPublic_Frontal-II_r_N10_nlin2StdColin27_11.3_publicDOI_f045564772c4094785e9876e796e2a0a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-GapMap_Frontal-II_pub/11.3/GapMapPublic_Frontal-II_r_N10_nlin2StdColin27_11.3_publicDOI_f045564772c4094785e9876e796e2a0a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "f045564772c4094785e9876e796e2a0a"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/871e077b-fb0b-4579-9e69-713da04d217b",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "8f85d02ddf1de227f39d91a83c67f28c",
			"name": "Pieperhoff, Peter",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5d838c31-6dcf-44a6-b16b-4d19d770c36b",
			"shortName": "Pieperhoff, P."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"id": "871e077b-fb0b-4579-9e69-713da04d217b",
	"kgReference": [
		"10.25493/A0EQ-WKE"
	],
	"publications": [
		{
			"name": "Julich-Brain: A 3D probabilistic atlas of the human brain\u2019s cytoarchitecture",
			"cite": "Amunts, K., Mohlberg, H., Bludau, S., Zilles, K. (2020). Julich-Brain \u2013 A 3D probabilistic atlas of human brain\u2019s cytoarchitecture. Science 369, 988-992",
			"doi": "10.1126/science.abb4588"
		}
	]
}
