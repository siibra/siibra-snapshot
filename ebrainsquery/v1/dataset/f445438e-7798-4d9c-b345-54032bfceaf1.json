{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area OP8 (Frontal Operculum) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently, the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces where each voxel is assigned with the probability to belong to Area OP8 (Frontal Operculum). The probability map of Area OP8 (Frontal Operculum) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area OP8 (Frontal Operculum):\nSaal et al. (2019) [Data set, v5.0] [DOI: 10.25493/1NGE-YH3](https://doi.org/10.25493%2F1NGE-YH3)\nSaal et al. (2019) [Data set, v5.1] [DOI: 10.25493/NGF8-TA4](https://doi.org/10.25493%2FNGF8-TA4)\n\nThe most probable delineation of Area OP8 (Frontal Operculum) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area OP8 (Frontal Operculum) ",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/6414e7b7-9b5a-401d-9e8c-4f090e2939ea"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area OP8 (Frontal Operculum) (v6.0)",
	"files": [
		{
			"byteSize": 20.0,
			"hashcode": "8be4ce1dd8b47d8d04e8ba0a431e98a5",
			"name": "subjects_Area-OP8.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP8_pub/6.0/subjects_Area-OP8.csv",
			"contentType": "text/csv",
			"hash": "d4fd357f7807fd448ba3bc2240f5df20"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "35f81ca1521584cb3a1a3e85cb80b85a",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP8_pub/6.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 66949.0,
			"hashcode": "eb9469c9defa6439645d61504e74d20d",
			"name": "Area-OP8_l_N11_nlin2ICBM152asym2009c_6.0_publicDOI_5cd6459b49d2b41981e09d2b3596767d.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP8_pub/6.0/Area-OP8_l_N11_nlin2ICBM152asym2009c_6.0_publicDOI_5cd6459b49d2b41981e09d2b3596767d.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "5cd6459b49d2b41981e09d2b3596767d"
		},
		{
			"byteSize": 92346.0,
			"hashcode": "b954ee5bc3687cc56308922b5cdddd24",
			"name": "Area-OP8_l_N11_nlin2Stdcolin27_6.0_publicDOI_183e5e6d839497f56742c5a21cec27fb.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP8_pub/6.0/Area-OP8_l_N11_nlin2Stdcolin27_6.0_publicDOI_183e5e6d839497f56742c5a21cec27fb.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "183e5e6d839497f56742c5a21cec27fb"
		},
		{
			"byteSize": 99563.0,
			"hashcode": "78824f583d22cf3346cd288fbb3435dd",
			"name": "Area-OP8_r_N11_nlin2Stdcolin27_6.0_publicDOI_81b7a048e2a47e7e348414ba7819628a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP8_pub/6.0/Area-OP8_r_N11_nlin2Stdcolin27_6.0_publicDOI_81b7a048e2a47e7e348414ba7819628a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "81b7a048e2a47e7e348414ba7819628a"
		},
		{
			"byteSize": 76833.0,
			"hashcode": "12d26e522fbd7576433878a4fbcd90e8",
			"name": "Area-OP8_r_N11_nlin2ICBM152asym2009c_6.0_publicDOI_1c280b88b61a5a904c97f4d86a43dbfe.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-OP8_pub/6.0/Area-OP8_r_N11_nlin2ICBM152asym2009c_6.0_publicDOI_1c280b88b61a5a904c97f4d86a43dbfe.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "1c280b88b61a5a904c97f4d86a43dbfe"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/f445438e-7798-4d9c-b345-54032bfceaf1",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "f3fe180f-4bf4-457f-8f66-0e23e1e2b84a303708",
			"name": "Saal, Martin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f3fe180f-4bf4-457f-8f66-0e23e1e2b84a",
			"shortName": "Saal, M."
		}
	],
	"id": "f445438e-7798-4d9c-b345-54032bfceaf1",
	"kgReference": [
		"10.25493/C0M6-NSY"
	],
	"publications": []
}
