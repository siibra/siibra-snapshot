{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Zachlod, D., R\u00fcttgers, B., Bludau, S., Mohlberg, H., Langner, R., Zilles, K., Amunts, K. (2020) Four new cytoarchitectonic areas surrounding the primary and early auditory cortex in human brains. Cortex, 128:1-29",
			"doi": "10.1016/j.cortex.2020.02.021"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area TeI (STG) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area TeI (STG). The probability map of Area TeI (STG) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nThe most probable delineation of Area TeI (STG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TeI (STG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/0eff5159-a136-4b42-b591-f17fbc82ae8a"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area TeI (STG) (v5.1)",
	"files": [
		{
			"byteSize": 72098.0,
			"hashcode": "fd8613c27e629d4dbaa6b7072e67e1a6",
			"name": "Area-TI1_r_N10_nlin2MNI152ASYM2009C_5.1_publicDOI_84d7716eb0afee93bf8ff8625361c4d8.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TI1_pub/5.1/Area-TI1_r_N10_nlin2MNI152ASYM2009C_5.1_publicDOI_84d7716eb0afee93bf8ff8625361c4d8.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "84d7716eb0afee93bf8ff8625361c4d8"
		},
		{
			"byteSize": 77092.0,
			"hashcode": "c84d4ec01613e076e20dc8881d6aadff",
			"name": "Area-TI1_l_N10_nlin2MNI152ASYM2009C_5.1_publicDOI_d0163a91613b36333d2f7113762b49ef.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TI1_pub/5.1/Area-TI1_l_N10_nlin2MNI152ASYM2009C_5.1_publicDOI_d0163a91613b36333d2f7113762b49ef.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "d0163a91613b36333d2f7113762b49ef"
		},
		{
			"byteSize": 21.0,
			"hashcode": "5550c44e04c78ea29bec77b74fca8842",
			"name": "subjects_Area-TI1.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TI1_pub/5.1/subjects_Area-TI1.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		},
		{
			"byteSize": 97495.0,
			"hashcode": "dbc3685383bba361af6c44131df1095b",
			"name": "Area-TI1_l_N10_nlin2Stdcolin27_5.1_publicDOI_cd40b4fa9fd491879a4f6eddfcd5ecc3.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TI1_pub/5.1/Area-TI1_l_N10_nlin2Stdcolin27_5.1_publicDOI_cd40b4fa9fd491879a4f6eddfcd5ecc3.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "cd40b4fa9fd491879a4f6eddfcd5ecc3"
		},
		{
			"byteSize": 96061.0,
			"hashcode": "5efa45fe84b2da573bda9e858d1ed484",
			"name": "Area-TI1_r_N10_nlin2Stdcolin27_5.1_publicDOI_390febadfb960f019db219f2017726de.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TI1_pub/5.1/Area-TI1_r_N10_nlin2Stdcolin27_5.1_publicDOI_390febadfb960f019db219f2017726de.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "390febadfb960f019db219f2017726de"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/35f2107d-2e3c-41ae-b26a-83b101829346",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "adb9bc50-26e7-4823-b313-d2f43128c919",
			"name": "R\u00fcttgers, Britta",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/adb9bc50-26e7-4823-b313-d2f43128c919",
			"shortName": "R\u00fcttgers, B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "fbec7907-0560-41cb-bec6-bf376991f02b303708",
			"name": "Zachlod, Daniel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/fbec7907-0560-41cb-bec6-bf376991f02b",
			"shortName": "Zachlod, D."
		}
	],
	"id": "35f2107d-2e3c-41ae-b26a-83b101829346",
	"kgReference": [
		"10.25493/DTC3-EVM"
	],
	"publications": [
		{
			"name": "Four new cytoarchitectonic areas surrounding the primary and early auditory cortex in human brains ",
			"cite": "Zachlod, D., R\u00fcttgers, B., Bludau, S., Mohlberg, H., Langner, R., Zilles, K., Amunts, K. (2020) Four new cytoarchitectonic areas surrounding the primary and early auditory cortex in human brains. Cortex, 128:1-29",
			"doi": "10.1016/j.cortex.2020.02.021"
		}
	]
}
