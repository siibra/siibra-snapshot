{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area Ig3 (Insula) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong toArea Ig3 (Insula). The probability map of Area Ig3 (Insula) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.\n\nOther available data versions of Area Ig3 (Insula):\nQuabs et al. (2019) [Data set, v11.0] [DOI: 10.25493/2FTZ-MWQ](https://doi.org/10.25493/10.25493/2FTZ-MWQ)\n\n\nThe most probable delineation of Area Ig3 (Insula) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493/8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Ig3 (Insula)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/10dba769-4f6c-40f9-8ffd-e0cce71c5adb"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Ig3 (Insula) (v4.0)",
	"files": [
		{
			"byteSize": 47020.0,
			"hashcode": "b0f89a625284c1c62c5aaf05ac1c986e",
			"name": "Area-Ig3_l_N14_nlin2ICBM152asym2009c_4.0_publicDOI_b202522b94697838209f8bacfefb77c6.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig3_pub/4.0/Area-Ig3_l_N14_nlin2ICBM152asym2009c_4.0_publicDOI_b202522b94697838209f8bacfefb77c6.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "b202522b94697838209f8bacfefb77c6"
		},
		{
			"byteSize": 74588.0,
			"hashcode": "d7abde285111483a551758a2ec531149",
			"name": "Area-Ig3_l_N14_nlin2Stdcolin27_4.0_publicDOI_b35cab237dcb8101f33624af2231a7b8.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig3_pub/4.0/Area-Ig3_l_N14_nlin2Stdcolin27_4.0_publicDOI_b35cab237dcb8101f33624af2231a7b8.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "b35cab237dcb8101f33624af2231a7b8"
		},
		{
			"byteSize": 24.0,
			"hashcode": "3f5d5488681542009baf481bc7a5826f",
			"name": "subjects_Area-Ig3.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig3_pub/4.0/subjects_Area-Ig3.csv",
			"contentType": "text/csv",
			"hash": "5f65aff2b50cea7b07f7c426a59762b3"
		},
		{
			"byteSize": 49103.0,
			"hashcode": "6f6bf3493b948dfd6f5ffbf2211365ef",
			"name": "Area-Ig3_r_N14_nlin2ICBM152asym2009c_4.0_publicDOI_b67c0672aeb76f76d0820e5c7ec46507.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig3_pub/4.0/Area-Ig3_r_N14_nlin2ICBM152asym2009c_4.0_publicDOI_b67c0672aeb76f76d0820e5c7ec46507.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "b67c0672aeb76f76d0820e5c7ec46507"
		},
		{
			"byteSize": 76421.0,
			"hashcode": "448dc72352fceb960138acbe247afc74",
			"name": "Area-Ig3_r_N14_nlin2Stdcolin27_4.0_publicDOI_fdc63791109807416a2d045a746fe8fe.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig3_pub/4.0/Area-Ig3_r_N14_nlin2Stdcolin27_4.0_publicDOI_fdc63791109807416a2d045a746fe8fe.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "fdc63791109807416a2d045a746fe8fe"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "03f9b0ff3be0174133a158db0edacf7b",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Ig3_pub/4.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/369d1df4-f62b-4069-a073-f4a803a5ec13",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"name": "Quabs, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ab366b3-9f9e-44ad-8cc5-aef080c7fb18",
			"shortName": "Quabs, J."
		}
	],
	"id": "369d1df4-f62b-4069-a073-f4a803a5ec13",
	"kgReference": [
		"10.25493/AMBK-R1T"
	],
	"publications": []
}
