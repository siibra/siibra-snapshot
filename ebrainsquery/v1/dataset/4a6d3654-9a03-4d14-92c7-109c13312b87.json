{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Schleicher, A., Amunts, K., Geyer, S., Morosan, P., & Zilles, K. (1999). Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics. NeuroImage, 9(1), 165\u2013177. ",
			"doi": "10.1006/nimg.1998.0385"
		},
		{
			"cite": "Morosan, P., Schleicher, A., Amunts, K., & Zilles, K. (2005). Multimodal architectonic mapping of human superior temporal gyrus. Anatomy and Embryology, 210(5-6), 401\u2013406. ",
			"doi": "10.1007/s00429-005-0029-1"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area TE 3 (STG) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area TE 3 (STG). The probability map of Area TE 3 (STG) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area TE 3 (STG):\nMorosan et al. (2018) [Data set, v5.0] [DOI: 10.25493/V09X-3EW](https://doi.org/10.25493%2FV09X-3EW)\nMorosan et al. (2019) [Data set, v5.1] [DOI: 10.25493/BN5J-JT8](https://doi.org/10.25493%2FBN5J-JT8)\n\nThe most probable delineation of Area TE 3 (STG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area TE 3 (STG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/7e1a3291-efdc-4ca6-a3d0-6c496c34639f"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area TE 3 (STG) (v6.0)",
	"files": [
		{
			"byteSize": 21.0,
			"hashcode": "0adc7dfdd60bc0bcb492b9e48a95d7e7",
			"name": "subjects_Area-TE-3.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/6.0/subjects_Area-TE-3.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		},
		{
			"byteSize": 111933.0,
			"hashcode": "9ae7fb7456aaf3007e19fc0a30894549",
			"name": "Area-TE-3_l_N10_nlin2ICBM152asym2009c_6.0_publicP_7fca55b37cb81442458988112cb6b680.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/6.0/Area-TE-3_l_N10_nlin2ICBM152asym2009c_6.0_publicP_7fca55b37cb81442458988112cb6b680.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "7fca55b37cb81442458988112cb6b680"
		},
		{
			"byteSize": 109011.0,
			"hashcode": "0721148e3169e1c2bcd7f11d7574cf68",
			"name": "Area-TE-3_r_N10_nlin2ICBM152asym2009c_6.0_publicP_22510989d8acfb54dbcb5d273d708b48.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/6.0/Area-TE-3_r_N10_nlin2ICBM152asym2009c_6.0_publicP_22510989d8acfb54dbcb5d273d708b48.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "22510989d8acfb54dbcb5d273d708b48"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "32571cee3114167621de008cf3bd4405",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/6.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 139350.0,
			"hashcode": "7363073d5d5ee05106bb48cb6c23a345",
			"name": "Area-TE-3_r_N10_nlin2Stdcolin27_6.0_publicP_2e726aaa058225e490ff9a8706948661.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/6.0/Area-TE-3_r_N10_nlin2Stdcolin27_6.0_publicP_2e726aaa058225e490ff9a8706948661.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "2e726aaa058225e490ff9a8706948661"
		},
		{
			"byteSize": 136205.0,
			"hashcode": "dd3a60bf155144ba1a23c9cb12bd6924",
			"name": "Area-TE-3_l_N10_nlin2Stdcolin27_6.0_publicP_622793d1e84d588106adab013bfb23a2.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-TE-3_pub/6.0/Area-TE-3_l_N10_nlin2Stdcolin27_6.0_publicP_622793d1e84d588106adab013bfb23a2.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "622793d1e84d588106adab013bfb23a2"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/4a6d3654-9a03-4d14-92c7-109c13312b87",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "183ac28a5978832f25623307522cba98",
			"name": "Geyer, Stefan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a38f341-5b88-4cb2-b8b7-6473a676d892",
			"shortName": "Geyer, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "13418f8079344b85dc89c232c5750f82",
			"name": "Morosan, Patricia",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a95a49b-5c22-47fc-8bc8-ff94b61aa3af",
			"shortName": "Morosan, P."
		}
	],
	"id": "4a6d3654-9a03-4d14-92c7-109c13312b87",
	"kgReference": [
		"10.25493/1WAY-M3E"
	],
	"publications": [
		{
			"name": "Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics",
			"cite": "Schleicher, A., Amunts, K., Geyer, S., Morosan, P., & Zilles, K. (1999). Observer-Independent Method for Microstructural Parcellation of Cerebral Cortex: A Quantitative Approach to Cytoarchitectonics. NeuroImage, 9(1), 165\u2013177. ",
			"doi": "10.1006/nimg.1998.0385"
		},
		{
			"name": "Multimodal architectonic mapping of human superior temporal gyrus",
			"cite": "Morosan, P., Schleicher, A., Amunts, K., & Zilles, K. (2005). Multimodal architectonic mapping of human superior temporal gyrus. Anatomy and Embryology, 210(5-6), 401\u2013406. ",
			"doi": "10.1007/s00429-005-0029-1"
		}
	]
}
