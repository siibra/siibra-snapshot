{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Amunts, K., Malikovic, A., Mohlberg, H., Schormann, T., & Zilles, K. (2000). Brodmann\u2019s Areas 17 and 18 Brought into Stereotaxic Space\u2014Where and How Variable? NeuroImage, 11(1), 66\u201384. ",
			"doi": "10.1006/nimg.1999.0516"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area hOc2 (V2, 18) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area hOc2 (V2, 18). The probability map of Area hOc2 (V2, 18) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area hOc2 (V2, 18):\nAmunts et al. (2018) [Data set, v2.2] [DOI: 10.25493/FKMC-ZX7](https://doi.org/10.25493%2FFKMC-ZX7)\n\nThe most probable delineation of Area hOc2 (V2, 18) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hOc2 (V2, 18)",
			"alias": "Area hOc2 (V2, 18)",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/04674a3c-bb3a-495e-a466-206355e630bd"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area hOc2 (V2, 18) (v2.4)",
	"files": [
		{
			"byteSize": 224084.0,
			"hashcode": "4ac01ff497d91aacbe72590c2f869ac7",
			"name": "Area-hOc2_l_N10_nlin2MNI152ASYM2009C_2.4_publicP_df81c453f3cf5b8092f52723b348b1e9.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.4/Area-hOc2_l_N10_nlin2MNI152ASYM2009C_2.4_publicP_df81c453f3cf5b8092f52723b348b1e9.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "df81c453f3cf5b8092f52723b348b1e9"
		},
		{
			"byteSize": 20.0,
			"hashcode": "141be32cba18f7d26866fce6a1056bbe",
			"name": "subjects_Area-hOc2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.4/subjects_Area-hOc2.csv",
			"contentType": "text/csv",
			"hash": "d4fd357f7807fd448ba3bc2240f5df20"
		},
		{
			"byteSize": 248894.0,
			"hashcode": "0b5118e175150a786fabc7a1b4743e69",
			"name": "Area-hOc2_l_N10_nlin2Stdcolin27_2.4_publicP_107c22563c6a2e866be63e0fa46dc438.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.4/Area-hOc2_l_N10_nlin2Stdcolin27_2.4_publicP_107c22563c6a2e866be63e0fa46dc438.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "107c22563c6a2e866be63e0fa46dc438"
		},
		{
			"byteSize": 228295.0,
			"hashcode": "17a1bba1a7d8666c0d0432f0b9da7807",
			"name": "Area-hOc2_r_N10_nlin2MNI152ASYM2009C_2.4_publicP_ce802616d4367d2869dd4bbd3f11f1cb.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.4/Area-hOc2_r_N10_nlin2MNI152ASYM2009C_2.4_publicP_ce802616d4367d2869dd4bbd3f11f1cb.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "ce802616d4367d2869dd4bbd3f11f1cb"
		},
		{
			"byteSize": 231046.0,
			"hashcode": "bdf8f6931221608ec372793c48be072a",
			"name": "Area-hOc2_r_N10_nlin2Stdcolin27_2.4_publicP_a06dd5eab4720f7e81c5940c36f9af41.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-hOc2_pub/2.4/Area-hOc2_r_N10_nlin2Stdcolin27_2.4_publicP_a06dd5eab4720f7e81c5940c36f9af41.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "a06dd5eab4720f7e81c5940c36f9af41"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/fa2af93e-c0bb-4c76-bdfe-52776a37d0c0",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "0b6888c55d248a941a022961b3de4374",
			"name": "Malikovic, Aleksandar",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8267c403-6f87-4c77-b113-3bd50d848e31",
			"shortName": "Malikovic, A."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"id": "fa2af93e-c0bb-4c76-bdfe-52776a37d0c0",
	"kgReference": [
		"10.25493/QG9C-THD"
	],
	"publications": [
		{
			"name": "Brodmann's Areas 17 and 18 Brought into Stereotaxic Space\u2014Where and How Variable?",
			"cite": "Amunts, K., Malikovic, A., Mohlberg, H., Schormann, T., & Zilles, K. (2000). Brodmann\u2019s Areas 17 and 18 Brought into Stereotaxic Space\u2014Where and How Variable? NeuroImage, 11(1), 66\u201384. ",
			"doi": "10.1006/nimg.1999.0516"
		}
	]
}
