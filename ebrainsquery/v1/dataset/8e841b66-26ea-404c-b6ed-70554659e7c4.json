{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Caspers, J., Zilles, K., Eickhoff, S. B., Schleicher, A., Mohlberg, H., & Amunts, K. (2012). Cytoarchitectonical analysis and probabilistic mapping of two extrastriate areas of the human posterior fusiform gyrus. Brain Structure and Function, 218(2), 511\u2013526. ",
			"doi": "10.1007/s00429-012-0411-8"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area FG2 (FusG) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area FG2 (FusG). The probability map of Area FG2 (FusG) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area FG2 (FusG):\nCaspers et al. (2018) [Data set, v1.2] [DOI: 10.25493/X835-SK7](https://doi.org/10.25493%2FX835-SK7)\nCaspers et al. (2019) [Data set, v1.4] [DOI: 10.25493/F2JH-KVV](https://doi.org/10.25493%2FF2JH-KVV)\nCaspers et al. (2020) [Data set, v3.0] [DOI: 10.25493/FTP2-SXV](https://doi.org/10.25493%2FFTP2-SXV)\n\nThe most probable delineation of Area FG2 (FusG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area FG2 (FusG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/6e7a0441-4baa-4355-921b-50d23d07d50f"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area FG2 (FusG) (v3.2)",
	"files": [
		{
			"byteSize": 93156.0,
			"hashcode": "aa62e14fa90b7ec9be880576dcf941b4",
			"name": "Area-FG2_r_N10_nlin2ICBM152asym2009c_3.2_publicP_94670fbc48636dc9e22a5a53efec34b2.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG2_pub/3.2/Area-FG2_r_N10_nlin2ICBM152asym2009c_3.2_publicP_94670fbc48636dc9e22a5a53efec34b2.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "0635c26d39035225c47693fb674ee4b8"
		},
		{
			"byteSize": 20.0,
			"hashcode": "d496e0c765fcb2fad19891d44808d4c3",
			"name": "subjects_Area-FG2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG2_pub/3.2/subjects_Area-FG2.csv",
			"contentType": "text/csv",
			"hash": "d4fd357f7807fd448ba3bc2240f5df20"
		},
		{
			"byteSize": 97571.0,
			"hashcode": "899d14f3476f559e039d34620734fdd6",
			"name": "Area-FG2_l_N10_nlin2ICBM152asym2009c_3.2_publicP_e9a924f7953e80cc3bfb2a1be2b8ef5a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG2_pub/3.2/Area-FG2_l_N10_nlin2ICBM152asym2009c_3.2_publicP_e9a924f7953e80cc3bfb2a1be2b8ef5a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "27f78d9ccf553110978754a8a8f28742"
		},
		{
			"byteSize": 113853.0,
			"hashcode": "a77322d10197e0105a8eccb8fac94c67",
			"name": "Area-FG2_l_N10_nlin2Stdcolin27_3.2_publicP_e9a924f7953e80cc3bfb2a1be2b8ef5a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG2_pub/3.2/Area-FG2_l_N10_nlin2Stdcolin27_3.2_publicP_e9a924f7953e80cc3bfb2a1be2b8ef5a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "e9a924f7953e80cc3bfb2a1be2b8ef5a"
		},
		{
			"byteSize": 110700.0,
			"hashcode": "bcb859d35d574b296b1adeaae14bb289",
			"name": "Area-FG2_r_N10_nlin2Stdcolin27_3.2_publicP_94670fbc48636dc9e22a5a53efec34b2.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG2_pub/3.2/Area-FG2_r_N10_nlin2Stdcolin27_3.2_publicP_94670fbc48636dc9e22a5a53efec34b2.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "94670fbc48636dc9e22a5a53efec34b2"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "2893f7608ec7db6a935730126c0af02c",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-FG2_pub/3.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/8e841b66-26ea-404c-b6ed-70554659e7c4",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "21ec0b4ae3f94c4d7e701862718f2ae2",
			"name": "Caspers, Julian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c62b7a14-880d-45ec-8e82-967cd3626dd5",
			"shortName": "Caspers, J."
		}
	],
	"id": "8e841b66-26ea-404c-b6ed-70554659e7c4",
	"kgReference": [
		"10.25493/N7ZP-17X"
	],
	"publications": [
		{
			"name": "Cytoarchitectonical analysis and probabilistic mapping of two extrastriate areas of the human posterior fusiform gyrus",
			"cite": "Caspers, J., Zilles, K., Eickhoff, S. B., Schleicher, A., Mohlberg, H., & Amunts, K. (2012). Cytoarchitectonical analysis and probabilistic mapping of two extrastriate areas of the human posterior fusiform gyrus. Brain Structure and Function, 218(2), 511\u2013526. ",
			"doi": "10.1007/s00429-012-0411-8"
		}
	]
}
