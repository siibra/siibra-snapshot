{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"cytoarchitectonic mapping",
		"probability mapping",
		"maximum probability mapping",
		"magnetic resonance imaging (MRI)"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct probabilistic cytoarchitectonic map of Area STS1 (STS) in the individual, single subject template of the MNI Colin 27 reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using classical histological criteria and quantitative cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to the reference space, where each voxel was assigned the probability to belong to Area STS1 (STS). The probability map of Area STS1 (STS) is provided in NifTi format for each hemisphere in the reference space. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and updated probability estimates for new brain structures may in some cases lead to measurable but negligible deviations of existing probability maps, as compared to earlier released datasets.\n\nOther available data versions of Area STS1 (STS):\nZachlod et al. (2019) [Data set, v3.1] [DOI: 10.25493/F6DF-H8P](https://doi.org/10.25493%2FF6DF-H8P)\n\nThe most probable delineation of Area STS1 (STS) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area STS1 (STS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/68784b66-ff15-4b09-b28a-a2146c0f8907"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area STS1 (STS) (v3.0)",
	"files": [
		{
			"byteSize": 124829.0,
			"hashcode": "4f9005a88a2a0fec564a31cdae7fe3d2",
			"name": "Area-STS1_r_N10_nlin2Stdcolin27_3.0_publicDOI_9869472fd4754fd4dbdd4575fd834dec.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS1_pub/3.0/Area-STS1_r_N10_nlin2Stdcolin27_3.0_publicDOI_9869472fd4754fd4dbdd4575fd834dec.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "9869472fd4754fd4dbdd4575fd834dec"
		},
		{
			"byteSize": 113233.0,
			"hashcode": "4d6018c9eee3c9866418fdb0d33f10e1",
			"name": "Area-STS1_r_N10_nlin2ICBM152ASYM2009C_3.0_publicDOI_d74f0d8ab3850914fbc1e3fad606d90a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS1_pub/3.0/Area-STS1_r_N10_nlin2ICBM152ASYM2009C_3.0_publicDOI_d74f0d8ab3850914fbc1e3fad606d90a.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "d74f0d8ab3850914fbc1e3fad606d90a"
		},
		{
			"byteSize": 123860.0,
			"hashcode": "c5668ded6aa81f15fec8becc4ec7c054",
			"name": "Area-STS1_l_N10_nlin2Stdcolin27_3.0_publicDOI_bd1ec136ca365186144bbec33209017d.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS1_pub/3.0/Area-STS1_l_N10_nlin2Stdcolin27_3.0_publicDOI_bd1ec136ca365186144bbec33209017d.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "bd1ec136ca365186144bbec33209017d"
		},
		{
			"byteSize": 108692.0,
			"hashcode": "7fba5cf4b3a1ded2c12a313e74345015",
			"name": "Area-STS1_l_N10_nlin2ICBM152ASYM2009C_3.0_publicDOI_8a0e4be394bcba111332d12a76941e42.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS1_pub/3.0/Area-STS1_l_N10_nlin2ICBM152ASYM2009C_3.0_publicDOI_8a0e4be394bcba111332d12a76941e42.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "8a0e4be394bcba111332d12a76941e42"
		},
		{
			"byteSize": 21.0,
			"hashcode": "6ec4e2e7302db9491f6541d4d1bc7d48",
			"name": "subjects_Area-STS1.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-STS1_pub/3.0/subjects_Area-STS1.csv",
			"contentType": "text/csv",
			"hash": "fb480b89393efc54aac75f0c4c830906"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/9d084c62-4bf9-4f6a-8824-ddbe17944dda",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "fbec7907-0560-41cb-bec6-bf376991f02b303708",
			"name": "Zachlod, Daniel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/fbec7907-0560-41cb-bec6-bf376991f02b",
			"shortName": "Zachlod, D."
		}
	],
	"id": "9d084c62-4bf9-4f6a-8824-ddbe17944dda",
	"kgReference": [
		"10.25493/2G11-1WA"
	],
	"publications": []
}
