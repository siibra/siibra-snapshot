{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Scheperjans, F., Eickhoff, S. B., Hoemke, L., Mohlberg, H., Hermann, K., Amunts, K., & Zilles, K. (2008). Probabilistic Maps, Morphometry, and Variability of Cytoarchitectonic Areas in the Human Superior Parietal Cortex. Cerebral Cortex, 18(9), 2141\u20132157. ",
			"doi": "10.1093/cercor/bhm241"
		},
		{
			"cite": "Scheperjans, F., Hermann, K., Eickhoff, S. B., Amunts, K., Schleicher, A., & Zilles, K. (2007). Observer-Independent Cytoarchitectonic Mapping of the Human Superior Parietal Cortex. Cerebral Cortex, 18(4), 846\u2013867. ",
			"doi": "10.1093/cercor/bhm116"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area 7M (SPL) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area 7M (SPL). The probability map of Area 7M (SPL) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area 7M (SPL):\nScheperjans et al. (2018) [Data set, v8.2] [DOI: 10.25493/F26Z-16P](https://doi.org/10.25493%2FF26Z-16P)\nScheperjans et al. (2019) [Data set, v8.4] [DOI: 10.25493/F25F-EKW](https://doi.org/10.25493%2FF25F-EKW)\nScheperjans et al. (2020) [Data set, v9.0] [DOI: 10.25493/7ZC6-KW5]()https://doi.org/10.25493%2F7ZC6-KW5\n\nThe most probable delineation of Area 7M (SPL) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 7M (SPL)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/0aacea5c-bc9e-483f-8376-25f176ada158"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area 7M (SPL) (v9.2)",
	"files": [
		{
			"byteSize": 77446.0,
			"hashcode": "131524065e0128f139faf7f90c7e5930",
			"name": "Area-7M_r_N10_nlin2Stdcolin27_9.2_publicP_10742519d7e253fe9b8a130c49ef8c67.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7M_pub/9.2/Area-7M_r_N10_nlin2Stdcolin27_9.2_publicP_10742519d7e253fe9b8a130c49ef8c67.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "10742519d7e253fe9b8a130c49ef8c67"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "ee2cade601a2103bd19f7cb3371e3aee",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7M_pub/9.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 52304.0,
			"hashcode": "c77bf374eddd6b36692d5e3006b2cc5a",
			"name": "Area-7M_r_N10_nlin2ICBM152asym2009c_9.2_publicP_10742519d7e253fe9b8a130c49ef8c67.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7M_pub/9.2/Area-7M_r_N10_nlin2ICBM152asym2009c_9.2_publicP_10742519d7e253fe9b8a130c49ef8c67.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "fe10eac99c6dfd45e4dc14f744f66e2d"
		},
		{
			"byteSize": 81216.0,
			"hashcode": "e4a026a5199aa315c0a50ae426a81d51",
			"name": "Area-7M_l_N10_nlin2Stdcolin27_9.2_publicP_4223c3ca06ffe0ff983f3aace83bc8c9.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7M_pub/9.2/Area-7M_l_N10_nlin2Stdcolin27_9.2_publicP_4223c3ca06ffe0ff983f3aace83bc8c9.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4223c3ca06ffe0ff983f3aace83bc8c9"
		},
		{
			"byteSize": 56633.0,
			"hashcode": "bea2df13acb02a3e14e7c90192f1d934",
			"name": "Area-7M_l_N10_nlin2ICBM152asym2009c_9.2_publicP_4223c3ca06ffe0ff983f3aace83bc8c9.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7M_pub/9.2/Area-7M_l_N10_nlin2ICBM152asym2009c_9.2_publicP_4223c3ca06ffe0ff983f3aace83bc8c9.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "725e846362689efbc6407852fbf3ddd3"
		},
		{
			"byteSize": 23.0,
			"hashcode": "e6de6d77f90169cc52e037b13d33e6e8",
			"name": "subjects_Area-7M.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-7M_pub/9.2/subjects_Area-7M.csv",
			"contentType": "text/csv",
			"hash": "499f1e375313a21192c78e14fe30bfc9"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/f3ab07b9-7a06-4321-8c26-59328f9e77e9",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": null,
			"identifier": "274457d14fdc8db443d4765a3ac061ba",
			"name": "Hoemke, L.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/323aa606-da3a-42a1-b16f-e07887dc43ec",
			"shortName": "Hoemke, L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "46f600ef63191d2b80362caa4686a2ec",
			"name": "Schleicher, Axel",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/f000487e-a9b4-4dee-8a2d-68f71019af0b",
			"shortName": "Schleicher, A."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "f6062e66693aad8bce7a1412ab016a73",
			"name": "Hermann, K.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/58065272-f536-4e39-90e5-77f7bf034e0c",
			"shortName": "Hermann, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7be384119cf6b1bfbff17cdda3476f2e",
			"name": "Scheperjans, Filip",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/eac78bec-6b4c-4652-bea0-0e927cd558b2",
			"shortName": "Scheperjans, F."
		}
	],
	"id": "f3ab07b9-7a06-4321-8c26-59328f9e77e9",
	"kgReference": [
		"10.25493/EYGK-PBG"
	],
	"publications": [
		{
			"name": "Probabilistic Maps, Morphometry, and Variability of Cytoarchitectonic Areas in the Human Superior Parietal Cortex",
			"cite": "Scheperjans, F., Eickhoff, S. B., Hoemke, L., Mohlberg, H., Hermann, K., Amunts, K., & Zilles, K. (2008). Probabilistic Maps, Morphometry, and Variability of Cytoarchitectonic Areas in the Human Superior Parietal Cortex. Cerebral Cortex, 18(9), 2141\u20132157. ",
			"doi": "10.1093/cercor/bhm241"
		},
		{
			"name": "Observer-Independent Cytoarchitectonic Mapping of the Human Superior Parietal Cortex",
			"cite": "Scheperjans, F., Hermann, K., Eickhoff, S. B., Amunts, K., Schleicher, A., & Zilles, K. (2007). Observer-Independent Cytoarchitectonic Mapping of the Human Superior Parietal Cortex. Cerebral Cortex, 18(4), 846\u2013867. ",
			"doi": "10.1093/cercor/bhm116"
		}
	]
}
