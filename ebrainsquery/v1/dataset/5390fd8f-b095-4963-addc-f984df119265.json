{
	"formats": [],
	"datasetDOI": [
		{
			"cite": "Demiraj A, Karozos K, Spartalis I, and Vassalos V. 2019. Meta-data management and quality control for the medical informatics platform. In Proceedings of the 23rd International Database Applications & Engineering Symposium ( IDEAS \u201919 ). Association for Computing Machinery, New York, NY, USA, Article 10, 1\u20139.",
			"doi": "https://doi.org/10.1145/3331076.3331088"
		}
	],
	"activity": [
		{
			"protocols": [
				"Reconstruction"
			],
			"preparation": []
		}
	],
	"referenceSpaces": [],
	"methods": [
		"metadata parsing",
		"analysis method"
	],
	"custodians": [
		{
			"schema.org/shortName": null,
			"identifier": "8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"name": "Vassalos, Vasilis",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"shortName": "Vassalos, V."
		}
	],
	"project": [
		"The Medical Informatics Platform (MIP)"
	],
	"description": "Data in the Medical Informatics Platform (MIP) resides in hospital servers and never leaves the hospital. Analyses and experiments are executed with respect to that principle while preserving patients\u2019 anonymity making it infeasible for their identity to be inferred. \nHospitals importing their data to the Medical Informatics Platform join a federation so as to run analyses on data from other hospital nodes as well. Each federation in the MIP refers to a specific Medical Condition. \nHere the metadata for the federation of hospitals that studies depression is presented.\n\nThis metadata schema, consisting of a total of 147 variables, has the following main variable categories:\n**1. Diagnosis** - 4 variables concerning the diagnosis code in ICD10, the disease duration, the age of the patient and patient control\n**2. Brain Anatomy** - 135 volumetric variables based on a brain atlas. Values have been generated from the brain feature extraction pipeline which uses SPM12\n**3. Scores** - 3 scores for the Montgomery-Asberg Depression Rating Scale as well as its score in and out\n**4. Demographics** - 5 variables for basic demographics information that does not reveal patient\u2019s identity\n\nThe metadata viewing and management is done by Data Catalogue, a central web portal of MIP. Data Catalogue offers presentation, search and hierarchical visualisation of metadata information for datasets imported into the MIP while providing metadata management features to authorized users. One of its features is parsing metadata descriptions in XLSX files and generating their equivalent in a hierarchical JSON format which the MIP uses. In this repository we upload metadata in both XLSX and the generated JSON format.",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-sa/4.0"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [],
	"species": [],
	"name": "Metadata Schema of the Common Data Elements for Depression (v1)",
	"files": [
		{
			"byteSize": 8746.0,
			"hashcode": "e17e019ad666f88cda16574fee2e828f",
			"name": "depression_hospitals.xlsx",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000003_MIPdepression_pub/depression_hospitals.xlsx",
			"contentType": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
			"hash": "589a628e08bbcf71b294f86b2219f91c"
		},
		{
			"byteSize": 198139.0,
			"hashcode": "51ab4b1900eeb6985bebfc34a1b3c8e3",
			"name": "DataDescriptor_depression-metadata.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000003_MIPdepression_pub/DataDescriptor_depression-metadata.pdf",
			"contentType": "application/pdf",
			"hash": "cf8bd384d5f63ae82719dd7bf6b4fbc5"
		},
		{
			"byteSize": 28128.0,
			"hashcode": "237150a41f9859a8e94e53349b822997",
			"name": "depression_cdes_v1.json",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000003_MIPdepression_pub/depression_cdes_v1.json",
			"contentType": "application/json",
			"hash": "e274da93b54319e979e0a55161f0c428"
		},
		{
			"byteSize": 13745.0,
			"hashcode": "5953028d98010138c94f15f7f57f6641",
			"name": "depression_cdes_v1.xlsx",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_7a51266670fe4d718ac2bc3951dd9f64/hbp-d000003_MIPdepression_pub/depression_cdes_v1.xlsx",
			"contentType": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
			"hash": "0c921ef807f5dc952280447deb7788d7"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/5390fd8f-b095-4963-addc-f984df119265",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"name": "Vassalos, Vasilis",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8293db94-40a0-4ca0-82c9-d4a1eeba5361",
			"shortName": "Vassalos, V."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7ddce0b2-ce63-4d4a-b965-60089a749c9a",
			"name": "Spartalis, Iosif",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7ddce0b2-ce63-4d4a-b965-60089a749c9a",
			"shortName": "Spartalis I."
		},
		{
			"schema.org/shortName": null,
			"identifier": "a0fdb87f-89ba-481d-8b99-325e03f53050",
			"name": "Abu-Nawwas, Laith",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a0fdb87f-89ba-481d-8b99-325e03f53050",
			"shortName": "Abu-Nawwas, L."
		},
		{
			"schema.org/shortName": null,
			"identifier": "a0eed0ab-9655-4881-ab69-e069e9f57d49",
			"name": "Karozos, Kostis ",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a0eed0ab-9655-4881-ab69-e069e9f57d49",
			"shortName": "Karozos, K."
		}
	],
	"id": "5390fd8f-b095-4963-addc-f984df119265",
	"kgReference": [
		"10.25493/ABKY-T1Y"
	],
	"publications": [
		{
			"name": "Meta-data management and quality control for the medical informatics platform",
			"cite": "Demiraj A, Karozos K, Spartalis I, and Vassalos V. 2019. Meta-data management and quality control for the medical informatics platform. In Proceedings of the 23rd International Database Applications & Engineering Symposium ( IDEAS \u201919 ). Association for Computing Machinery, New York, NY, USA, Article 10, 1\u20139.",
			"doi": "https://doi.org/10.1145/3331076.3331088"
		}
	]
}
