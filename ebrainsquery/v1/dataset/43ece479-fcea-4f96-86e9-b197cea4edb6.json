{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Wojtasik, M., Bludau, S., Eickhoff, S. B., Mohlberg, H., Gerboga, F., Caspers, S., Amunts, K. (2020) Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex. Frontiers in Neuroanatomy,14(2)",
			"doi": "10.3389/fnana.2020.00002"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Fo6 (OFC) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces where each voxel is assigned with the probability to belong to Area Fo6 (OFC). The probability map of Area Fo6 (OFC) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area Fo6 (OFC):\nWojtasik et al. (2019) [Data set, v2.1] [DOI: 10.25493/34Q4-H62](https://doi.org/10.25493%2F34Q4-H62)\nWojtasik et al. (2020) [Data set, v3.0] [DOI: 10.25493/2J43-HT2](https://doi.org/10.25493%2F2J43-HT2)\n\nThe most probable delineation of Area Fo6 (OFC) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493/8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493/TAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Fo6 (OFC)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/330ae178-557c-4bd0-a932-f138c0a05345"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Fo6 (OFC) (v3.2)",
	"files": [
		{
			"byteSize": 24.0,
			"hashcode": "0b80b36a2f03bd493aaa35438c60ea70",
			"name": "subjects_Area-Fo6.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/3.2/subjects_Area-Fo6.csv",
			"contentType": "text/csv",
			"hash": "06a73e8b0d18633bec1fb222634f8a89"
		},
		{
			"byteSize": 98266.0,
			"hashcode": "01e026f1d743b126b14a1c4794061666",
			"name": "Area-Fo6_r_N10_nlin2Stdcolin27_3.2_publicDOI_6b16e0d96ba620ab4d91a181cf40f86a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/3.2/Area-Fo6_r_N10_nlin2Stdcolin27_3.2_publicDOI_6b16e0d96ba620ab4d91a181cf40f86a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "6b16e0d96ba620ab4d91a181cf40f86a"
		},
		{
			"byteSize": 99754.0,
			"hashcode": "8caa03c1aea4ceebf5da16c821264f9c",
			"name": "Area-Fo6_l_N10_nlin2Stdcolin27_3.2_publicDOI_8125902b3e8519a3b7c9a8d6a2b18b16.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/3.2/Area-Fo6_l_N10_nlin2Stdcolin27_3.2_publicDOI_8125902b3e8519a3b7c9a8d6a2b18b16.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "8125902b3e8519a3b7c9a8d6a2b18b16"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "d7b08616a491c229d468872003c5fac7",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/3.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 74889.0,
			"hashcode": "7e547170ef257b19b395041080ed7e31",
			"name": "Area-Fo6_r_N10_nlin2ICBM152asym2009c_3.2_publicDOI_6b16e0d96ba620ab4d91a181cf40f86a.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/3.2/Area-Fo6_r_N10_nlin2ICBM152asym2009c_3.2_publicDOI_6b16e0d96ba620ab4d91a181cf40f86a.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "e6a8dcadabe9099df23b208486c18199"
		},
		{
			"byteSize": 76309.0,
			"hashcode": "b7ab09005f22bf04d71f4858f984b6a1",
			"name": "Area-Fo6_l_N10_nlin2ICBM152asym2009c_3.2_publicDOI_8125902b3e8519a3b7c9a8d6a2b18b16.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo6_pub/3.2/Area-Fo6_l_N10_nlin2ICBM152asym2009c_3.2_publicDOI_8125902b3e8519a3b7c9a8d6a2b18b16.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "554ac7f445a6c2dac5c84a5904ffe0d0"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/43ece479-fcea-4f96-86e9-b197cea4edb6",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Wojtasik, M.",
			"identifier": "ca2960dc-6ef0-415b-bf89-b047283e7257",
			"name": "Wojtasik, Magdalena",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ca2960dc-6ef0-415b-bf89-b047283e7257",
			"shortName": "Wojtasik, M."
		}
	],
	"id": "43ece479-fcea-4f96-86e9-b197cea4edb6",
	"kgReference": [
		"10.25493/Y9WF-Z5V"
	],
	"publications": [
		{
			"name": "Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex",
			"cite": "Wojtasik, M., Bludau, S., Eickhoff, S. B., Mohlberg, H., Gerboga, F., Caspers, S., Amunts, K. (2020) Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex. Frontiers in Neuroanatomy,14(2)",
			"doi": "10.3389/fnana.2020.00002"
		}
	]
}
