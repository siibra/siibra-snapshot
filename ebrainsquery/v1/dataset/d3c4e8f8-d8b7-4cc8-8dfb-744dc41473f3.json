{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": "BigBrain",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/a1655b99-82f1-420f-a3c2-fe80fd4c8588"
		}
	],
	"methods": [
		"magnetic resonance imaging (MRI)",
		"silver staining",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Ultrahigh resolution 3D maps of cytoarchitectonic areas in the Big Brain model"
	],
	"description": "This dataset contains cytoarchitectonic maps of Area hIP7 (IPS) in the Big Brain dataset [Amunts et al. 2013]. The mappings were created using the semi-automatic method presented in Schleicher et al. 1999, based on coronal histological sections on 1 micron resolution. Mappings are available on approximately every 15-60th section of this region. They were then aligned to the corresponding sections of the 3D reconstructed Big Brain space, using the transformations used in Amunts et al. 2013, kindly provided by Claude Lepage (McGill University).\n\nFrom these delineations, a preliminary 3D map of Area hIP7 (IPS) has been created by simple interpolation of the coronal contours in the 3D anatomical space of the Big Brain. This map gives a first impression of the location of this area in the Big Brain, and can be viewed in the atlas viewer using the URL below.\n\nA full mapping of this area in every histological section using a Deep Learning approach is in progress.\n\n\n**Additional information:**\nThe reference delineations used for this map are part of the work for the corresponding probabilistic map of Area hIP7 (IPS) of the JuBrain Cytoarchitectonic Atlas, published in:\nRichter et al.(2019) [Data set, v7.0] [DOI: 10.25493/QN1F-WAF](https://doi.org/10.25493%2FQN1F-WAF)\nRichter et al. (2019) [Data set, v7.1] [DOI: 10.25493/WRCY-8Z1](https://doi.org/10.25493%2FWRCY-8Z1)\n\nIn addition, the dataset of the probabilistic cytoarchitectonic map of Area hIP7 (IPS) is part of the following research publication:\nRichter, M., Amunts, K., Mohlberg, H., Bludau, S., Eickhoff, S. B., Zilles, K., Caspers, S. (2018). Cytoarchitectonic segregation of human posterior intraparietal and adjacent parieto-occipital sulcus and its relation to visuomotor and cognitive functions. Cerebral Cortex, 29(3), 1305-1327 [DOI: 10.1093/cercor/bhy245](https://doi.org/10.1093%2Fcercor%2Fbhy245%20)\n\n",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hIP7 (IPS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/9c6c3c96-8129-4e0e-aa22-a0fb435aab45"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Interpolated 3D map of Area hIP7 (IPS) in the BigBrain",
	"files": [
		{
			"byteSize": 1363318.0,
			"hashcode": "c1dbf6f84869e59872906995b95bb576",
			"name": "preview_volume_pIPS_hIP7.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-hIP7/volumes/preview_volume_pIPS_hIP7.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "0f6de5128665b8720ede92fa79f50921"
		},
		{
			"byteSize": 94658.0,
			"hashcode": "cd1bdaa90401758c8c4906b6fe76b78f",
			"name": "pIPS_hIP7.zip",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-hIP7/annotations/pIPS_hIP7.zip",
			"contentType": "application/zip",
			"hash": "4cf062317bb3b5abd303821d584cd1ec"
		},
		{
			"byteSize": 133312195.0,
			"hashcode": "77006854076deee104ae932d7cc7b107",
			"name": "volume_pIPS_hIP7.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000005_BigBrainCytoMapping_pub/InterpolatedMaps/Area-hIP7/volumes/volume_pIPS_hIP7.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "8e5dd75178d117268ac73fa478368ed0"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/d3c4e8f8-d8b7-4cc8-8dfb-744dc41473f3",
	"contributors": [
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"name": "Richter, Monika",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ef4f06ad-16df-4edd-8ce9-ab6d34bd9f30",
			"shortName": "Richter, M."
		}
	],
	"id": "d3c4e8f8-d8b7-4cc8-8dfb-744dc41473f3",
	"kgReference": [
		"10.25493/VP8C-5F1"
	],
	"publications": []
}
