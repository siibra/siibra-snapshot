{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Papp, E.A., Leergaard, T.B., Calabrese, E., Johnson, G.A., Bjaalie, J.G., 2015. Addendum to \"Waxholm space atlas of the Sprague Dawley rat brain\" [NeuroImage 97 (2014) 374-386]. Neuroimage 105, 561-2.",
			"doi": "10.1016/j.neuroimage.2014.10.017"
		},
		{
			"cite": "Papp, E.A., Leergaard, T.B., Calabrese, E., Johnson, G.A., Bjaalie, J.G., 2014. Waxholm Space atlas of the Sprague Dawley rat brain. NeuroImage 97, 374-386. ",
			"doi": "10.1016/j.neuroimage.2014.04.001"
		}
	],
	"activity": [
		{
			"protocols": [
				"Structural brain imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"Diffusion-Weighted Magnetic Resonance Imaging (DWI)",
		"Structural Magnetic Resonance Imaging (sMRI)",
		"Diffusion Tensor Imaging (DTI)",
		"Contrast enhanced",
		"Waxholm Space",
		"Stereotaxic",
		"Reference atlas",
		"Neuroinformatic",
		"Template",
		"Digital brain atlas",
		"magnetic resonance imaging (MRI)"
	],
	"custodians": [
		{
			"schema.org/shortName": null,
			"identifier": "c327c58e-5162-45d2-bc1a-6cab422d7912",
			"name": "Johnson, G. Allan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c327c58e-5162-45d2-bc1a-6cab422d7912",
			"shortName": "Johnson, G.A."
		}
	],
	"project": [
		"Waxholm Space atlas of the Sprague Dawley rat brain"
	],
	"description": "Anatomical reference volume of a normal adult male Sprague Dawley rat brain, consisting of high resolution, contrast enhanced, structural magnetic resonance images (sMRI; 39 \u03bcm isotropic voxels) and diffusion tensor images (DTI, 78 \u03bcm isotropic voxels). \n\nVersion 1.01 features a standardized orientation of the volumetric datasets according to NIfTI-1 format defaults, allowing users to read Waxholm Space coordinates directly from the data volume using publicly available software (e.g. ITK-SNAP and MBAT). For details, see \u201cRelease notes v1.01\u201d.\n\nThis dataset is hosted on [NITRC.org](https://www.nitrc.org/frs/?group_id=1081) and includes:\n* [T2*-weighted gradient echo image](https://www.nitrc.org/frs/download.php/9423/WHS_SD_rat_T2star_v1.01.nii.gz) at 39 \u03bcm original resolution (1024x512x512 voxels)\n* [Apparent Diffusion Coefficient (ADC) map](https://www.nitrc.org/frs/download.php/9430/WHS_SD_rat_ADC_v1.01.nii.gz) resampled to 39 \u03bcm resolution (1024x512x512 voxels)\n* [DTI b0 image](https://www.nitrc.org/frs/download.php/9431/WHS_SD_rat_DTI_512_v1.01.zip) resampled to 39 \u03bcm resolution (1024x512x512 voxels)\n* [Diffusion weighted image map](https://www.nitrc.org/frs/download.php/9429/WHS_SD_rat_DWI_v1.01.nii.gz) resampled to 39 \u03bcm resolution (1024x512x512 voxels)\n* [Fractional Anisotropy (FA) map](https://www.nitrc.org/frs/download.php/9428/WHS_SD_rat_FA_v1.01.nii.gz) resampled to 39 \u03bcm resolution (1024x512x512 voxels)\n* [Color FA map](https://www.nitrc.org/frs/download.php/9424/WHS_SD_rat_FA_color_v1.01.nii.gz) resampled to 39 \u03bcm resolution (1024x512x512 voxels), combining the FA and primary eigenvector to 24-bit color RGB where intensity is given by relative FA value and color is assigned to directions (RGB to xyz respectively)\n\n**How to cite:** \nPlease cite these anatomical reference volumes by referring to the Waxholm Space atlas of the Sprague Dawley rat brain RRID: SCR_017124, and cite the publications (see Related publications below).",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-sa/4.0"
		}
	],
	"embargoStatus": [
		{
			"identifier": "203e75c5-7a67-48d8-9264-6588e059d292",
			"name": "Externally hosted",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/203e75c5-7a67-48d8-9264-6588e059d292"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Rat Whole brain (v4)",
			"alias": "Whole brain",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/665ebeab-45dd-41d6-8a98-0614a461634e"
		}
	],
	"species": [
		"Rattus norvegicus"
	],
	"name": "Microscopic resolution diffusion magnetic resonance images of a normal adult Sprague Dawley rat brain",
	"files": [],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/a4a4984f-b1a2-4d28-9c94-fa4767b19c62",
	"contributors": [
		{
			"schema.org/shortName": null,
			"identifier": "cdda3cdc7b3f7ab98830205602f87db3",
			"name": "Bjaalie, Jan G.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/5271206e-7dcd-4081-a49a-bc5f147028ce",
			"shortName": "Bjaalie, J.G."
		},
		{
			"schema.org/shortName": null,
			"identifier": "c327c58e-5162-45d2-bc1a-6cab422d7912",
			"name": "Johnson, G. Allan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/c327c58e-5162-45d2-bc1a-6cab422d7912",
			"shortName": "Johnson, G.A."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7e9275db-a0f5-4562-89e4-1db994d7a342",
			"name": "Calabrese, Evan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e9275db-a0f5-4562-89e4-1db994d7a342",
			"shortName": "Calabrese, E."
		},
		{
			"schema.org/shortName": null,
			"identifier": "af81c3857f9ddaeff432d30349c4565a",
			"name": "Leergaard, Trygve B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a1cb1a1f-2634-4e81-b97f-303fd55efc13",
			"shortName": "Leergaard, T.B."
		},
		{
			"schema.org/shortName": null,
			"identifier": "222f2b54-21e2-4215-8798-4b7455e8ac58",
			"name": "Papp, Eszter A.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/222f2b54-21e2-4215-8798-4b7455e8ac58",
			"shortName": "Papp, E.A."
		}
	],
	"id": "a4a4984f-b1a2-4d28-9c94-fa4767b19c62",
	"kgReference": [
		"10.25493/DTSG-ZBS"
	],
	"publications": [
		{
			"name": "Addendum to \"Waxholm space atlas of the Sprague Dawley rat brain\"",
			"cite": "Papp, E.A., Leergaard, T.B., Calabrese, E., Johnson, G.A., Bjaalie, J.G., 2015. Addendum to \"Waxholm space atlas of the Sprague Dawley rat brain\" [NeuroImage 97 (2014) 374-386]. Neuroimage 105, 561-2.",
			"doi": "10.1016/j.neuroimage.2014.10.017"
		},
		{
			"name": "Waxholm Space atlas of the Sprague Dawley rat brain",
			"cite": "Papp, E.A., Leergaard, T.B., Calabrese, E., Johnson, G.A., Bjaalie, J.G., 2014. Waxholm Space atlas of the Sprague Dawley rat brain. NeuroImage 97, 374-386. ",
			"doi": "10.1016/j.neuroimage.2014.04.001"
		}
	]
}
