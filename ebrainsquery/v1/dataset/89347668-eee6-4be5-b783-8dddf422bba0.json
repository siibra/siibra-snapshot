{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Ruan, J., Bludau, S., Palomero-Gallagher, N., Caspers, S., Mohlberg, H.,  Eickhoff, S. B., Seitz, R. J., Amunts, A. (2018) Cytoarchitecture, probability maps, and functions of the human supplementary and pre-supplementary motor areas, 223(9):4169-4186.",
			"doi": "10.1007/s00429-018-1738-6"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area 6ma (preSMA, mesial SFG) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently, the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces where each voxel is assigned with the probability to belong to Area 6ma (preSMA, mesial SFG). The probability map of Area 6ma (preSMA, mesial SFG) is provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area 6ma (preSMA, mesial SFG):\nRuan et al. (2019) [Data set, v9.0] [DOI: 10.25493/HPX1-1CT](https://doi.org/10.25493%2FHPX1-1CT)\n\nThe most probable delineation of Area 6ma (preSMA, mesial SFG) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)",
	"parcellationAtlas": [],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 6ma (preSMA, mesial SFG)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/07b4c6a1-8a24-4f88-8f73-b2ea06e1c2f3"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area 6ma (preSMA, mesial SFG) (v9.1)",
	"files": [
		{
			"byteSize": 120269.0,
			"hashcode": "1717e4d8d37115930447a7d18a0736bb",
			"name": "Area-6ma_l_N10_nlin2Stdcolin27_9.1_publicP_c4e110a68942904a829ce1a6abb28bd1.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-6ma_pub/9.1/Area-6ma_l_N10_nlin2Stdcolin27_9.1_publicP_c4e110a68942904a829ce1a6abb28bd1.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "c4e110a68942904a829ce1a6abb28bd1"
		},
		{
			"byteSize": 98601.0,
			"hashcode": "86b93b4f9666f0dc6f172dc30752a48d",
			"name": "Area-6ma_l_N10_nlin2MNI152ASYM2009C_9.1_publicP_64a7846e7444bfc427baffe0d5dd606f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-6ma_pub/9.1/Area-6ma_l_N10_nlin2MNI152ASYM2009C_9.1_publicP_64a7846e7444bfc427baffe0d5dd606f.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "64a7846e7444bfc427baffe0d5dd606f"
		},
		{
			"byteSize": 95338.0,
			"hashcode": "74fca53223cd161b391f6b134a2a0f83",
			"name": "Area-6ma_r_N10_nlin2MNI152ASYM2009C_9.1_publicP_e6a4f1fe3062ba205a426d5f759a95f9.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-6ma_pub/9.1/Area-6ma_r_N10_nlin2MNI152ASYM2009C_9.1_publicP_e6a4f1fe3062ba205a426d5f759a95f9.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "e6a4f1fe3062ba205a426d5f759a95f9"
		},
		{
			"byteSize": 120789.0,
			"hashcode": "7116a95201464febd91d62cf230aff2d",
			"name": "Area-6ma_r_N10_nlin2Stdcolin27_9.1_publicP_d7e68149ee548516d2e3f63158253c5f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-6ma_pub/9.1/Area-6ma_r_N10_nlin2Stdcolin27_9.1_publicP_d7e68149ee548516d2e3f63158253c5f.nii.gz",
			"contentType": "application/octet-stream",
			"hash": "d7e68149ee548516d2e3f63158253c5f"
		},
		{
			"byteSize": 24.0,
			"hashcode": "b5b1beb73bb77633be6b8ad17183eefc",
			"name": "subjects_Area-6ma.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-6ma_pub/9.1/subjects_Area-6ma.csv",
			"contentType": "text/csv",
			"hash": "30c5f1139285c2d487fa61049dc272cf"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/89347668-eee6-4be5-b783-8dddf422bba0",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Seitz, R. J.",
			"identifier": "a855fc95-0e0d-44c5-bff2-4ea4a03d635b",
			"name": "Seitz, R\u00fcdiger J.  ",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/a855fc95-0e0d-44c5-bff2-4ea4a03d635b",
			"shortName": "Seitz, R. J."
		},
		{
			"schema.org/shortName": "Eickhoff, S. B.",
			"identifier": "f7e179682ccf98378bb566852f442c93",
			"name": "Eickhoff, Simon B.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/99a95057-bd4d-43ab-ad70-b7920ceedc48",
			"shortName": "Eickhoff, S. B."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Caspers, S.",
			"identifier": "7e060345-3035-4867-9dfd-704283b32fea",
			"name": "Caspers, Svenja",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7e060345-3035-4867-9dfd-704283b32fea",
			"shortName": "Caspers, S."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		},
		{
			"schema.org/shortName": "Bludau, S.",
			"identifier": "0c420063c33118f0fd65cabd330e9dd1",
			"name": "Bludau, Sebastian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8e05318b-4e11-454c-a49b-f81e3589dc98",
			"shortName": "Bludau, S."
		},
		{
			"schema.org/shortName": "Ruan, J.",
			"identifier": "7c01a711-1dd5-47bb-8829-e4b6d89d3045",
			"name": "Ruan, Jianghai",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/7c01a711-1dd5-47bb-8829-e4b6d89d3045",
			"shortName": "Ruan, J."
		}
	],
	"id": "89347668-eee6-4be5-b783-8dddf422bba0",
	"kgReference": [
		"10.25493/WVNR-SPT"
	],
	"publications": [
		{
			"name": "Cytoarchitecture, probability maps, and functions of the human supplementary and pre-supplementary motor  ",
			"cite": "Ruan, J., Bludau, S., Palomero-Gallagher, N., Caspers, S., Mohlberg, H.,  Eickhoff, S. B., Seitz, R. J., Amunts, A. (2018) Cytoarchitecture, probability maps, and functions of the human supplementary and pre-supplementary motor areas, 223(9):4169-4186.",
			"doi": "10.1007/s00429-018-1738-6"
		}
	]
}
