{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Wojtasik, M., Bludau, S., Eickhoff, S. B., Mohlberg, H., Gerboga, F., Caspers, S., Amunts, K. (2020) Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex. Frontiers in Neuroanatomy,14(2)",
			"doi": "10.3389/fnana.2020.00002"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area Fo5 (OFC) in the MNI Colin 27 and MNI ICBM 152 reference spaces. As part of the Julich-Brain atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. Subsequently the results of the cytoarchitectonic analysis are mapped to the MNI Colin 27 and MNI ICBM 152 reference spaces where each voxel is assigned with the probability to belong to Area Fo5 (OFC). The probability map of Area Fo5 (OFC) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area Fo5 (OFC):\nWojtasik et al. (2019) [Data set, v2.1] [DOI: 10.25493/HJMY-ZZP](https://doi.org/10.25493%2FHJMY-ZZP)\nWojtasik et al. (2020) [Data set, v3.0] [DOI: 10.25493/Z83Y-RZQ](https://doi.org/10.25493%2FZ83Y-RZQ)\n\nThe most probable delineation of Area Fo5 (OFC) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)\nAmunts et al. (2021) [Data set, v2.6] [DOI: 10.25493/KJQN-AM0](https://doi.org/10.25493%2FKJQN-AM0)\nAmunts et al. (2021) [Data set, v2.9] [DOI: 10.25493/VSMK-H94](https://doi.org/10.25493/VSMK-H94)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area Fo5 (OFC)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/3fd2e113-ec08-407b-bc88-172c9285694a"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area Fo5 (OFC) (v3.2)",
	"files": [
		{
			"byteSize": 69765.0,
			"hashcode": "ea3b2ae47505da22050642cec1efb57c",
			"name": "Area-Fo5_r_N10_nlin2ICBM152asym2009c_3.2_publicDOI_2ee82ada1e05e2c9c1b6be02dbb5456d.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo5_pub/3.2/Area-Fo5_r_N10_nlin2ICBM152asym2009c_3.2_publicDOI_2ee82ada1e05e2c9c1b6be02dbb5456d.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "d074002be2b3ae693c28d57934f13127"
		},
		{
			"byteSize": 92774.0,
			"hashcode": "3bac80d3e03e4fb0b3377834e3b5e4f0",
			"name": "Area-Fo5_r_N10_nlin2Stdcolin27_3.2_publicDOI_2ee82ada1e05e2c9c1b6be02dbb5456d.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo5_pub/3.2/Area-Fo5_r_N10_nlin2Stdcolin27_3.2_publicDOI_2ee82ada1e05e2c9c1b6be02dbb5456d.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "2ee82ada1e05e2c9c1b6be02dbb5456d"
		},
		{
			"byteSize": 24.0,
			"hashcode": "3e00541f87281bcdc14d4a1ffbaca461",
			"name": "subjects_Area-Fo5.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo5_pub/3.2/subjects_Area-Fo5.csv",
			"contentType": "text/csv",
			"hash": "06a73e8b0d18633bec1fb222634f8a89"
		},
		{
			"byteSize": 64086.0,
			"hashcode": "b65f72108c97dae2519bfddd9bd4f2e5",
			"name": "Area-Fo5_l_N10_nlin2ICBM152asym2009c_3.2_publicDOI_4953cdbf67a278e04edfc64ac8a849af.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo5_pub/3.2/Area-Fo5_l_N10_nlin2ICBM152asym2009c_3.2_publicDOI_4953cdbf67a278e04edfc64ac8a849af.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "9facd67b77191fa948edf76786d33dae"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "bb722fffc1a86070f5932e7639923d92",
			"name": "Licence-CC-BY-NC-SA.pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo5_pub/3.2/Licence-CC-BY-NC-SA.pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 86798.0,
			"hashcode": "ecca0b1c8195871fe4b00cb1399574ee",
			"name": "Area-Fo5_l_N10_nlin2Stdcolin27_3.2_publicDOI_4953cdbf67a278e04edfc64ac8a849af.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-Fo5_pub/3.2/Area-Fo5_l_N10_nlin2Stdcolin27_3.2_publicDOI_4953cdbf67a278e04edfc64ac8a849af.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "4953cdbf67a278e04edfc64ac8a849af"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/1a9ce543-b919-4e8c-9ca3-b53b8f9451f9",
	"contributors": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		},
		{
			"schema.org/shortName": "Mohlberg, H.",
			"identifier": "22bc11ef3726cc89459f83e66e88961b",
			"name": "Mohlberg, Hartmut",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d2be3785-5b4b-4451-87be-62b30442ed30",
			"shortName": "Mohlberg, H."
		},
		{
			"schema.org/shortName": "Wojtasik, M.",
			"identifier": "ca2960dc-6ef0-415b-bf89-b047283e7257",
			"name": "Wojtasik, Magdalena",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ca2960dc-6ef0-415b-bf89-b047283e7257",
			"shortName": "Wojtasik, M."
		}
	],
	"id": "1a9ce543-b919-4e8c-9ca3-b53b8f9451f9",
	"kgReference": [
		"10.25493/B65P-57X"
	],
	"publications": [
		{
			"name": "Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex",
			"cite": "Wojtasik, M., Bludau, S., Eickhoff, S. B., Mohlberg, H., Gerboga, F., Caspers, S., Amunts, K. (2020) Cytoarchitectonic Characterization and Functional Decoding of Four New Areas in the Human Lateral Orbitofrontal Cortex. Frontiers in Neuroanatomy,14(2)",
			"doi": "10.3389/fnana.2020.00002"
		}
	]
}
