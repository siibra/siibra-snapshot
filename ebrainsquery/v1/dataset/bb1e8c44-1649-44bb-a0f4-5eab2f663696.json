{
	"formats": [],
	"datasetDOI": [],
	"activity": [
		{
			"protocols": [
				"histology",
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [],
	"methods": [
		"Semantic atlas registration",
		"autoradiography - quantitative analysis",
		"autoradiography - imaging"
	],
	"custodians": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"project": [
		"Quantitative receptor data"
	],
	"description": "This dataset contains the densities (in fmol/mg protein) of receptors for classical neurotransmitters in the dorsal part of Area hOc3 (Cuneus) obtained by means of quantitative _in vitro_ autoradiography. The receptor densities are visualized as _fingerprints_ (**fp**), which provide the mean density and standard deviation for each of the analyzed receptor types, averaged across samples. \n\nOverview of available measurements [ **receptor** \\| **_neurotransmitter_** \\| _labeling agent_ ]:\n**AMPA** \\| **_glutamate_** \\| _[<sup>3</sup>H]AMPA_\n**kainate** \\| **_glutamate_** \\| _[<sup>3</sup>H]kainate_\n**NMDA** \\| **_glutamate_** \\| _[<sup>3</sup>H]MK-801_\n**GABA<sub>A</sub>** \\| **_GABA_** \\| _[<sup>3</sup>H]muscimol_\n**GABA<sub>B</sub>** \\| **_GABA_** \\| _[<sup>3</sup>H]CGP54626_\n**GABA<sub>A</sub>/BZ** \\| **_GABA_** \\| _[<sup>3</sup>H]flumazenil_\n**muscarinic M<sub>1</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]pirenzepine_\n**muscarinic M<sub>2</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]oxotremorine-M_\n**muscarinic M<sub>3</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]4-DAMP_\n**nicotinic &#945<sub>4</sub>&#946<sub>2</sub>** \\| **_acetylcholine_** \\| _[<sup>3</sup>H]epibatidine_\n**&#945<sub>1</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[<sup>3</sup>H]prazosin_\n**&#945<sub>2</sub>** \\| **_noradrenalin/norepinephrine_** \\| _[<sup>3</sup>H]UK-14,304_\n**5-HT<sub>1A</sub>** \\| **_serotonin_** \\| _[<sup>3</sup>H]8-OH-DPAT_\n**5-HT<sub>2</sub>** \\| **_serotonin_** \\| _[<sup>3</sup>H]ketanserin_\n**D<sub>1</sub>** \\| **_dopamine_** \\| _[<sup>3</sup>H]SCH23390_\n\nFor exemplary samples, we also provide laminar _profiles_ (**pr**)  and/or color-coded laminar _autoradiography_ images (**ar**). The density profiles provide, for a single tissue sample, an exemplary density distribution for a single receptor from the pial surface to the border between layer VI and the white matter. The autoradiography images show an exemplary density distribution of a single receptor for one laminar cross-section in a single tissue sample. \n\nInformation on the used tissue samples and corresponding subjects for the receptor fingerprints, profiles and autoradiographs as well as a list of analyzed receptors accompanies the provided dataset.\n\n**For methodological details, see:**\nZilles, K. et al. (2002). Quantitative analysis of cyto- and receptorarchitecture of the human brain, pp. 573-602. In: Brain Mapping: The Methods, 2nd edition (A.W. Toga and J.C. Mazziotta, eds.). San Diego, Academic Press.\n\nPalomero-Gallagher N, Zilles K. (2018) Cyto- and receptorarchitectonic mapping of the human brain. In: Handbook of Clinical Neurology 150: 355-387",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area hOc3d (Cuneus)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/d7ec4342-ae58-41e3-a68c-28e90a719d41"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Density measurements of different receptors for dorsal part of Area hOc3 (Cuneus) [human, v1.0]",
	"files": [
		{
			"byteSize": 390.0,
			"hashcode": "bf0c52c152b63f1083d9e2e7eef840d0",
			"name": "hOc3d_fp_20200214.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-hOc3d_pub/v1.0/hOc3d_fp_20200214.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "374749b17e0c266bf0bc7fb9bad1e10e"
		},
		{
			"byteSize": 999.0,
			"hashcode": "4fa98176ab1c636cee614631e9000347",
			"name": "labeling-agents.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-hOc3d_pub/v1.0/labeling-agents.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "808193442ce16295f968e5c22a092815"
		},
		{
			"byteSize": 99.0,
			"hashcode": "d4165b18bffe835964791acaca56d98f",
			"name": "subjects.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-hOc3d_pub/v1.0/subjects.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "188601b62c71e246a28fce0251811fe0"
		},
		{
			"byteSize": 1682.0,
			"hashcode": "5cea02554b4490fead0e8a67b76cf2e5",
			"name": "receptors.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-hOc3d_pub/v1.0/receptors.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "3e0becab5ee08519b2d67ba20771e2ad"
		},
		{
			"byteSize": 211.0,
			"hashcode": "58a7fc2f736fcba16754f910d3d09dda",
			"name": "tissue-samples.tsv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000045_receptors-human-hOc3d_pub/v1.0/tissue-samples.tsv",
			"contentType": "text/tab-separated-values",
			"hash": "4fdaabd1f5cfbd17da3247466832926e"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/bb1e8c44-1649-44bb-a0f4-5eab2f663696",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": "Palomero-Gallagher, N.",
			"identifier": "3ac753a73ac8cfd3151639808ae45913",
			"name": "Palomero-Gallagher, Nicola",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/d77176e3-faa9-4c93-8dc0-7f3663c6d2ee",
			"shortName": "Palomero-Gallagher, N."
		}
	],
	"id": "bb1e8c44-1649-44bb-a0f4-5eab2f663696",
	"kgReference": [
		"10.25493/4ETW-9XB"
	],
	"publications": []
}
