{
	"formats": [
		"NIFTI"
	],
	"datasetDOI": [
		{
			"cite": "Grefkes, C., Geyer, S., Schormann, T., Roland, P., & Zilles, K. (2001). Human Somatosensory Area 2: Observer-Independent Cytoarchitectonic Mapping, Interindividual Variability, and Population Map. NeuroImage, 14(3), 617\u2013631. ",
			"doi": "10.1006/nimg.2001.0858"
		}
	],
	"activity": [
		{
			"protocols": [
				"histology"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"imaging"
			],
			"preparation": [
				"Ex vivo"
			]
		},
		{
			"protocols": [
				"brain mapping"
			],
			"preparation": [
				"Ex vivo"
			]
		}
	],
	"referenceSpaces": [
		{
			"name": null,
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/dafcffc5-4826-4bf1-8ff6-46b8a31ff8e2"
		},
		{
			"name": "MNI Colin 27",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/referencespace/v1.0.0/7f39f7be-445b-47c0-9791-e971c0b6d992"
		}
	],
	"methods": [
		"silver staining",
		"magnetic resonance imaging (MRI)",
		"probability mapping",
		"cytoarchitectonic mapping"
	],
	"custodians": [
		{
			"schema.org/shortName": "Amunts, K.",
			"identifier": "e86dc72d5594a43f7a1db1e3945db2bf",
			"name": "Amunts, Katrin",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/01784c79-9a7b-4b47-83b6-0f50c075af81",
			"shortName": "Amunts, K."
		}
	],
	"project": [
		"Julich-Brain: cytoarchitectonic probabilistic maps of the human brain"
	],
	"description": "This dataset contains the distinct architectonic Area 2 (PostCS) in the individual, single subject template of the MNI Colin 27 as well as the MNI ICBM 152 2009c nonlinear asymmetric reference space. As part of the Julich-Brain cytoarchitectonic atlas, the area was identified using cytoarchitectonic analysis on cell-body-stained histological sections of 10 human postmortem brains obtained from the body donor program of the University of D\u00fcsseldorf. The results of the cytoarchitectonic analysis were then mapped to both reference spaces, where each voxel was assigned the probability to belong to Area 2 (PostCS). The probability map of Area 2 (PostCS) are provided in the NifTi format for each brain reference space and hemisphere. The Julich-Brain atlas relies on a modular, flexible and adaptive framework containing workflows to create the probabilistic brain maps for these structures. Note that methodological improvements and integration of new brain structures may lead to small deviations in earlier released datasets.\n\nOther available data versions of Area 2 (PostCS):\nGrefkes et al. (2018) [Data set, v3.2] [DOI: 10.25493/JZP0-Q97](https://doi.org/10.25493%2FJZP0-Q97)\nGrefkes et al. (2019) [Data set, v3.4] [DOI: 10.25493/QA8F-DD2](https://doi.org/10.25493%2FQA8F-DD2)\n\nThe most probable delineation of Area 2 (PostCS) derived from the calculation of a maximum probability map of all currently released Julich-Brain brain structures can be found here:\nAmunts et al. (2019) [Data set, v1.13] [DOI: 10.25493/Q3ZS-NV6](https://doi.org/10.25493%2FQ3ZS-NV6)\nAmunts et al. (2019) [Data set, v1.18] [DOI: 10.25493/8EGG-ZAR](https://doi.org/10.25493%2F8EGG-ZAR)\nAmunts et al. (2020) [Data set, v2.2] [DOI: 10.25493/TAKY-64D](https://doi.org/10.25493%2FTAKY-64D)\nAmunts et al. (2020) [Data set, v2.4] [DOI: 10.25493/A7Y0-NX9](https://doi.org/10.25493%2FA7Y0-NX9)\nAmunts et al. (2020) [Data set, v2.5] [DOI: 10.25493/8JKE-M53](https://doi.org/10.25493/8JKE-M53)",
	"parcellationAtlas": [
		{
			"name": "Julich-Brain Atlas",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationatlas/v1.0.0/94c1125b-b87e-45e4-901c-00daee7f2579",
			"id": [
				"deec923ec31a82f89a9c7c76a6fefd6b",
				"e2d45e028b6da0f6d9fdb9491a4de80a"
			]
		}
	],
	"licenseInfo": [
		{
			"name": "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International",
			"url": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
		}
	],
	"embargoStatus": [
		{
			"identifier": [
				"b24ce0cd392a5b0b8dedc66c25213594",
				"b24ce0cd392a5b0b8dedc66c25213594"
			],
			"name": "Free",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/embargostatus/v1.0.0/222b535c-2e8f-4892-acf4-39006c5219b9"
		}
	],
	"license": [],
	"parcellationRegion": [
		{
			"species": [],
			"name": "Area 2 (PostCS)",
			"alias": "",
			"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/parcellationregion/v1.0.0/f9147ae9-5cf0-41b2-89a3-e6e6df07bef1"
		}
	],
	"species": [
		"Homo sapiens"
	],
	"name": "Probabilistic cytoarchitectonic map of Area 2 (PostCS) (v5.0)",
	"files": [
		{
			"byteSize": 19.0,
			"hashcode": "167496fc471f95efe51c05614b5253e7",
			"name": "subjects_Area-2.csv",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-2_pub/5.0/subjects_Area-2.csv",
			"contentType": "text/csv",
			"hash": "77b4f1dfa1c761cad012be7caa2a4875"
		},
		{
			"byteSize": 150351.0,
			"hashcode": "ecea232a0ee1fae2c23c17741774b282",
			"name": "Area-2_l_N9_nlin2Stdcolin27_5.0_publicP_491ecb7a007fb60ef451ecb31056bc47.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-2_pub/5.0/Area-2_l_N9_nlin2Stdcolin27_5.0_publicP_491ecb7a007fb60ef451ecb31056bc47.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "491ecb7a007fb60ef451ecb31056bc47"
		},
		{
			"byteSize": 143466.0,
			"hashcode": "ff409503387d666290d22eff91bfb26e",
			"name": "Licence (CCBY-NC-SA).pdf",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-2_pub/5.0/Licence (CCBY-NC-SA).pdf",
			"contentType": "application/pdf",
			"hash": "368857ff4d0e4b14c745d94db7f7d3ff"
		},
		{
			"byteSize": 147991.0,
			"hashcode": "fa6fed5a2d556014729d11f4d6efa0e3",
			"name": "Area-2_r_N9_nlin2Stdcolin27_5.0_publicP_7ca64d97db65499ede7118ce50043289.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-2_pub/5.0/Area-2_r_N9_nlin2Stdcolin27_5.0_publicP_7ca64d97db65499ede7118ce50043289.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "7ca64d97db65499ede7118ce50043289"
		},
		{
			"byteSize": 145581.0,
			"hashcode": "4c322470a99f9cbb4d9c6468ddad2214",
			"name": "Area-2_l_N9_nlin2ICBM152asym2009c_5.0_publicP_ec3c72725b30062daeda0131b18d483e.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-2_pub/5.0/Area-2_l_N9_nlin2ICBM152asym2009c_5.0_publicP_ec3c72725b30062daeda0131b18d483e.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "ec3c72725b30062daeda0131b18d483e"
		},
		{
			"byteSize": 136309.0,
			"hashcode": "756459ea937c0aa5547d3c5cf68407e8",
			"name": "Area-2_r_N9_nlin2ICBM152asym2009c_5.0_publicP_c81ae8818033bc962de7122119b5663f.nii.gz",
			"absolutePath": "https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000001_jubrain-cytoatlas-Area-2_pub/5.0/Area-2_r_N9_nlin2ICBM152asym2009c_5.0_publicP_c81ae8818033bc962de7122119b5663f.nii.gz",
			"contentType": "application/x-gzip",
			"hash": "c81ae8818033bc962de7122119b5663f"
		}
	],
	"fullId": "https://nexus.humanbrainproject.org/v0/data/minds/core/dataset/v1.0.0/6f3a6471-7c1e-48c0-a622-7f669464aeca",
	"contributors": [
		{
			"schema.org/shortName": "Zilles, K.",
			"identifier": "2457b4a7cf0b3fa199dcc3c88180dc9f",
			"name": "Zilles, Karl",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bacae1fc-d8ff-4050-8388-7d826592c62c",
			"shortName": "Zilles, K."
		},
		{
			"schema.org/shortName": null,
			"identifier": "88170aec3154260af796dcfe393fc9fa",
			"name": "Roland, Per E.",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/14181b4a-fd94-4e19-885b-70d8a0dd8146",
			"shortName": "Roland, P."
		},
		{
			"schema.org/shortName": null,
			"identifier": "7097a4c37e89c2b8a8395b3604513004",
			"name": "Schormann, Thorsten",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/ebf7c571-b0c2-43ce-9d84-91fb925949f3",
			"shortName": "Schormann, T."
		},
		{
			"schema.org/shortName": null,
			"identifier": "183ac28a5978832f25623307522cba98",
			"name": "Geyer, Stefan",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/8a38f341-5b88-4cb2-b8b7-6473a676d892",
			"shortName": "Geyer, S."
		},
		{
			"schema.org/shortName": null,
			"identifier": "117cfb31e36224c51aa326590ddac35b",
			"name": "Grefkes, Christian",
			"@id": "https://nexus.humanbrainproject.org/v0/data/minds/core/person/v1.0.0/bbfedca2-a0da-4ef1-aef0-ec6356d809b4",
			"shortName": "Grefkes, C."
		}
	],
	"id": "6f3a6471-7c1e-48c0-a622-7f669464aeca",
	"kgReference": [
		"10.25493/4160-B8K"
	],
	"publications": [
		{
			"name": "Human Somatosensory Area 2: Observer-Independent Cytoarchitectonic Mapping, Interindividual Variability, and Population Map",
			"cite": "Grefkes, C., Geyer, S., Schormann, T., Roland, P., & Zilles, K. (2001). Human Somatosensory Area 2: Observer-Independent Cytoarchitectonic Mapping, Interindividual Variability, and Population Map. NeuroImage, 14(3), 617\u2013631. ",
			"doi": "10.1006/nimg.2001.0858"
		}
	]
}
