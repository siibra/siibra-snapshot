import os
import requests
import json

query_id = "64ad0cd6-fbd6-47d3-b247-099e9ce8f313"

url = f"https://core.kg.ebrains.eu/v3/queries/{query_id}/instances?stage=RELEASED&vocab=https://schema.hbp.eu/myQuery/"

auth_token=os.getenv("AUTH_TOKEN")

path_to_snapshot="ebrainsquery/v3/DatasetVersion"

def main():
    resp = requests.get(url, headers={
        "Authorization": f"bearer {auth_token}"
    })
    resp.raise_for_status()
    resp_json = resp.json()
    for item in resp_json.get("data"):
        _id: str = item.get("id")
        trimmed_id = _id.replace("https://kg.ebrains.eu/api/instances/", "")
        with open(f"{path_to_snapshot}/{trimmed_id}.json", "w") as fp:
            json.dump(item, fp=fp, indent="\t")
            fp.write("\n")

if __name__ == "__main__":
    main()