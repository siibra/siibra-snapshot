import os
import requests
import json

query_id = "fe925a3a-bfcd-43a9-8eab-f8d4589cedea"

url = f"https://core.kg.ebrains.eu/v3/queries/{query_id}/instances?stage=RELEASED&vocab=https://schema.hbp.eu/myQuery/"

auth_token=os.getenv("AUTH_TOKEN")

path_to_snapshot="ebrainsquery/v3/Dataset"

def main():
    resp = requests.get(url, headers={
        "Authorization": f"bearer {auth_token}"
    })
    resp.raise_for_status()
    resp_json = resp.json()
    for item in resp_json.get("data"):
        _id: str = item.get("id")
        trimmed_id = _id.replace("https://kg.ebrains.eu/api/instances/", "")
        with open(f"{path_to_snapshot}/{trimmed_id}.json", "w") as fp:
            json.dump(item, fp=fp, indent="\t")
            fp.write("\n")

if __name__ == "__main__":
    main()