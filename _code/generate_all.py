import os, json
dir_to_walk="ebrainsquery"

def main():
    for dirpath, dirnames, filenames in os.walk(dir_to_walk):
        list_of_items = []
        for filename in filenames:
            if not filename.endswith(".json"):
                continue
            if "_all.json" in filename:
                continue
            with open(f"{dirpath}/{filename}", "r") as fp:
                list_of_items.append(json.load(fp))
        if len(list_of_items) > 0:
            with open(f"{dirpath}/_all.json", "w") as fp:
                json.dump(list_of_items, fp=fp)


if __name__ == "__main__":
    main()