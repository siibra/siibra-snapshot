import requests, os
from concurrent.futures import ThreadPoolExecutor
from itertools import repeat
import json

server = "https://kg.humanbrainproject.eu"
query_id = "interactiveViewerKgQuery-v1_0-Copy2"
auth_token = os.getenv("AUTH_TOKEN")
type_name = "dataset"
schema_id = f"minds/core/{type_name}/v1.0.0"

def write(dir, d):
    filename = d.get("fullId").replace(f"https://nexus.humanbrainproject.org/v0/data/{schema_id}/", '') + ".json"
    if os.path.exists(f"{dir}/{filename}"):
        os.unlink(f"{dir}/{filename}")
    with open(f"{dir}/{filename}", "w") as fp:
        json.dump(d, fp=fp, indent="\t")
        fp.write("\n")
    

def main():
    url=f"{server}/query/{schema_id}/{query_id}/instances?databaseScope=RELEASED&vocab=https://schema.hbp.eu/myQuery/"
    resp = requests.get(url, headers={
        "Authorization": f"bearer {auth_token}"
    })
    resp.raise_for_status()
    resp_json = resp.json()
    
    items = resp_json.get("results")
    with ThreadPoolExecutor(max_workers=16) as ex:
        for _ in ex.map(
            write,
            repeat(f"./ebrainsquery/v1/{type_name}"),
            items
        ):
            pass

    

if __name__ == "__main__":
    main()