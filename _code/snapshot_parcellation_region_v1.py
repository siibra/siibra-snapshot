import requests, os
from concurrent.futures import ThreadPoolExecutor
from itertools import repeat
import json

server = "https://kg.humanbrainproject.eu"
query_id = "siibra-kg-feature-summary-0_0_4"
auth_token = os.getenv("AUTH_TOKEN")
type_name = "parcellationregion"
schema_id = f"minds/core/{type_name}/v1.0.0"

def write(dir, d):
    filename = d.get("@id").replace(f"https://nexus.humanbrainproject.org/v0/data/{schema_id}/", '') + ".json"
    with open(f"{dir}/{filename}", "w") as fp:
        json.dump(d, fp=fp, indent="\t")
        fp.write("\n")
    

def main():
    url=f"{server}/query/{schema_id}/{query_id}/instances?databaseScope=RELEASED&vocab=https://schema.hbp.eu/myQuery/"
    resp = requests.get(url, headers={
        "Authorization": f"bearer {auth_token}"
    })
    resp.raise_for_status()
    resp_json = resp.json()
    
    items = resp_json.get("results")
    with ThreadPoolExecutor(max_workers=16) as ex:
        for _ in ex.map(
            write,
            repeat(f"./ebrainsquery/v1/{type_name}s"),
            items
        ):
            pass

    

if __name__ == "__main__":
    main()