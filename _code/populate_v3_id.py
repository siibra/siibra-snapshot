# Script used to populate siibra-configurations with v1 ids with v3 ids

import os
import json


v3_dir = "./ebrainsquery/v3/Dataset"
target_dir = "../brainscapes-configurations"
ignore_dir = (
    "venv",
)

species_key = "minds/core/species/v1.0.0"
dataset_key = "minds/core/dataset/v1.0.0"

new_species_key = "openminds/Species"
new_dataset_key = "openminds/Dataset"

def main():

    not_yet = set()
    dataset_dict = {}
    v3_ids = set()
    species_dict = {
        # homo sapien
        "0ea4e6ba-2681-4f7d-9fa9-49b915caaac9": "97c070c6-8e1f-4ee8-9d28-18c7945921dd",
        "f3490d7f-8f7f-4b40-b238-963dcac84412": "ab532423-1fd7-4255-8c6f-f99dc6df814f",


        "cfc1656c-67d1-4d2c-a17e-efd7ce0df88c": "d9875ebd-260e-4337-a637-b62fed4aa91d",

        # Macaca fascicularis
        "c541401b-69f4-4809-b6eb-82594fc90551": "0b6df2b3-5297-40cf-adde-9443d3d8214a",
    }
    def process_ebrains_ref(obj):
        
        if species_key in obj:
            assert obj[species_key] in species_dict,  f"species {obj[species_key]}"
            obj[new_species_key] = species_dict[obj[species_key]]
        if dataset_key in obj:
            old_id = obj[dataset_key]
            if old_id not in dataset_dict:
                if old_id in v3_ids:
                    v3_id = obj.pop(dataset_key)
                    obj[new_dataset_key] = v3_id
                else:
                    not_yet.add(old_id)
                    print(f"dataset with id {old_id} not found in dict")
            else:
                obj[new_dataset_key] = dataset_dict[old_id]

    def process_obj(obj):
        if isinstance(obj, (str, int, float)):
            return
        if isinstance(obj, list):
            for item in obj:
                process_obj(item)
        if isinstance(obj, dict):
            for key in obj:
                process_obj(obj[key])
            if "ebrains" in obj:
                process_ebrains_ref(obj["ebrains"])
                


    # populate v3 dict
    for dirpath, dirnames, filenames in os.walk(v3_dir):
        for filename in filenames:
            if not filename.endswith(".json"):
                continue
            if filename.startswith("_all"):
                continue
            with open(f"{dirpath}/{filename}", "r") as fp:
                input_json = json.load(fp)
                new_id = input_json.get("id").split("/")[-1]
                v3_ids.add(new_id)
                for version in input_json.get("versions", []):
                    id = version.get("id")
                    assert id
                    old_id = id.split("/")[-1]
                    dataset_dict[old_id] = new_id

    for dirpath, dirnames, filenames in os.walk(target_dir):
        if any(d in dirpath for d in ignore_dir):
            continue
        for filename in filenames:
            if not filename.endswith(".json"):
                continue
            fullpath = f"{dirpath}/{filename}"
            with open(fullpath, "r") as fp:
                inputjson = json.load(fp)
                process_obj(inputjson)
            with open(fullpath, "w") as fp:
                json.dump(inputjson, indent="\t", fp=fp)
                fp.write("\n")
    
    with open("missing.txt", "w") as fp:
        for l in list(not_yet):
            fp.write(l)
            fp.write("\n")
    
if __name__ == "__main__":
    main()